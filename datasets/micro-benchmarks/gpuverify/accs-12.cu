//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 31 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 18 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 43 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 34 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 43 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 23 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 10 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 25 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 1 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 24 * blockDim.x ];
    out[threadIdx.x + 10 * blockDim.x ] = out[threadIdx.x + 31 * blockDim.x ];
    out[threadIdx.x + 11 * blockDim.x ] = out[threadIdx.x + 18 * blockDim.x ];

}
