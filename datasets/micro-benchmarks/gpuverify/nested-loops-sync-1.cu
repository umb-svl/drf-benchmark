//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (float* out, float* a, float* b, int ub) {



    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x + i0] = a[i0] + b[i0];
        __syncthreads();
    }

}
