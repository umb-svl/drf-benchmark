#!/usr/bin/env python3
import subprocess
import yaml
import argparse

def main():
    params = yaml.safe_load(open("config.yaml"))
    parser = argparse.ArgumentParser()
    probs = list(params["problems"])
    probs.sort()
    parser.add_argument("-p", "--problem", choices=probs)
    tools = list(params["tools"].keys())
    tools.sort()
    parser.add_argument("-f", "--format", choices=tools)
    args = parser.parse_args()
    for tool in params["formats"]:
        if args.format is not None and tool != args.format:
            continue
        lo, hi = params["default_range"]
        files = []
        for size in range(lo, hi + 1):
            for problem in params["problems"]:
                if args.problem is not None and problem != args.problem:
                    continue
                ext = "c" if tool == "pug" else "cu"
                output = f"{tool}/{problem}-{size}.{ext}"
                cmd = [
                    "../../benchmark/contrib/microbench-gen.py",
                    "--block-dim", str(params["block_dim"]),
                    "--grid-dim", str(params["grid_dim"]),
                    "--size", str(size),
                    "--tool", tool,
                    "-o", output,
                    problem
                ]
                files.append(output)
                print("RUN", " ".join(cmd))
                subprocess.run(cmd)
        with open(f"{tool}/{lo}-{hi}.txt", "w") as fp:
            for f in files:
                print(f, file=fp)
if __name__ == '__main__':
    main()
