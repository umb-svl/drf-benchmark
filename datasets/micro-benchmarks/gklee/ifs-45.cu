#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out, float* a, float* b) {



    if (threadIdx.x == 0) {
        out[threadIdx.x] = a[0] + b[0];
    }
    if (threadIdx.x == 1) {
        out[threadIdx.x] = a[1] + b[1];
    }
    if (threadIdx.x == 2) {
        out[threadIdx.x] = a[2] + b[2];
    }
    if (threadIdx.x == 3) {
        out[threadIdx.x] = a[3] + b[3];
    }
    if (threadIdx.x == 4) {
        out[threadIdx.x] = a[4] + b[4];
    }
    if (threadIdx.x == 5) {
        out[threadIdx.x] = a[5] + b[5];
    }
    if (threadIdx.x == 6) {
        out[threadIdx.x] = a[6] + b[6];
    }
    if (threadIdx.x == 7) {
        out[threadIdx.x] = a[7] + b[7];
    }
    if (threadIdx.x == 8) {
        out[threadIdx.x] = a[8] + b[8];
    }
    if (threadIdx.x == 9) {
        out[threadIdx.x] = a[9] + b[9];
    }
    if (threadIdx.x == 10) {
        out[threadIdx.x] = a[10] + b[10];
    }
    if (threadIdx.x == 11) {
        out[threadIdx.x] = a[11] + b[11];
    }
    if (threadIdx.x == 12) {
        out[threadIdx.x] = a[12] + b[12];
    }
    if (threadIdx.x == 13) {
        out[threadIdx.x] = a[13] + b[13];
    }
    if (threadIdx.x == 14) {
        out[threadIdx.x] = a[14] + b[14];
    }
    if (threadIdx.x == 15) {
        out[threadIdx.x] = a[15] + b[15];
    }
    if (threadIdx.x == 16) {
        out[threadIdx.x] = a[16] + b[16];
    }
    if (threadIdx.x == 17) {
        out[threadIdx.x] = a[17] + b[17];
    }
    if (threadIdx.x == 18) {
        out[threadIdx.x] = a[18] + b[18];
    }
    if (threadIdx.x == 19) {
        out[threadIdx.x] = a[19] + b[19];
    }
    if (threadIdx.x == 20) {
        out[threadIdx.x] = a[20] + b[20];
    }
    if (threadIdx.x == 21) {
        out[threadIdx.x] = a[21] + b[21];
    }
    if (threadIdx.x == 22) {
        out[threadIdx.x] = a[22] + b[22];
    }
    if (threadIdx.x == 23) {
        out[threadIdx.x] = a[23] + b[23];
    }
    if (threadIdx.x == 24) {
        out[threadIdx.x] = a[24] + b[24];
    }
    if (threadIdx.x == 25) {
        out[threadIdx.x] = a[25] + b[25];
    }
    if (threadIdx.x == 26) {
        out[threadIdx.x] = a[26] + b[26];
    }
    if (threadIdx.x == 27) {
        out[threadIdx.x] = a[27] + b[27];
    }
    if (threadIdx.x == 28) {
        out[threadIdx.x] = a[28] + b[28];
    }
    if (threadIdx.x == 29) {
        out[threadIdx.x] = a[29] + b[29];
    }
    if (threadIdx.x == 30) {
        out[threadIdx.x] = a[30] + b[30];
    }
    if (threadIdx.x == 31) {
        out[threadIdx.x] = a[31] + b[31];
    }
    if (threadIdx.x == 32) {
        out[threadIdx.x] = a[32] + b[32];
    }
    if (threadIdx.x == 33) {
        out[threadIdx.x] = a[33] + b[33];
    }
    if (threadIdx.x == 34) {
        out[threadIdx.x] = a[34] + b[34];
    }
    if (threadIdx.x == 35) {
        out[threadIdx.x] = a[35] + b[35];
    }
    if (threadIdx.x == 36) {
        out[threadIdx.x] = a[36] + b[36];
    }
    if (threadIdx.x == 37) {
        out[threadIdx.x] = a[37] + b[37];
    }
    if (threadIdx.x == 38) {
        out[threadIdx.x] = a[38] + b[38];
    }
    if (threadIdx.x == 39) {
        out[threadIdx.x] = a[39] + b[39];
    }
    if (threadIdx.x == 40) {
        out[threadIdx.x] = a[40] + b[40];
    }
    if (threadIdx.x == 41) {
        out[threadIdx.x] = a[41] + b[41];
    }
    if (threadIdx.x == 42) {
        out[threadIdx.x] = a[42] + b[42];
    }
    if (threadIdx.x == 43) {
        out[threadIdx.x] = a[43] + b[43];
    }
    if (threadIdx.x == 44) {
        out[threadIdx.x] = a[44] + b[44];
    }

}
int main () {
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    
    /* Declare array 'a' */
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    
    /* Declare array 'b' */
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out,
        a,
        b
    );
    return 0;
}
