#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out, float* a, float* b) {



    if (threadIdx.x == 0) {
        out[threadIdx.x] = a[0] + b[0];
    }
    if (threadIdx.x == 1) {
        out[threadIdx.x] = a[1] + b[1];
    }
    if (threadIdx.x == 2) {
        out[threadIdx.x] = a[2] + b[2];
    }
    if (threadIdx.x == 3) {
        out[threadIdx.x] = a[3] + b[3];
    }

}
int main () {
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    
    /* Declare array 'a' */
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    
    /* Declare array 'b' */
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out,
        a,
        b
    );
    return 0;
}
