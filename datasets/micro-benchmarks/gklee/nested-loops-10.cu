#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out, float* a, float* b, int ub) {



    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x] = a[i0] + b[i0];
    for (int i1 = 0; i1 < ub; i1++) {
        out[threadIdx.x] = a[i1] + b[i1];
    for (int i2 = 0; i2 < ub; i2++) {
        out[threadIdx.x] = a[i2] + b[i2];
    for (int i3 = 0; i3 < ub; i3++) {
        out[threadIdx.x] = a[i3] + b[i3];
    for (int i4 = 0; i4 < ub; i4++) {
        out[threadIdx.x] = a[i4] + b[i4];
    for (int i5 = 0; i5 < ub; i5++) {
        out[threadIdx.x] = a[i5] + b[i5];
    for (int i6 = 0; i6 < ub; i6++) {
        out[threadIdx.x] = a[i6] + b[i6];
    for (int i7 = 0; i7 < ub; i7++) {
        out[threadIdx.x] = a[i7] + b[i7];
    for (int i8 = 0; i8 < ub; i8++) {
        out[threadIdx.x] = a[i8] + b[i8];
    for (int i9 = 0; i9 < ub; i9++) {
        out[threadIdx.x] = a[i9] + b[i9];
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }

}
int main () {
    /* Declare scalar 'ub' */
    int ub;
    klee_make_symbolic(&ub, sizeof(int), "ub");
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    
    /* Declare array 'a' */
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    
    /* Declare array 'b' */
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out,
        a,
        b,
        ub
    );
    return 0;
}
