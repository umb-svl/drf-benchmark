#include <stdio.h>







__global__ void kernel (float* out, float* a, float* b, int ub) {

    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x + i0] = a[i0] + b[i0];
        __syncthreads();
    }
    for (int i1 = 0; i1 < ub; i1++) {
        out[threadIdx.x + i1] = a[i1] + b[i1];
        __syncthreads();
    }
    for (int i2 = 0; i2 < ub; i2++) {
        out[threadIdx.x + i2] = a[i2] + b[i2];
        __syncthreads();
    }
    for (int i3 = 0; i3 < ub; i3++) {
        out[threadIdx.x + i3] = a[i3] + b[i3];
        __syncthreads();
    }


}
int main () {
    /* Declare scalar 'ub' */
    int ub;
    klee_make_symbolic(&ub, sizeof(ub), "ub");
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    
    /* Declare array 'a' */
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    
    /* Declare array 'b' */
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out,
        a,
        b,
        ub
    );
    return 0;
}
