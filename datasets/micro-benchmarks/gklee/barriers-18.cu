#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out, float* a, float* b) {



    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();

}
int main () {
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    
    /* Declare array 'a' */
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    
    /* Declare array 'b' */
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out,
        a,
        b
    );
    return 0;
}
