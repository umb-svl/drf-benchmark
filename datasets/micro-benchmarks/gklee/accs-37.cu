#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 44 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 39 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 6 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 60 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 40 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 43 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 54 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 48 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 54 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 41 * blockDim.x ];
    out[threadIdx.x + 10 * blockDim.x ] = out[threadIdx.x + 33 * blockDim.x ];
    out[threadIdx.x + 11 * blockDim.x ] = out[threadIdx.x + 3 * blockDim.x ];
    out[threadIdx.x + 12 * blockDim.x ] = out[threadIdx.x + 41 * blockDim.x ];
    out[threadIdx.x + 13 * blockDim.x ] = out[threadIdx.x + 63 * blockDim.x ];
    out[threadIdx.x + 14 * blockDim.x ] = out[threadIdx.x + 24 * blockDim.x ];
    out[threadIdx.x + 15 * blockDim.x ] = out[threadIdx.x + 29 * blockDim.x ];
    out[threadIdx.x + 16 * blockDim.x ] = out[threadIdx.x + 34 * blockDim.x ];
    out[threadIdx.x + 17 * blockDim.x ] = out[threadIdx.x + 51 * blockDim.x ];
    out[threadIdx.x + 18 * blockDim.x ] = out[threadIdx.x + 7 * blockDim.x ];
    out[threadIdx.x + 19 * blockDim.x ] = out[threadIdx.x + 63 * blockDim.x ];
    out[threadIdx.x + 20 * blockDim.x ] = out[threadIdx.x + 44 * blockDim.x ];
    out[threadIdx.x + 21 * blockDim.x ] = out[threadIdx.x + 30 * blockDim.x ];
    out[threadIdx.x + 22 * blockDim.x ] = out[threadIdx.x + 39 * blockDim.x ];
    out[threadIdx.x + 23 * blockDim.x ] = out[threadIdx.x + 51 * blockDim.x ];
    out[threadIdx.x + 24 * blockDim.x ] = out[threadIdx.x + 19 * blockDim.x ];
    out[threadIdx.x + 25 * blockDim.x ] = out[threadIdx.x + 60 * blockDim.x ];
    out[threadIdx.x + 26 * blockDim.x ] = out[threadIdx.x + 25 * blockDim.x ];
    out[threadIdx.x + 27 * blockDim.x ] = out[threadIdx.x + 63 * blockDim.x ];
    out[threadIdx.x + 28 * blockDim.x ] = out[threadIdx.x + 48 * blockDim.x ];
    out[threadIdx.x + 29 * blockDim.x ] = out[threadIdx.x + 28 * blockDim.x ];
    out[threadIdx.x + 30 * blockDim.x ] = out[threadIdx.x + 29 * blockDim.x ];
    out[threadIdx.x + 31 * blockDim.x ] = out[threadIdx.x + 10 * blockDim.x ];
    out[threadIdx.x + 32 * blockDim.x ] = out[threadIdx.x + 62 * blockDim.x ];
    out[threadIdx.x + 33 * blockDim.x ] = out[threadIdx.x + 52 * blockDim.x ];
    out[threadIdx.x + 34 * blockDim.x ] = out[threadIdx.x + 63 * blockDim.x ];
    out[threadIdx.x + 35 * blockDim.x ] = out[threadIdx.x + 58 * blockDim.x ];
    out[threadIdx.x + 36 * blockDim.x ] = out[threadIdx.x + 54 * blockDim.x ];

}
int main () {
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 4096 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out
    );
    return 0;
}
