#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif






__global__ void kernel (float* out, float* a, float* b) {




__requires(blockDim.x == 64);





__requires(gridDim.x == 1);





    if (threadIdx.x == 0) {
        out[threadIdx.x] = a[0] + b[0];
    }
    if (threadIdx.x == 1) {
        out[threadIdx.x] = a[1] + b[1];
    }
    if (threadIdx.x == 2) {
        out[threadIdx.x] = a[2] + b[2];
    }
    if (threadIdx.x == 3) {
        out[threadIdx.x] = a[3] + b[3];
    }
    if (threadIdx.x == 4) {
        out[threadIdx.x] = a[4] + b[4];
    }
    if (threadIdx.x == 5) {
        out[threadIdx.x] = a[5] + b[5];
    }
    if (threadIdx.x == 6) {
        out[threadIdx.x] = a[6] + b[6];
    }
    if (threadIdx.x == 7) {
        out[threadIdx.x] = a[7] + b[7];
    }
    if (threadIdx.x == 8) {
        out[threadIdx.x] = a[8] + b[8];
    }
    if (threadIdx.x == 9) {
        out[threadIdx.x] = a[9] + b[9];
    }
    if (threadIdx.x == 10) {
        out[threadIdx.x] = a[10] + b[10];
    }
    if (threadIdx.x == 11) {
        out[threadIdx.x] = a[11] + b[11];
    }
    if (threadIdx.x == 12) {
        out[threadIdx.x] = a[12] + b[12];
    }
    if (threadIdx.x == 13) {
        out[threadIdx.x] = a[13] + b[13];
    }
    if (threadIdx.x == 14) {
        out[threadIdx.x] = a[14] + b[14];
    }
    if (threadIdx.x == 15) {
        out[threadIdx.x] = a[15] + b[15];
    }
    if (threadIdx.x == 16) {
        out[threadIdx.x] = a[16] + b[16];
    }
    if (threadIdx.x == 17) {
        out[threadIdx.x] = a[17] + b[17];
    }
    if (threadIdx.x == 18) {
        out[threadIdx.x] = a[18] + b[18];
    }
    if (threadIdx.x == 19) {
        out[threadIdx.x] = a[19] + b[19];
    }
    if (threadIdx.x == 20) {
        out[threadIdx.x] = a[20] + b[20];
    }
    if (threadIdx.x == 21) {
        out[threadIdx.x] = a[21] + b[21];
    }
    if (threadIdx.x == 22) {
        out[threadIdx.x] = a[22] + b[22];
    }
    if (threadIdx.x == 23) {
        out[threadIdx.x] = a[23] + b[23];
    }
    if (threadIdx.x == 24) {
        out[threadIdx.x] = a[24] + b[24];
    }
    if (threadIdx.x == 25) {
        out[threadIdx.x] = a[25] + b[25];
    }
    if (threadIdx.x == 26) {
        out[threadIdx.x] = a[26] + b[26];
    }
    if (threadIdx.x == 27) {
        out[threadIdx.x] = a[27] + b[27];
    }
    if (threadIdx.x == 28) {
        out[threadIdx.x] = a[28] + b[28];
    }
    if (threadIdx.x == 29) {
        out[threadIdx.x] = a[29] + b[29];
    }
    if (threadIdx.x == 30) {
        out[threadIdx.x] = a[30] + b[30];
    }
    if (threadIdx.x == 31) {
        out[threadIdx.x] = a[31] + b[31];
    }
    if (threadIdx.x == 32) {
        out[threadIdx.x] = a[32] + b[32];
    }
    if (threadIdx.x == 33) {
        out[threadIdx.x] = a[33] + b[33];
    }
    if (threadIdx.x == 34) {
        out[threadIdx.x] = a[34] + b[34];
    }

}
