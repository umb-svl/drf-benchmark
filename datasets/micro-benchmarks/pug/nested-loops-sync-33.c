#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif






__global__ void kernel (float* out, float* a, float* b, int ub) {




__requires(blockDim.x == 64);





__requires(gridDim.x == 1);





    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x + i0] = a[i0] + b[i0];
        __syncthreads();
    for (int i1 = 0; i1 < ub; i1++) {
        out[threadIdx.x + i1] = a[i1] + b[i1];
        __syncthreads();
    for (int i2 = 0; i2 < ub; i2++) {
        out[threadIdx.x + i2] = a[i2] + b[i2];
        __syncthreads();
    for (int i3 = 0; i3 < ub; i3++) {
        out[threadIdx.x + i3] = a[i3] + b[i3];
        __syncthreads();
    for (int i4 = 0; i4 < ub; i4++) {
        out[threadIdx.x + i4] = a[i4] + b[i4];
        __syncthreads();
    for (int i5 = 0; i5 < ub; i5++) {
        out[threadIdx.x + i5] = a[i5] + b[i5];
        __syncthreads();
    for (int i6 = 0; i6 < ub; i6++) {
        out[threadIdx.x + i6] = a[i6] + b[i6];
        __syncthreads();
    for (int i7 = 0; i7 < ub; i7++) {
        out[threadIdx.x + i7] = a[i7] + b[i7];
        __syncthreads();
    for (int i8 = 0; i8 < ub; i8++) {
        out[threadIdx.x + i8] = a[i8] + b[i8];
        __syncthreads();
    for (int i9 = 0; i9 < ub; i9++) {
        out[threadIdx.x + i9] = a[i9] + b[i9];
        __syncthreads();
    for (int i10 = 0; i10 < ub; i10++) {
        out[threadIdx.x + i10] = a[i10] + b[i10];
        __syncthreads();
    for (int i11 = 0; i11 < ub; i11++) {
        out[threadIdx.x + i11] = a[i11] + b[i11];
        __syncthreads();
    for (int i12 = 0; i12 < ub; i12++) {
        out[threadIdx.x + i12] = a[i12] + b[i12];
        __syncthreads();
    for (int i13 = 0; i13 < ub; i13++) {
        out[threadIdx.x + i13] = a[i13] + b[i13];
        __syncthreads();
    for (int i14 = 0; i14 < ub; i14++) {
        out[threadIdx.x + i14] = a[i14] + b[i14];
        __syncthreads();
    for (int i15 = 0; i15 < ub; i15++) {
        out[threadIdx.x + i15] = a[i15] + b[i15];
        __syncthreads();
    for (int i16 = 0; i16 < ub; i16++) {
        out[threadIdx.x + i16] = a[i16] + b[i16];
        __syncthreads();
    for (int i17 = 0; i17 < ub; i17++) {
        out[threadIdx.x + i17] = a[i17] + b[i17];
        __syncthreads();
    for (int i18 = 0; i18 < ub; i18++) {
        out[threadIdx.x + i18] = a[i18] + b[i18];
        __syncthreads();
    for (int i19 = 0; i19 < ub; i19++) {
        out[threadIdx.x + i19] = a[i19] + b[i19];
        __syncthreads();
    for (int i20 = 0; i20 < ub; i20++) {
        out[threadIdx.x + i20] = a[i20] + b[i20];
        __syncthreads();
    for (int i21 = 0; i21 < ub; i21++) {
        out[threadIdx.x + i21] = a[i21] + b[i21];
        __syncthreads();
    for (int i22 = 0; i22 < ub; i22++) {
        out[threadIdx.x + i22] = a[i22] + b[i22];
        __syncthreads();
    for (int i23 = 0; i23 < ub; i23++) {
        out[threadIdx.x + i23] = a[i23] + b[i23];
        __syncthreads();
    for (int i24 = 0; i24 < ub; i24++) {
        out[threadIdx.x + i24] = a[i24] + b[i24];
        __syncthreads();
    for (int i25 = 0; i25 < ub; i25++) {
        out[threadIdx.x + i25] = a[i25] + b[i25];
        __syncthreads();
    for (int i26 = 0; i26 < ub; i26++) {
        out[threadIdx.x + i26] = a[i26] + b[i26];
        __syncthreads();
    for (int i27 = 0; i27 < ub; i27++) {
        out[threadIdx.x + i27] = a[i27] + b[i27];
        __syncthreads();
    for (int i28 = 0; i28 < ub; i28++) {
        out[threadIdx.x + i28] = a[i28] + b[i28];
        __syncthreads();
    for (int i29 = 0; i29 < ub; i29++) {
        out[threadIdx.x + i29] = a[i29] + b[i29];
        __syncthreads();
    for (int i30 = 0; i30 < ub; i30++) {
        out[threadIdx.x + i30] = a[i30] + b[i30];
        __syncthreads();
    for (int i31 = 0; i31 < ub; i31++) {
        out[threadIdx.x + i31] = a[i31] + b[i31];
        __syncthreads();
    for (int i32 = 0; i32 < ub; i32++) {
        out[threadIdx.x + i32] = a[i32] + b[i32];
        __syncthreads();
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }

}
