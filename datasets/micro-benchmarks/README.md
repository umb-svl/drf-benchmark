# `benchmark-graph.py`

Example usage:
```
python3 benchmark-graph.py"
```

Micro-benchmark .csv path must be in the same directory as `benchmark-graph.py`. The csv filename must be `timings-<toolname>-<lower range>-<upper range>.csv`.
Micro-benchmark kernel filenames (supplied in the .csv file) must be in the form:

- `barriers-<toolname>-<n>.cu`
- `ifs-<toolname>-<n>.cu`
- `nested-loops-<toolname>-<n>.cu`
- `nested-loops-sync-<toolname>-<n>.cu`
- `serial-loops-<toolname>-<n>.cu`
*<toolname> is the name of the verification tool. e.g. faial.*
*<n> is the size of the variable. `nested-loops-<toolname>-3.cu` means there is 3 nested loops in this kernel.*
*Exception: PUG kernels must be `.c` instead of `.cu`*
