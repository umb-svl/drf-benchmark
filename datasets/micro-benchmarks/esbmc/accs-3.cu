#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 16 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 38 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 35 * blockDim.x ];

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out);
    return 0;
}
