#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out, float* a, float* b) {



    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();
    out[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
    __syncthreads();

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out, a, b);
    return 0;
}
