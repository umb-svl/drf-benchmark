#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 29 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 56 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 36 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 55 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 60 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 50 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 30 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 29 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 33 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 55 * blockDim.x ];
    out[threadIdx.x + 10 * blockDim.x ] = out[threadIdx.x + 38 * blockDim.x ];

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out);
    return 0;
}
