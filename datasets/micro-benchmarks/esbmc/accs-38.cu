#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 41 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 27 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 28 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 49 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 47 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 7 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 5 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 24 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 45 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 30 * blockDim.x ];
    out[threadIdx.x + 10 * blockDim.x ] = out[threadIdx.x + 24 * blockDim.x ];
    out[threadIdx.x + 11 * blockDim.x ] = out[threadIdx.x + 3 * blockDim.x ];
    out[threadIdx.x + 12 * blockDim.x ] = out[threadIdx.x + 39 * blockDim.x ];
    out[threadIdx.x + 13 * blockDim.x ] = out[threadIdx.x + 43 * blockDim.x ];
    out[threadIdx.x + 14 * blockDim.x ] = out[threadIdx.x + 11 * blockDim.x ];
    out[threadIdx.x + 15 * blockDim.x ] = out[threadIdx.x + 38 * blockDim.x ];
    out[threadIdx.x + 16 * blockDim.x ] = out[threadIdx.x + 23 * blockDim.x ];
    out[threadIdx.x + 17 * blockDim.x ] = out[threadIdx.x + 21 * blockDim.x ];
    out[threadIdx.x + 18 * blockDim.x ] = out[threadIdx.x + 18 * blockDim.x ];
    out[threadIdx.x + 19 * blockDim.x ] = out[threadIdx.x + 46 * blockDim.x ];
    out[threadIdx.x + 20 * blockDim.x ] = out[threadIdx.x + 22 * blockDim.x ];
    out[threadIdx.x + 21 * blockDim.x ] = out[threadIdx.x + 40 * blockDim.x ];
    out[threadIdx.x + 22 * blockDim.x ] = out[threadIdx.x + 47 * blockDim.x ];
    out[threadIdx.x + 23 * blockDim.x ] = out[threadIdx.x + 20 * blockDim.x ];
    out[threadIdx.x + 24 * blockDim.x ] = out[threadIdx.x + 31 * blockDim.x ];
    out[threadIdx.x + 25 * blockDim.x ] = out[threadIdx.x + 62 * blockDim.x ];
    out[threadIdx.x + 26 * blockDim.x ] = out[threadIdx.x + 49 * blockDim.x ];
    out[threadIdx.x + 27 * blockDim.x ] = out[threadIdx.x + 38 * blockDim.x ];
    out[threadIdx.x + 28 * blockDim.x ] = out[threadIdx.x + 26 * blockDim.x ];
    out[threadIdx.x + 29 * blockDim.x ] = out[threadIdx.x + 15 * blockDim.x ];
    out[threadIdx.x + 30 * blockDim.x ] = out[threadIdx.x + 60 * blockDim.x ];
    out[threadIdx.x + 31 * blockDim.x ] = out[threadIdx.x + 40 * blockDim.x ];
    out[threadIdx.x + 32 * blockDim.x ] = out[threadIdx.x + 6 * blockDim.x ];
    out[threadIdx.x + 33 * blockDim.x ] = out[threadIdx.x + 34 * blockDim.x ];
    out[threadIdx.x + 34 * blockDim.x ] = out[threadIdx.x + 32 * blockDim.x ];
    out[threadIdx.x + 35 * blockDim.x ] = out[threadIdx.x + 10 * blockDim.x ];
    out[threadIdx.x + 36 * blockDim.x ] = out[threadIdx.x + 28 * blockDim.x ];
    out[threadIdx.x + 37 * blockDim.x ] = out[threadIdx.x + 34 * blockDim.x ];

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out);
    return 0;
}
