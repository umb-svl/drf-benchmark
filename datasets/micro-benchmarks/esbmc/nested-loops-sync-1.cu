#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>
int ub;







__global__ void kernel (float* out, float* a, float* b) {



    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x + i0] = a[i0] + b[i0];
        __syncthreads();
    }

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    ub = nondet_int();
    ESBMC_verify_kernel(kernel, 1, 64, out, a, b);
    return 0;
}
