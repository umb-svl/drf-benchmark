#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 62 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 14 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 1 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 34 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 48 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 3 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 11 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 59 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 16 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 2 * blockDim.x ];
    out[threadIdx.x + 10 * blockDim.x ] = out[threadIdx.x + 4 * blockDim.x ];
    out[threadIdx.x + 11 * blockDim.x ] = out[threadIdx.x + 57 * blockDim.x ];
    out[threadIdx.x + 12 * blockDim.x ] = out[threadIdx.x + 52 * blockDim.x ];
    out[threadIdx.x + 13 * blockDim.x ] = out[threadIdx.x + 44 * blockDim.x ];
    out[threadIdx.x + 14 * blockDim.x ] = out[threadIdx.x + 10 * blockDim.x ];

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out);
    return 0;
}
