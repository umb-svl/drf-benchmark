#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>
int ub;







__global__ void kernel (float* out, float* a, float* b) {

    for (int i0 = 0; i0 < ub; i0++) {
        out[threadIdx.x + i0] = a[i0] + b[i0];
        __syncthreads();
    }
    for (int i1 = 0; i1 < ub; i1++) {
        out[threadIdx.x + i1] = a[i1] + b[i1];
        __syncthreads();
    }
    for (int i2 = 0; i2 < ub; i2++) {
        out[threadIdx.x + i2] = a[i2] + b[i2];
        __syncthreads();
    }
    for (int i3 = 0; i3 < ub; i3++) {
        out[threadIdx.x + i3] = a[i3] + b[i3];
        __syncthreads();
    }
    for (int i4 = 0; i4 < ub; i4++) {
        out[threadIdx.x + i4] = a[i4] + b[i4];
        __syncthreads();
    }
    for (int i5 = 0; i5 < ub; i5++) {
        out[threadIdx.x + i5] = a[i5] + b[i5];
        __syncthreads();
    }
    for (int i6 = 0; i6 < ub; i6++) {
        out[threadIdx.x + i6] = a[i6] + b[i6];
        __syncthreads();
    }
    for (int i7 = 0; i7 < ub; i7++) {
        out[threadIdx.x + i7] = a[i7] + b[i7];
        __syncthreads();
    }
    for (int i8 = 0; i8 < ub; i8++) {
        out[threadIdx.x + i8] = a[i8] + b[i8];
        __syncthreads();
    }
    for (int i9 = 0; i9 < ub; i9++) {
        out[threadIdx.x + i9] = a[i9] + b[i9];
        __syncthreads();
    }
    for (int i10 = 0; i10 < ub; i10++) {
        out[threadIdx.x + i10] = a[i10] + b[i10];
        __syncthreads();
    }
    for (int i11 = 0; i11 < ub; i11++) {
        out[threadIdx.x + i11] = a[i11] + b[i11];
        __syncthreads();
    }
    for (int i12 = 0; i12 < ub; i12++) {
        out[threadIdx.x + i12] = a[i12] + b[i12];
        __syncthreads();
    }
    for (int i13 = 0; i13 < ub; i13++) {
        out[threadIdx.x + i13] = a[i13] + b[i13];
        __syncthreads();
    }


}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    ub = nondet_int();
    ESBMC_verify_kernel(kernel, 1, 64, out, a, b);
    return 0;
}
