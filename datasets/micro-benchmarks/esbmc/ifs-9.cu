#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>







__global__ void kernel (float* out, float* a, float* b) {



    if (threadIdx.x == 0) {
        out[threadIdx.x] = a[0] + b[0];
    }
    if (threadIdx.x == 1) {
        out[threadIdx.x] = a[1] + b[1];
    }
    if (threadIdx.x == 2) {
        out[threadIdx.x] = a[2] + b[2];
    }
    if (threadIdx.x == 3) {
        out[threadIdx.x] = a[3] + b[3];
    }
    if (threadIdx.x == 4) {
        out[threadIdx.x] = a[4] + b[4];
    }
    if (threadIdx.x == 5) {
        out[threadIdx.x] = a[5] + b[5];
    }
    if (threadIdx.x == 6) {
        out[threadIdx.x] = a[6] + b[6];
    }
    if (threadIdx.x == 7) {
        out[threadIdx.x] = a[7] + b[7];
    }
    if (threadIdx.x == 8) {
        out[threadIdx.x] = a[8] + b[8];
    }

}


int main () {
    float *out;
    cudaMalloc((void**)&out, 1024 * sizeof(float));
    float *a;
    cudaMalloc((void**)&a, 1024 * sizeof(float));
    float *b;
    cudaMalloc((void**)&b, 1024 * sizeof(float));
    ESBMC_verify_kernel(kernel, 1, 64, out, a, b);
    return 0;
}
