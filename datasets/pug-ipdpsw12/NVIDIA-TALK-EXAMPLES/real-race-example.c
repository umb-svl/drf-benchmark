// #include <stdio.h>
// #include <stdlib.h>
// #include <cuda_runtime.h>

#include "my_cutil.h"
#include "math.h"

/*  #define K1 // unsupported syntax (nonlinear function calls) */
/*  #define K2 */
/*  #define K3 // +R */
/*  #define K4 // Found a harmless race */
  #define K5 //<-- Found multiple real races (of the same type) which can be attributed to no barrier in the loops 

/*  #define K6 */
/*  #define K7 */
/*  #define K8 */
/*  #define K9 */
/*  #define K10 */

/*  #define K11 // the same as K4 */
/*  #define K12 */
/*  #define K13 // the same as K4 */
/*  #define K14 */
/*  #define K15 */

/*  #define K16 */
/*  #define K17 // +P */
/*  #define K18 */
/*  #define K19 */
/*  #define K20 */

/*  #define K21 */
/*  #define K22 */

/*  #define K23 */
/*  #define K24 */
/*  #define K25 */
/*  #define K26 */

/*  #define K27 */
/*  #define K28 */
/*  #define K29 // Found real synchronization errors */


/*****************************************************************/
/*          Kernel 1                                             */
/*****************************************************************/

#ifdef K1 

#define SIZE 256
#define BLOCKSIZE 32
#define SEARCH_NUMBER 6


__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {
  int pos, incr;

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],SEARCH_NUMBER);
  }
  
  //sync the threads before the reduction
  __syncthreads();
  
  //height of the binary tree given by log2(#leaves)
  for(int i=0; i<6; i++)
  {
    //math
    pos = (int)pow(2.0,i+1);
    incr = (int)pow(2.0,i);
	
	//if this is the thread which needs to be updated
	if (threadIdx.x % pos == 0)
	{
	  d_out[threadIdx.x] += d_out[threadIdx.x + incr]; 
	}
	
	//sync the threads before starting the next iteration
	__syncthreads();
  }
  
}

#endif


/*****************************************************************/
/*          Kernel 2                                             */
/*****************************************************************/


#ifdef K2

#define SIZE 256
#define BLOCKSIZE 32


__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void compute(int *d_in,int *d_out) 
{
  d_out[blockIdx.x * BLOCKSIZE + threadIdx.x] = 0;
  d_out[blockIdx.x * BLOCKSIZE + threadIdx.x] = compare(d_in[blockIdx.x * BLOCKSIZE + threadIdx.x],6);
}

__global__ void reduceKernel(int *d_out, int rIndex)
{
   int i = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
  int jump = rIndex / 2;
  
  if( (i == 0) || ( (i / rIndex != 0) && (i % rIndex == 0) )  )
    {
      int j = (blockIdx.x * BLOCKSIZE + threadIdx.x) + jump;
      d_out[i] += d_out[j];						
    }
}


#endif


/*****************************************************************/
/*          Kernel 3                                             */
/*****************************************************************/

#ifdef K3 

#define SIZE 256
#define BLOCKSIZE 32
#define COMPARE_VAL 6 // what number should be counted! used in both GPU and CPU calculations

// Count the number of 6's!
__global__ void count6sKernel(int *inArray, int *threadArray, int* count) {
  threadArray[threadIdx.x] = 0;
  
  for(int i = 0; i < (SIZE/BLOCKSIZE); i++) {
    int val = inArray[i*BLOCKSIZE+threadIdx.x];
    if(val == COMPARE_VAL)
      threadArray[threadIdx.x]++;
  }

  __syncthreads();

  int i = 1;
  for(int k = BLOCKSIZE/2; k >= 1; k /= 2) {
    assume (i * k == BLOCKSIZE/2);     // refinement
    int kRev = BLOCKSIZE / k;
    if((threadIdx.x % kRev) == 0) {
      threadArray[threadIdx.x] = threadArray[threadIdx.x+i];
    }
    i *= 2;
    __syncthreads();
  }
  
  if(threadIdx.x == 0)
    *count = threadArray[0];
}

#endif


/*****************************************************************/
/*          Kernel 4                                             */
/*****************************************************************/

#ifdef K4 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) 
{
  if (a == b) 
    return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out, int* d_sum) 
{
  d_out[threadIdx.x] = 0;
  for (int i = 0; i < SIZE / BLOCKSIZE; i++) 
    {
      d_out[threadIdx.x] += compare(d_in[i * BLOCKSIZE + threadIdx.x], 6);
    }
  
  __syncthreads();
  
  // for(int i = 2, j = 1; i <= BLOCKSIZE; i *= 2, j *= 2)
  for(int i = 2; i <= BLOCKSIZE; i *= 2)
    {
      int j = i / 2;
      if(threadIdx.x % i == 0)
	{
	  d_out[threadIdx.x] += d_out[threadIdx.x + j];
	}
      __syncthreads();
    }
  
   *d_sum = d_out[0];       // a careless race */
}


#endif


/*****************************************************************/
/*          Kernel 5                                             */
/*****************************************************************/

#ifdef K5 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out, int *d_sum) {
  // *d_sum=0;
  
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  __syncthreads();

  /*if(threadIdx.x==0) {
	for(i=0;i<BLOCKSIZE;i++) {
		*d_sum+=d_out[i];
		}
	}*/
  assume(blockDim.x <= BLOCKSIZE / 2);  // for testing

  if(threadIdx.x%2==0) {
    for(int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x+SIZE/BLOCKSIZE*i]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+1];

      /*
      The counter example given by PUG is:
      t1.x = 2, t2.x = 10, i@t1 = 1, i@t2 = 0,
      that is,
      d_out[threadIdx.x+8*i]+=d_out[threadIdx.x+8*i+1];
      d_out[2+8*1]+=d_out[10+8*0+1];
      d_out[10]+=d_out[10]    a race!!!
      Another counterexample is (t1.x = 0, t2.x = 8)
      */
    }
  }
  __syncthreads();

  // similar races occur in the following code
  if(threadIdx.x==0||threadIdx.x==4) {
    for(int i=0; i<SIZE/BLOCKSIZE; i++) {
		d_out[threadIdx.x+SIZE/BLOCKSIZE*i]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+2];
	}
  }
  __syncthreads();
  if(threadIdx.x==0) {
	for(int i=0; i<SIZE/BLOCKSIZE; i++){
		d_out[threadIdx.x]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+4];
	}
  }
  __syncthreads();
  // *d_sum=*d_out;
}


#endif


/*****************************************************************/
/*          Kernel 6                                             */
/*****************************************************************/

#ifdef K6 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKnel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
}

__global__ void convergeKernel(int *d_out, int scope, int gap) {
	int initPoint = threadIdx.x * scope;
	int endPoint = initPoint + scope;
	
	for (int i = initPoint+gap ; i < endPoint ; i += gap) {
		d_out[initPoint] += d_out[i];
	}
}


#endif


/*****************************************************************/
/*          Kernel 7                                             */
/*****************************************************************/

#ifdef K7 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}


__global__ void computeKernel(int *d_in, int *d_out) {
  
  /* Each thread collects count of 6s for its set of numbers */
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
    d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  
  /* Gather results in parallel */
  int m = 2;
  // int logThreads = ceilf(log2f(BLOCKSIZE));
  int logThreads = 6;
  for (int i=0; i<logThreads; i++) {
    /* MUST sync between each level of merging tree values, or race conditions can affect results */
    __syncthreads();
    if ((threadIdx.x & (m-1)) == 0) { /* when m is a power of 2 (always is here), (i&(m-1)) is equivalent to (i%m), but more efficient */
      /* index to merge with is (m/2) slots away in the array */
      int mergeIdx = threadIdx.x + (m>>1);
      if (mergeIdx < BLOCKSIZE) {
        d_out[threadIdx.x] += d_out[mergeIdx];
      }
    }
    /* Double m for next level of merging */
    m <<= 1;
  }
}

#endif


/*****************************************************************/
/*          Kernel 8                                             */
/*****************************************************************/

#ifdef K8 

#define SIZE 256
/* The number of threads that are used to sum. */
#define BLOCKSIZE 32

/*
 * compare
 * A simple compare function that executes on the device. It will return 1 if
 * a == b, otherwise 0 is returned.
 */
__device__ int compare(int a, int b)
{
    if (a == b)
        return 1;
    else
        return 0;
}

/*
 * compute
 * This function is executed on the device. It uses BLOCKSIZE threads to sum
 * the number of 6's in the input array. It then reduces this sum in parallel
 * so the final sum will be stored in d_out[0] at the end of the function call.
 */
__global__ void computeKernel(int *d_in, int *d_out)
{
    /* Each thread calculates their sum. */
    d_out[threadIdx.x] = 0;
    for (int i = 0; i < SIZE/BLOCKSIZE; i++)
    {
        d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
    }
  
    /* Use the nodes in parallel to reduce the answer to the final sum. */
    for (int i = 1; i < BLOCKSIZE; i *= 2)
    {
        __syncthreads();
        if (threadIdx.x % (i*2) == 0)
        {
            d_out[threadIdx.x] += d_out[threadIdx.x + i];
        }
    }
}


#endif


/*****************************************************************/
/*          Kernel 9                                             */
/*****************************************************************/

#ifdef K9 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

/* __global__ void compute(int *d_in,int *d_out) { */
/*   int i; */

/*   d_out[threadIdx.x] = 0; */
/*   for (i=0; i<SIZE/BLOCKSIZE; i++) { */
/*       d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6); */
/*   } */
/*   __syncthreads(); // wait for all threads to finish counting their 6's */
/*   int useNumThreads = BLOCKSIZE / 2; */
/*    while(useNumThreads>0) */
/*   { */
/*         int combinedSize = BLOCKSIZE / useNumThreads; */
/*         if(threadIdx.x % combinedSize == 0) */
/*         { */
/*                 d_out[threadIdx.x] = d_out[threadIdx.x] + d_out[threadIdx.x + (combinedSize /2)]; */
/*                 useNumThreads = useNumThreads/2; */

/*         } */
/*         __syncthreads(); */
/*   } */

/*   //for(i=0;i<SIZE/BLOCKSIZE;i++) { */
/*           printf("\nblock id.x : %d \t blockIx.y : %d \t", blockIdx.x,blockIdx.y); */
/*           printf("Thread Id.x  : %d \t ThreadIx.y : %d\n", threadIdx.x, threadIdx.y); */
/*   //} */
/*   getchar(); */

/* } */

__global__ void computeKernel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }

  __syncthreads(); // wait for all threads to finish counting their 6's

  for (int useNumThreads = BLOCKSIZE / 2; useNumThreads > 0; useNumThreads >>= 1) 
  {
    int combinedSize = BLOCKSIZE / useNumThreads;
    if(threadIdx.x % combinedSize == 0)
      {
	d_out[threadIdx.x] = d_out[threadIdx.x] + d_out[threadIdx.x + (combinedSize /2)];	
      }	
    __syncthreads(); 
  }

/*   //for(i=0;i<SIZE/BLOCKSIZE;i++) { */
/* 	  printf("\nblock id.x : %d \t blockIx.y : %d \t", blockIdx.x,blockIdx.y); */
/* 	  printf("Thread Id.x  : %d \t ThreadIx.y : %d\n", threadIdx.x, threadIdx.y); */
/*   //} */
/*   getchar(); */

}


#endif


/*****************************************************************/
/*          Kernel 10                                            */
/*****************************************************************/

#ifdef K10 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }

  __syncthreads();

  for(int i = 2; i <= BLOCKSIZE; i*=2)
    {
      if(threadIdx.x % i == 0)
	{
	  d_out[threadIdx.x] += d_out[threadIdx.x + (i/2)];
	}
      __syncthreads();
    }

  //Or unrolled below... for speed

	if(threadIdx.x % 2 == 0)
	{
		d_out[threadIdx.x] += d_out[threadIdx.x + 1];
	}

	__syncthreads();

	if(threadIdx.x % 4 == 0)
	{
		d_out[threadIdx.x] += d_out[threadIdx.x + 2];
	}

	__syncthreads();

	if(threadIdx.x % 8 == 0)
	{
		d_out[threadIdx.x] += d_out[threadIdx.x + 4];
	}

	__syncthreads();

	if(threadIdx.x % 16 == 0)
	{
		d_out[threadIdx.x] += d_out[threadIdx.x + 8];
	}

	__syncthreads();

	if(threadIdx.x % 32 == 0)
	{
		d_out[threadIdx.x] += d_out[threadIdx.x + 16];
	}

	//__syncthreads();
  
}


#endif


/*****************************************************************/
/*          Kernel 11                                            */
/*****************************************************************/

#ifdef K11 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out, int* d_sum) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  
  __syncthreads();
  
  // for(int i=2, j=1; i <= BLOCKSIZE; i*=2, j*=2){
  for(int i = 2; i <= BLOCKSIZE; i *= 2)
    {
      int j = i / 2;
      if(threadIdx.x % i == 0)
	{
	  d_out[threadIdx.x] += d_out[threadIdx.x + j];
	}
      __syncthreads();
    }
  *d_sum = d_out[0];
  
}


#endif

/*****************************************************************/
/*          Kernel 12                                            */
/*****************************************************************/

#ifdef K12 

#define SRC_ARRAY_SIZE 256
#define BLOCKSIZE 32
#define LOG_BLOCKSIZE 5

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in, int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SRC_ARRAY_SIZE/BLOCKSIZE; i++) {
      //d_out[threadIdx.x] += ( (d_in[i*BLOCKSIZE+threadIdx.x] == 6) ? 1 : 0 );
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
}

#define IS_RECEIVER(x, mask) (((x) < (mask)))
#define SENDER(tid, mask) (((tid) | (mask)))
/* supports 2^^LOG_BLOCKSIZE participating threads */
__global__ void reduceKernel(int* d_out) {
  unsigned int tid = threadIdx.x;
  for (int round = LOG_BLOCKSIZE - 1; round >=0; round--) {
    // unsigned int round_mask = (1 << round); // unsupported by Yices
    unsigned int round_mask = round;
    __syncthreads();
    if (IS_RECEIVER(tid, round_mask)) {
        d_out[tid] += d_out[SENDER(tid, round_mask)];
    }
  }
}


#endif

/*****************************************************************/
/*          Kernel 13                                            */
/*****************************************************************/

#ifdef K13 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void compute(int *d_in,int *d_out, int* d_sum) {
  int i;

  d_out[threadIdx.x] = 0;
  for (i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  
  __syncthreads();
  
  for(int i=2, j=1; i <= BLOCKSIZE; i*=2, j*=2){
	if(threadIdx.x % i == 0){
		d_out[threadIdx.x] += d_out[threadIdx.x + j];
	}
	__syncthreads();
  }
  
  *d_sum = d_out[0];
  
}


#endif


/*****************************************************************/
/*          Kernel 14                                            */
/*****************************************************************/

#ifdef K14 

#define BLOCKSIZE 	32	
#define NUMBER 		6
#define SIZE		256

__device__ int compare(int a, int b) 
{
  if (a == b) 
    {
      return 1;
    }
  
  return 0;
}

__global__ void computeKernel(int* d_in, int* d_out, int* sum) 
{
	d_out[threadIdx.x] = 0;

	for (int i=0; i<SIZE/BLOCKSIZE; i++) 
	{
	  d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x], NUMBER);
	}

	__syncthreads();

/* 	while (n <= SIZE) */
/* 	{ */
/* 		if ((threadIdx.x % n) == 0) */
/* 		{ */
/* 			if (threadIdx.x + n/2 <= SIZE) */
/* 			{ */
/* 				d_out[threadIdx.x] += d_out[threadIdx.x + n/2]; */
/* 			} */
/* 		}	 */

/* 		n = n * 2; */
/* 		__syncthreads(); */
/* 	} */

	for (int n = 2; n <= SIZE; n *= 2)
	{
	  if ((threadIdx.x % n) == 0)
	    {
	      if (threadIdx.x + n/2 <= SIZE)
		{
		  d_out[threadIdx.x] += d_out[threadIdx.x + n/2];
		}
	    }
	  __syncthreads();
	}

	__syncthreads();
	
	if (threadIdx.x == 0)
	{
		(*sum) = d_out[0];
	}
}


#endif


/*****************************************************************/
/*          Kernel 15                                            */
/*****************************************************************/

#ifdef K15 

#define INIT_SIZE 256
#define INIT_BLOCKSIZE 32

/* __global__ void computeKernel(int *d_in,int *d_out) { */

/*   int blocksize = INIT_BLOCKSIZE; */
/*   int size = INIT_SIZE; */
/*   int coalesce_num = size/blocksize; */
/* /\*   d_out[threadIdx.x] = 0; *\/ */
/* /\*   // 256->32 *\/ */
/* /\*   for (int j=0; j < coalesce_num; j++) *\/ */
/* /\*     d_out[threadIdx.x] += (d_in[j * blocksize + threadIdx.x] == 6) ? 1 : 0; *\/ */
  
/*   //32->16, 16->8, 8->4, 4->2, 2->1 */
/*   for (int i=0; i<5; i++){ */
/*     __syncthreads(); */
/*     blocksize = blocksize/2; */
/*     __syncthreads(); */
    
/*     if (threadIdx.x < blocksize){ */
/*       d_out[threadIdx.x] += d_out[ blocksize + threadIdx.x]; */
/*     } */
/*   } */
/* } */

__global__ void computeKernel(int *d_in,int *d_out, int blocksize) {

  int size = INIT_SIZE;
  int coalesce_num = size/blocksize;
  d_out[threadIdx.x] = 0;
  // 256->32
  for (int j=0; j < coalesce_num; j++)
    d_out[threadIdx.x] += (d_in[j * blocksize + threadIdx.x] == 6) ? 1 : 0;
  
  //32->16, 16->8, 8->4, 4->2, 2->1
  for (int i=0; i<5; i++){
    __syncthreads();
    blocksize = blocksize/2;
    __syncthreads();
    
    if (threadIdx.x < blocksize){
      d_out[threadIdx.x] += d_out[ blocksize + threadIdx.x];
    }
  }
}

#endif


/*****************************************************************/
/*          Kernel 16                                            */
/*****************************************************************/

#ifdef K16 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

/***************************************************************/
__global__ void computeKernel(int *d_in,int *d_out) {
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
}

/***************************************************************/
__global__ void calcSumKernel(int *d_in,int *d_out) {
  int i;

  i = threadIdx.x * 2;
  d_out[threadIdx.x] = d_in[i] + d_in [i+1];
}


#endif


/*****************************************************************/
/*          Kernel 17                                            */
/*****************************************************************/

#ifdef K17 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {

  assume (blockDim.x <= BLOCKSIZE);

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  
  __syncthreads();

/*   int block = BLOCKSIZE; */
/*   while(block != 1) { */
/*      int x = BLOCKSIZE/block; */
/*      if(threadIdx.x % (2*x) == 0) { */
/*          d_out[threadIdx.x] = d_out[threadIdx.x]+d_out[threadIdx.x + x]; */
/*      } */
/*      __syncthreads(); */
/*      block = block/2; */
/*   } */

  for (int block = BLOCKSIZE; block > 1; block >>= 1) {
    int x = BLOCKSIZE/block;
    if (threadIdx.x % (2*x) == 0) {
      // race when x = 1 ( i.e. block = BLOCKSIZE) where t1.x = 1 and t2.x = 0
      d_out[threadIdx.x] = d_out[threadIdx.x]+d_out[threadIdx.x + x];
    }
    __syncthreads();
  }

}


#endif


/*****************************************************************/
/*          Kernel 18                                            */
/*****************************************************************/

#ifdef K18 

#define SIZE 256
#define BLOCKSIZE 32

__global__ void deviceMatComputeKernel(int *A,int *B, int *C,int N,int M) {
  // Block index
  int bx = blockIdx.x;
  int by = blockIdx.y;
  // Thread index
  int tx = threadIdx.x;
  
  for(int i=0; i<32; i++){
    int index = M*by*32+bx*64+tx+i*M;
    C[index] = A[index] + B[index];
    __syncthreads();
  }

}


#endif


/*****************************************************************/
/*          Kernel 19                                            */
/*****************************************************************/

#ifdef K19 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out, int *d_sum) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
    d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }

  for(int i = 1; i <= BLOCKSIZE/2; i*=2) {
    __syncthreads();
    if(threadIdx.x % (i*2) == 0) {
      d_out[threadIdx.x] += d_out[threadIdx.x + i];
    }
  }
  if(threadIdx.x == 0) {
    *d_sum = d_out[0];
  }
}


#endif


/*****************************************************************/
/*          Kernel 20                                            */
/*****************************************************************/

#ifdef K20

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__device__ int add(int a, int b) {
  return a+b;
}

__global__ void computeKernel(int *d_in,int *d_out) {

 /* compute number of appearances of 6 for subset of data in each thread! */
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
    d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }

  /*reduce all results to d_out[0]*/
  for (int i=1; i<BLOCKSIZE; i=i*2){
    __syncthreads();
    if (threadIdx.x % (2*i) == 0)
      d_out[threadIdx.x] = add(d_out[threadIdx.x], d_out[threadIdx.x+i]);
  }
  
}


#endif


/*****************************************************************/
/*          Kernel 21                                            */
/*****************************************************************/

#ifdef K21

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}


__global__ void computeKernel(int *d_in,int *d_out) {
  
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[(SIZE/BLOCKSIZE)*threadIdx.x+i],6);
  }

  __syncthreads();

/*   int base = 2; */
/*   /\* number of  times the loop should run or Depth or tree */
/*      is log2 of the BLOCKSIZE */
/*   *\/ */
/*   int count = 5; */
  
/*   while(count) { */
    
/*     if (threadIdx.x%base==0) */
/*       { */
/* 	d_out[threadIdx.x]+=d_out[threadIdx.x+(base/2)]; */
	
/*       } */
/*     __syncthreads(); */
    
/*     count--; */
/*     base*=2; */
/*   } */

  int base = 2;
  /* number of  times the loop should run or Depth or tree
     is log2 of the BLOCKSIZE
  */
  for (int count = 5; count > 0; count--) {
    if (threadIdx.x%base==0)
      {
	d_out[threadIdx.x]+=d_out[threadIdx.x+(base/2)];
	
      }
    __syncthreads();
    base *= 2;
  }

}


#endif


/*****************************************************************/
/*          Kernel 22                                            */
/*****************************************************************/

#ifdef K22

#define SIZE 256
#define BLOCKSIZE 32
#define NUMBER 6

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],NUMBER);
  }
  
  __syncthreads();
  
/*   for (int a=2, i=BLOCKSIZE; i>1; a*=2, i=i/2) { */
/*     if (threadIdx.x % a == 0) { */
/*       d_out[threadIdx.x] += d_out[threadIdx.x + (a/2)]; */
/*     } */
/*     __syncthreads(); */
/*   } */

  int a = 2;
  for (int i=BLOCKSIZE; i>1; i=i/2) {
    if (threadIdx.x % a == 0) {
      d_out[threadIdx.x] += d_out[threadIdx.x + (a/2)];
    }
    a*=2;
    __syncthreads();
  }

}


#endif

/*****************************************************************/
/*          Kernel 23                                            */
/*****************************************************************/

#ifdef K23 

#define SIZE 256
#define BLOCKSIZE 32
//#define SIZE 16
//#define BLOCKSIZE 4

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  __syncthreads();

  //sum up results
  for (int i=2; i<=BLOCKSIZE; i*=2) {
    if (threadIdx.x%i == 0) {
      d_out[threadIdx.x] += d_out[threadIdx.x+i/2];
    }
    __syncthreads();
  }
}


#endif


/*****************************************************************/
/*          Kernel 24                                            */
/*****************************************************************/

#ifdef K24

#define SIZE 256
#define BLOCKSIZE 32
#define KERNELSIZE 8

__device__ int d_arr[SIZE];
__device__ int d_count;
__device__ int d_lcount[BLOCKSIZE];

__global__ void computeKernel(int* d_arr, int* d_lcount, int d_count) {
  int si = threadIdx.x*KERNELSIZE;
  int ei = (threadIdx.x+1)*KERNELSIZE;

  d_lcount[threadIdx.x] = 0;
  for(int i=si; i<ei; i++)
    if(d_arr[i] == 6) d_lcount[threadIdx.x]++;

  __syncthreads();
  for(int i=2; i<=BLOCKSIZE; i*=2) {
    if(!(threadIdx.x % i))
      d_lcount[threadIdx.x] += d_lcount[threadIdx.x+i/2];
    __syncthreads();
  }

  if(!threadIdx.x)
    d_count = d_lcount[threadIdx.x];
}


#endif


/*****************************************************************/
/*          Kernel 25                                            */
/*****************************************************************/

#ifdef K25

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  return (a == b) ? 1 : 0;
}

__shared__ int localSums[BLOCKSIZE];

__global__ void computeKernel(int *d_in, int* d_sum) {
  int localSum = 0;

  for(int i=0; i < SIZE/BLOCKSIZE; i++) {
    localSum += compare(d_in[i*BLOCKSIZE+threadIdx.x], 6);
  }
  localSums[threadIdx.x] = localSum;

  // Wait for all threads to finish computation
  __syncthreads();

  // Gather the results in a tree-like fashion
  for(int stride = 1; stride < BLOCKSIZE; stride*=2)
  {
    if((threadIdx.x%(stride*2)) == 0)
    {
      localSums[threadIdx.x] += localSums[threadIdx.x + stride];
    }
    __syncthreads();
  }

  // Put the final result in the sum variable
  if(threadIdx.x == 0)
  {
    d_sum[0] = localSums[0];
  }
}


#endif


/*****************************************************************/
/*          Kernel 26                                            */
/*****************************************************************/

#ifdef K26

#define SIZE 256
#define BLOCKSIZE 32
#define VALUE 6

__device__ int compare(int a, int b) 
{ 
  if (a == b) 
    return 1; 
  return 0; 
}


__global__ void computeKnel(int *d_in,int *d_out)
{
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) { 
    int val = d_in[i*BLOCKSIZE + threadIdx.x];
    d_out[threadIdx.x] += compare(val, VALUE);
  } 
  __syncthreads();
}

__global__ void addAllElementsKernel(int *d_array_in, int *d_sum_out, int arraySize)
{
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;

  int numberOfAdditions = log2f (arraySize * 2);
  numberOfAdditions = BLOCKSIZE;
  for (int s = 1; s <= numberOfAdditions; s*=2) {
    if ((i % (2*s)) == 0) {
      d_array_in[i] += d_array_in[i + s];
    }
    __syncthreads();   
  }
  if (i == 0) d_sum_out[0] = d_array_in[0];
}

#endif


/*****************************************************************/
/*          Kernel 27                                            */
/*****************************************************************/

#ifdef K27

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__device__ int gpuadd(int a, int b){
	return a + b;
}

__global__ void computeKernel(int *d_in,int *d_out) {

	d_out[threadIdx.x] = 0;
	for (int i=0; i<SIZE/BLOCKSIZE; i++) {
		d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
	}


	__syncthreads();
	for (int j = 1; j < BLOCKSIZE; j *= 2)
	{
		if(threadIdx.x % (2 * j) == 0)
			d_out[threadIdx.x] = gpuadd(d_out[threadIdx.x], d_out[threadIdx.x + j]);	
		__syncthreads();
	}

}


#endif


/*****************************************************************/
/*          Kernel 28                                            */
/*****************************************************************/

#ifdef K28 

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out) {

  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }

  __syncthreads();
  /* Tree-structured results-gathing phase, for BLOCKSIZE=32 */
  if(threadIdx.x < 16){ d_out[threadIdx.x] += d_out[threadIdx.x + 16];} __syncthreads();
  if(threadIdx.x < 8) { d_out[threadIdx.x] += d_out[threadIdx.x + 8]; } __syncthreads();
  if(threadIdx.x < 4) { d_out[threadIdx.x] += d_out[threadIdx.x + 4]; } __syncthreads();
  if(threadIdx.x < 2) { d_out[threadIdx.x] += d_out[threadIdx.x + 2]; } __syncthreads();
  if(threadIdx.x < 1) { d_out[threadIdx.x] += d_out[threadIdx.x + 1]; }
}


#endif


/*****************************************************************/
/*          Kernel 29                                             */
/*****************************************************************/

#ifdef K29

#define SIZE 256
#define BLOCKSIZE 32

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}

__global__ void computeKernel(int *d_in,int *d_out, int *d_sum) {
  // *d_sum=0;
  
  d_out[threadIdx.x] = 0;
  for (int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  __syncthreads();


  // PUG found this synchronization error
  if(threadIdx.x%2==0) {
    for(int i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x+SIZE/BLOCKSIZE*i]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+1];
      __syncthreads();
    }
  }

  if(threadIdx.x==0||threadIdx.x==4) {
    for(int i=0; i<SIZE/BLOCKSIZE; i++) {
		d_out[threadIdx.x+SIZE/BLOCKSIZE*i]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+2];
	}
  }
  __syncthreads();

  if(threadIdx.x==0) {
	for(int i=0; i<SIZE/BLOCKSIZE; i++){
		d_out[threadIdx.x]+=d_out[threadIdx.x+SIZE/BLOCKSIZE*i+4];
	}
  }
  __syncthreads();
  // *d_sum=*d_out;
}


#endif
