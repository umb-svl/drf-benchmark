//pass
//--gridDim=196              --blockDim=256
#include "my_cutil.h"
  
__device__ void;
vectorAddKernel(const float *A, const float *B, float *C, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numElements)
    {
        C[i] = A[i] + B[i];
    }
}
