//pass
//--gridDim=[4096,1,1]     --blockDim=[256,1,1]
#include "my_cutil.h"

__global__ void vectorAddGPU(float *a, float *b, float *c, int N)
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;

    if (idx < N)
    {
        c[idx] = a[idx] + b[idx];
    }
}
