#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cutil.h>

#define SIZE 256
#define BLOCKSIZE 32
//#define SIZE 16
//#define BLOCKSIZE 4

__host__ void outer_compute(int *in_arr, int *out_arr);

int main(int argc, char **argv)
{
  int *in_array, *out_array;
  int sum=0;

  /* initialization */
  in_array = (int *) malloc(SIZE*sizeof(int)); 
  for (int i=0; i<SIZE; i++) {
    in_array[i] = rand()%10; 
    printf("in_array[%d] = %d\n",i,in_array[i]);    
  }
  out_array = (int *) malloc(SIZE*sizeof(int)); 

  /* compute number of appearances of 6 */
  outer_compute(in_array, out_array);

  //for (int i=0; i<BLOCKSIZE; i++) {
    //sum+=out_array[i];
  //}
  sum = out_array[0];

  printf ("The number 6 appears %d times in array of  %d numbers\n",sum,SIZE);
  CUT_EXIT(argc, argv);  
}

__device__ int compare(int a, int b) {
  if (a == b) return 1;
  return 0;
}


__global__ void compute(int *d_in,int *d_out) {
  int i;

  d_out[threadIdx.x] = 0;
  for (i=0; i<SIZE/BLOCKSIZE; i++) {
      d_out[threadIdx.x] += compare(d_in[i*BLOCKSIZE+threadIdx.x],6);
  }
  __syncthreads(); // wait for all threads to finish counting their 6's
  int useNumThreads = BLOCKSIZE / 2;
   while(useNumThreads>0)
  {
	int combinedSize = BLOCKSIZE / useNumThreads;
	if(threadIdx.x % combinedSize == 0)
	{
		d_out[threadIdx.x] = d_out[threadIdx.x] + d_out[threadIdx.x + (combinedSize /2)];	
		useNumThreads = useNumThreads/2;

	}	
	__syncthreads(); 
  } 

  //for(i=0;i<SIZE/BLOCKSIZE;i++) {
	  printf("\nblock id.x : %d \t blockIx.y : %d \t", blockIdx.x,blockIdx.y);
	  printf("Thread Id.x  : %d \t ThreadIx.y : %d\n", threadIdx.x, threadIdx.y);
  //}
  getchar();

}

__host__ void outer_compute(int *h_in_array, int *h_out_array) {
  int *d_in_array, *d_out_array;

  /* allocate memory for device copies, and copy input to device */
  cudaMalloc((void **) &d_in_array,SIZE*sizeof(int));
  cudaMalloc((void **) &d_out_array,SIZE*sizeof(int));
  cudaMemcpy(d_in_array,h_in_array,SIZE*sizeof(int),cudaMemcpyHostToDevice);

  /* compute number of appearances of 8 for subset of data in each thread! */
  dim3 grid(2,2);
  dim3 block(2,2);
  compute<<<grid,block,(SIZE+BLOCKSIZE)*sizeof(int)>>>(d_in_array,d_out_array);
  
  // out_array[0] now contains the total sum

  cudaMemcpy(h_out_array,d_out_array,SIZE*sizeof(int),cudaMemcpyDeviceToHost);
  //cudaMemcpy(h_out_array,d_out_array,SIZE*sizeof(int),cudaMemcpyDeviceToHost);
}
