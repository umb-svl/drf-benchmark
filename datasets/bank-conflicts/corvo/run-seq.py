# /usr/bin/env python3

import argparse
import subprocess

import numpy as np
import pandas as pd


# Generates a dataset for each cumulative set of options.
def generate_datasets(options, files):
    for i in range(len(options) + 1):
        params = ' '.join(options[:i])
        subprocess.run(['python', 'generate-cuda.py', '-p', params, '-f',
                        files, '-l', f'gencuda{i}.log', '-g', f'gencuda{i}'])


# Runs faial-bc and racuda on each generated dataset.
def run_tools(options, files):
    for i in range(len(options) + 1):
        # Get costs.
        subprocess.run(['python', 'run-bc.py', '-d', f'gencuda{i}',
                        '-f', files, '-o', f'gencuda{i}_b.csv',
                        '-l', f'gencuda{i}_b.log'])
        subprocess.run(['python', 'run-racuda.py', '-d', f'gencuda{i}',
                        '-f', files, '-o', f'gencuda{i}_r.csv',
                        '-l', f'gencuda{i}_r.log', '-t', '5m'])

        # Get asymptotic costs.
        subprocess.run(['python', 'run-bc.py', '-d', f'gencuda{i}',
                        '-f', files, '-o', f'gencuda{i}_b_a.csv',
                        '-l', f'gencuda{i}_b_a.log', '-c', 'asympt'])
        subprocess.run(['python', 'run-racuda.py', '-d', f'gencuda{i}',
                        '-f', files, '-o', f'gencuda{i}_r_a.csv',
                        '-l', f'gencuda{i}_r_a.log', '-t', '5m', '-a'])


# Determine which stats to use and the method to bin kernels.
def select_stats(bin_outcome):
    if bin_outcome:
        stats = ['faial error', 'racuda failure',
                 'same outcome', 'different outcome']
        bin_kernel = bin_kernel_outcome
    else:
        stats = ['faial error', 'absynth error', 'parsing error',
                 'unfound bound', 'timed out', 'same cost', 'same asympt cost',
                 'racuda=zero', 'faial=zero', 'different cost']
        bin_kernel = bin_kernel_category

    return stats, bin_kernel


# Reads in analysis results corresponding to the given dataset index.
def read_analysis_results(dataset):
    # Read in kernel data from CSV files.
    faial = pd.read_csv(f'gencuda{dataset}_b.csv')
    racuda = pd.read_csv(f'gencuda{dataset}_r.csv')
    faial_asympt = pd.read_csv(f'gencuda{dataset}_b_a.csv')
    racuda_asympt = pd.read_csv(f'gencuda{dataset}_r_a.csv')

    # Collect data into an iterable object.
    data = [d.itertuples()
            for d in [faial, racuda, faial_asympt, racuda_asympt]]

    return zip(*data)


# Bins the kernel into a category based on the results of each tool.
def bin_kernel_category(faial, racuda, faial_asympt, racuda_asympt):
    if faial.exit_code == 1:
        return 'faial error'
    elif faial.exit_code == 255:
        return 'absynth error'
    elif racuda.exit_code == 2:
        return 'parsing error'
    elif racuda.exit_code == 1:
        return 'unfound bound'
    elif racuda.exit_code == 124:
        return 'timed out'
    elif racuda.cost == faial.cost:
        return 'same cost'
    elif racuda.cost == '0':
        return 'racuda=zero'
    elif faial.cost == '0':
        return 'faial=zero'
    elif racuda_asympt.cost == faial_asympt.cost:
        return 'same asympt cost'
    else:
        return 'different cost'


# Bins the kernel into an outcome based on the results of each tool.
def bin_kernel_outcome(faial, racuda, *_):
    if faial.exit_code != 0:
        return 'faial error'
    elif racuda.exit_code != 0:
        return 'racuda failure'
    elif racuda.cost == faial.cost:
        return 'same outcome'
    else:
        return 'different outcome'


# Adds a delta annotation to each stat in the column.
def add_delta(column: pd.Series):
    prev_stat = column[0]

    for i in range(1, len(column)):
        stat = column[i]
        delta = stat - prev_stat

        if delta > 0:
            delta = f' (+{delta})'
        elif delta < 0:
            delta = f' ({delta})'
        else:
            delta = ''

        column[i] = f'{stat}{delta}'
        prev_stat = stat

    return column


# Returns a dataframe of the kernel categories for each dataset.
def get_kernel_data(options, bin_kernel, kernels):
    # Set up a row for each kernel's dataset categories.
    kernel_data = [{'filename': kernel} for kernel in kernels]

    # Get analysis results for each generated dataset.
    for dataset, option in enumerate(options):
        data = read_analysis_results(dataset)

        # Bin analysis results from each kernel into a category.
        for kernel, results in enumerate(data):
            stat = bin_kernel(*results)
            kernel_data[kernel][option] = stat

    return pd.DataFrame(kernel_data)


# Summarizes categorical kernel data into statistics for each dataset.
def summarize_data(kernel_data: pd.DataFrame, options, stats):
    # Count the kernels in each category and transpose the table.
    kernel_stats = kernel_data[options].apply(pd.value_counts).transpose()

    # Reorder the columns to match the statistics.
    columns = set(kernel_stats.columns)
    stats = [stat for stat in stats if stat in columns]
    kernel_stats = kernel_stats[stats]

    # Discard columns with no kernels.
    kernel_stats = kernel_stats.fillna(0).astype(int)
    kernel_stats = kernel_stats.loc[:, (kernel_stats != 0).any(axis=0)]

    # Make the first column the code generation option.
    kernel_stats.index.name = 'option'

    return kernel_stats


# Drop kernels that always encounter an error with faial-bc.
def drop_error_kernels(kernel_data: pd.DataFrame, options):
    df = kernel_data[options]
    mask = np.all((df == 'faial error') | (df == 'absynth error'), axis=1)
    return kernel_data[~mask]


# Command-line interface.
def set_up_cli():
    description = ('Generates a series of datasets based on the given code '
                   'generation options, runs faial-bc and racuda on each one, '
                   'and compiles comparative results in a CSV file.')
    opts = ['gen-params', 'expand-device', 'simplify-kernel', 'const-fold',
            'div-to-mult', 'use-dummy-array', 'distinct-vars', 'mod-gv-args']

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--options', '-o', default=opts,
                        help='Sequence of code generation options.')
    parser.add_argument('--gen-datasets', '-g', help='Generate the datasets.',
                        action='store_true')
    parser.add_argument('--files', '-f', default='all-shared.txt',
                        help='Text file containing the list of kernel names. '
                        'Default: %(default)s')
    parser.add_argument('--run-tools', '-r', help='Run faial-bc and racuda on '
                        'each dataset.', action='store_true')
    parser.add_argument('--timeout', '-t', default='5m',
                        help='Time to wait before killing RaCUDA. '
                        'Default: %(default)s')
    parser.add_argument('--bin-outcome', '-b', help='Categorize kernels into '
                        'more general tool outcomes.', action='store_true')
    parser.add_argument('--discard-error', '-d', help='Discard kernels that '
                        'always encounter errors.', action='store_true')
    parser.add_argument('--show-delta', '-s', help='Show the increase/decrease'
                        ' between each row.', action='store_true')
    parser.add_argument('--data-csv', default='results_seq_data.csv',
                        help='Output CSV file for categorical kernel data. '
                        'Default: %(default)s')
    parser.add_argument('--stats-csv', '-c', default='results_seq_stats.csv',
                        help='Output CSV file for kernel statistics. '
                        'Default: %(default)s')

    return parser


def main():
    # Parse command-line arguments.
    parser = set_up_cli()
    args = parser.parse_args()

    # Generate the datasets/run the analyses, if requested.
    if args.gen_datasets:
        generate_datasets(args.options, args.files)
    if args.run_tools:
        run_tools(args.options, args.files)

    # Determine which stats to measure and the method to bin kernels.
    stats, bin_kernel = select_stats(args.bin_outcome)

    # Get the names of all kernels in the dataset.
    kernels = open(args.files).read().splitlines()

    # Include a column for no code generation options.
    options = ['none'] + args.options

    # Collect the categorical kernel data.
    kernel_data = get_kernel_data(options, bin_kernel, kernels)

    # Discard error kernels, if requested.
    if args.discard_error:
        kernel_data = drop_error_kernels(kernel_data, options)

    # Summarize the data into statistics.
    kernel_stats = summarize_data(kernel_data, options, stats)

    # Add a delta to each row, if requested.
    if args.show_delta:
        kernel_stats.apply(add_delta, axis=0)

    # Write categorical kernel data and statistics to CSV files.
    kernel_data.to_csv(args.data_csv, index=False)
    kernel_stats.to_csv(args.stats_csv)


if __name__ == '__main__':
    main()
