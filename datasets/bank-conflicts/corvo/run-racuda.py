# /usr/bin/env python3

import argparse
import csv
import os
import re
import subprocess


# Exits if the path is not a valid directory.
def validate_dir(path):
    if not os.path.isdir(path):
        print(f'{os.path.abspath(path)} is not a directory.')
        exit(1)


# Sets up the necessary files to run/record analysis results.
def run_tool(output, dataset_dir, files, log, timeout, asympt):
    # Load the names of kernels in the dataset.
    kernels = open(files, 'r').read().splitlines()

    # Open CSV file for writing.
    with open(output, 'w', newline='') as csvfile:
        fieldnames = ['exit_code', 'cost', 'filename']
        writer = csv.DictWriter(csvfile, fieldnames)
        writer.writeheader()

        # Open log file to capture error output.
        with open(log, 'w') as error_log:
            run_files(dataset_dir, kernels, writer, error_log, timeout, asympt)


# Runs RaCUDA on each kernel in the dataset.
def run_files(dataset_dir, kernels, writer, error_log, timeout, asympt):
    pattern = re.compile(r'Bound: (.*?)\n')

    for kernel in kernels:
        kernel_path = f'{dataset_dir}/{kernel}'
        params_path = f'{kernel_path}.params'

        # Check whether the kernel exists.
        if not os.path.isfile(kernel_path):
            error_log.write(f'{kernel_path} is not a valid kernel path.\n')
            continue

        # Set up command.
        args = ['timeout', timeout, 'absynth-cuda', '-ascii',
                '-metric', 'shared', kernel_path]
        args += ['-asympt'] if asympt else []

        # Use .params file if it exists.
        if os.path.isfile(params_path):
            args += ['-param-file', params_path]
        else:
            error_log.write(f'WARNING: {kernel_path} not found.\n')

        # Run analysis on the kernel.
        result = subprocess.run(
            args=args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

        # Get cost from output.
        match result.returncode:
            case 0:
                cost = pattern.findall(result.stdout)[0]
            case 1:
                cost = 'not found'
            case 124:
                cost = 'timed out'
            case _:
                cost = ''

        # Write analysis results to the CSV file.
        writer.writerow({
            'exit_code': result.returncode,
            'cost': cost,
            'filename': kernel
        })

        # Write errors to the log file.
        error_log.write(f'{kernel}\n{result.stderr}')


def main():
    parser = argparse.ArgumentParser(description='Runs RaCUDA on the dataset.')
    parser.add_argument('--output', '-o', default='racuda_results.csv',
                        help='Output CSV file. Default: %(default)s')
    parser.add_argument('--dataset-dir', '-d', default='gencuda',
                        help='Directory containing the kernels. '
                        'Default: %(default)s')
    parser.add_argument('--files', '-f', default='kernels.txt',
                        help='Text file containing the list of kernel names. '
                        'Default: %(default)s')
    parser.add_argument('--log', '-l', default='racuda_errors.log',
                        help='Log file of the errors. Default: %(default)s')
    parser.add_argument('--timeout', '-t', default='5s',
                        help='Time to wait before killing RaCUDA. '
                        'Default: %(default)s')
    parser.add_argument('--asympt', '-a', help='Derive cost in O(n) terms',
                        action='store_true')
    args = parser.parse_args()
    validate_dir(args.dataset_dir)
    run_tool(**vars(args))


if __name__ == '__main__':
    main()
