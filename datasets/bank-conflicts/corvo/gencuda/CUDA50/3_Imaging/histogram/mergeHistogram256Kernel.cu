//pass
//--blockDim=[256,1,1] --gridDim=[256,1,1]

extern __attribute__((device)) int __dummyd_Histogram_w();
extern __attribute__((device)) int __dummyd_PartialHistograms_w();
extern __attribute__((device)) int __dummydata_w();
__global__ void kernel(int *__dummy, int *d_Histogram, int *d_PartialHistograms, int histogramCount)
{
    __shared__ int data[256];
    int __dummyd_Histogram;
    int __dummyd_PartialHistograms;
    int __dummydata;
    for (int i = 0; i < histogramCount; i += 256) {
        if (i >= threadIdx.x && i < histogramCount) {
            __dummyd_PartialHistograms = d_PartialHistograms[blockIdx.x + (256 * i)];
        }
    }
    data[threadIdx.x] = __dummydata_w();
    for (int stride = 1; stride < 129; stride *= 2) {
        __syncthreads();
        if (threadIdx.x < stride) {
            __dummydata = data[threadIdx.x];
            __dummydata = data[threadIdx.x + stride];
            data[threadIdx.x] = __dummydata_w();
        }
    }
    if (threadIdx.x == 0) {
        __dummydata = data[0];
        d_Histogram[blockIdx.x] = __dummyd_Histogram_w();
    }
}
