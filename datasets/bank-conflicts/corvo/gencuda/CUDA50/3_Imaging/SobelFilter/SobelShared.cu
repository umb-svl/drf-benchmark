//pass
//--blockDim=[32,32,1] --gridDim=[2,128,1]

extern __attribute__((device)) int __dummyLocalBlock_w();
extern __attribute__((device)) int __dummypSobelOriginal_w();
__global__ void kernel(int *__dummy, int *pSobelOriginal, int BlockWidth, int SharedPitch, int h, int w)
{
    extern __shared__ int LocalBlock[];
    int __dummyLocalBlock;
    int __dummypSobelOriginal;
    for (int ib1 = 0; ib1 < 2 + BlockWidth; ib1 += 32) {
        if (ib1 >= threadIdx.x && ib1 < 2 + BlockWidth) {
            LocalBlock[((threadIdx.y * SharedPitch) + (4 * ib1)) / 4] = __dummyLocalBlock_w();
            LocalBlock[(1 + ((threadIdx.y * SharedPitch) + (4 * ib1))) / 4] = __dummyLocalBlock_w();
            LocalBlock[(2 + ((threadIdx.y * SharedPitch) + (4 * ib1))) / 4] = __dummyLocalBlock_w();
            LocalBlock[(3 + ((threadIdx.y * SharedPitch) + (4 * ib1))) / 4] = __dummyLocalBlock_w();
        }
    }
    if (threadIdx.y < 2) {
        for (int ib11 = 0; ib11 < 2 + BlockWidth; ib11 += 32) {
            if (ib11 >= threadIdx.x && ib11 < 2 + BlockWidth) {
                LocalBlock[(((32 + threadIdx.y) * SharedPitch) + (4 * ib11)) / 4] = __dummyLocalBlock_w();
                LocalBlock[(1 + (((32 + threadIdx.y) * SharedPitch) + (4 * ib11))) / 4] = __dummyLocalBlock_w();
                LocalBlock[(2 + (((32 + threadIdx.y) * SharedPitch) + (4 * ib11))) / 4] = __dummyLocalBlock_w();
                LocalBlock[(3 + (((32 + threadIdx.y) * SharedPitch) + (4 * ib11))) / 4] = __dummyLocalBlock_w();
            }
        }
    }
    __syncthreads();
    for (int ib12 = 0; ib12 < BlockWidth; ib12 += 32) {
        if (ib12 >= threadIdx.x && ib12 < BlockWidth) {
            __dummyLocalBlock = LocalBlock[((threadIdx.y * SharedPitch) + (4 * ib12)) / 4];
            __dummyLocalBlock = LocalBlock[(1 + ((threadIdx.y * SharedPitch) + (4 * ib12))) / 4];
            __dummyLocalBlock = LocalBlock[(2 + ((threadIdx.y * SharedPitch) + (4 * ib12))) / 4];
            __dummyLocalBlock = LocalBlock[(((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch) / 4];
            __dummyLocalBlock = LocalBlock[(1 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(2 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(1 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch))) / 4];
            __dummyLocalBlock = LocalBlock[(2 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch))) / 4];
            __dummyLocalBlock = LocalBlock[(3 + ((threadIdx.y * SharedPitch) + (4 * ib12))) / 4];
            __dummyLocalBlock = LocalBlock[(3 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(3 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch))) / 4];
            __dummyLocalBlock = LocalBlock[(4 + ((threadIdx.y * SharedPitch) + (4 * ib12))) / 4];
            __dummyLocalBlock = LocalBlock[(4 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(4 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch))) / 4];
            __dummyLocalBlock = LocalBlock[(5 + ((threadIdx.y * SharedPitch) + (4 * ib12))) / 4];
            __dummyLocalBlock = LocalBlock[(5 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + SharedPitch)) / 4];
            __dummyLocalBlock = LocalBlock[(5 + (((threadIdx.y * SharedPitch) + (4 * ib12)) + (2 * SharedPitch))) / 4];
        }
    }
    __syncthreads();
}
