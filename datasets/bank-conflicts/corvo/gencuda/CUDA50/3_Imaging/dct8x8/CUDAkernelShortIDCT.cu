//pass
//--blockDim=[32,32,32] --gridDim=[16,16,1]

extern __attribute__((device)) int __dummySrcDst_w();
extern __attribute__((device)) int __dummyblock_w();
__global__ void kernel(int *__dummy, int *SrcDst, int ImgStride)
{
    __shared__ int block[1088];
    int __dummySrcDst;
    int __dummyblock;
    if ((8 * threadIdx.y) + threadIdx.x < 16) {
        for (int i = 0; i < 8; i += 1) {
            __dummySrcDst = SrcDst[((((32 * blockIdx.y) + (8 * threadIdx.z)) * ImgStride) + ((32 * blockIdx.x) + (2 * ((8 * threadIdx.y) + threadIdx.x)))) + (i * (ImgStride / 2))];
        }
    }
    __syncthreads();
    __syncthreads();
    __syncthreads();
    if ((8 * threadIdx.y) + threadIdx.x < 16) {
        for (int i1 = 0; i1 < 8; i1 += 1) {
            SrcDst[((((32 * blockIdx.y) + (8 * threadIdx.z)) * ImgStride) + ((32 * blockIdx.x) + (2 * ((8 * threadIdx.y) + threadIdx.x)))) + (i1 * (ImgStride / 2))] = __dummySrcDst_w();
        }
    }
}
