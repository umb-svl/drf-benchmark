//pass
//--blockDim=[32,32,32] --gridDim=[16,32,1]

extern __attribute__((device)) int __dummyblock_w();
extern __attribute__((device)) int __dummydst_w();
extern __attribute__((device)) int __dummysrc_w();
__global__ void kernel(int *__dummy, int *dst, int *src, int ImgStride)
{
    __shared__ int block[528];
    int __dummyblock;
    int __dummydst;
    int __dummysrc;
    for (int i = 0; i < 8; i += 1) {
        __dummysrc = src[(((((16 * blockIdx.y) + (8 * threadIdx.z)) * ImgStride) + (32 * blockIdx.x)) + ((8 * threadIdx.y) + threadIdx.x)) + (i * ImgStride)];
    }
    for (int i1 = 0; i1 < 8; i1 += 1) {
        dst[(((((16 * blockIdx.y) + (8 * threadIdx.z)) * ImgStride) + (32 * blockIdx.x)) + ((8 * threadIdx.y) + threadIdx.x)) + (i1 * ImgStride)] = __dummydst_w();
    }
}
