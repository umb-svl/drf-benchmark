//pass
//--blockDim=[32,32,1] --gridDim=[64,64,1]

extern __attribute__((device)) int __dummyCurBlockLocal1_w();
extern __attribute__((device)) int __dummyCurBlockLocal2_w();
extern __attribute__((device)) int __dummyDst_w();
__global__ void kernel(int *__dummy, int *Dst, int ImgWidth, int OffsetXBlocks, int OffsetYBlocks)
{
    __shared__ int CurBlockLocal1[64];
    __shared__ int CurBlockLocal2[64];
    int __dummyCurBlockLocal1;
    int __dummyCurBlockLocal2;
    int __dummyDst;
    CurBlockLocal1[(threadIdx.y << 3) + threadIdx.x] = __dummyCurBlockLocal1_w();
    __syncthreads();
    for (int i = 0; i < 8; i += 1) {
        __dummyCurBlockLocal1 = CurBlockLocal1[threadIdx.x];
    }
    CurBlockLocal2[(threadIdx.y << 3) + threadIdx.x] = __dummyCurBlockLocal2_w();
    __syncthreads();
    for (int i1 = 0; i1 < 8; i1 += 1) {
        __dummyCurBlockLocal2 = CurBlockLocal2[threadIdx.y << 3];
    }
    CurBlockLocal1[(threadIdx.y << 3) + threadIdx.x] = __dummyCurBlockLocal1_w();
    __syncthreads();
    __dummyCurBlockLocal1 = CurBlockLocal1[(threadIdx.y << 3) + threadIdx.x];
    Dst[((((blockIdx.y + OffsetYBlocks) << 3) + threadIdx.y) * ImgWidth) + (((blockIdx.x + OffsetXBlocks) << 3) + threadIdx.x)] = __dummyDst_w();
}
