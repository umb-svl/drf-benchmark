//pass
//--blockDim=[32,32,1] --gridDim=[40,51,1]

extern __attribute__((device)) int __dummydst_w();
extern __attribute__((device)) int __dummyfWeights_w();
__global__ void kernel(int *__dummy, int *dst, int imageH, int imageW)
{
    __shared__ int fWeights[64];
    int __unk01 = __dummy[threadIdx.x];
    int __dummydst;
    int __dummyfWeights;
    fWeights[(8 * threadIdx.y) + threadIdx.x] = __dummyfWeights_w();
    __syncthreads();
    for (int i = -3; i < 5; i += 1) {
        for (int j = -3; j < 5; j += 1) {
            __dummyfWeights = fWeights[__unk01];
        }
    }
    dst[(imageW * ((32 * blockIdx.y) + threadIdx.y)) + ((32 * blockIdx.x) + threadIdx.x)] = __dummydst_w();
}
