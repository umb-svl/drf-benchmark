//pass
//--blockDim=[32,32,1] --gridDim=[32,32,1]

extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *g_odata, int imgh, int imgw, int r, int tilew)
{
    extern __shared__ int sdata[];
    int __dummyg_odata;
    int __dummysdata;
    sdata[((r + threadIdx.y) * tilew) + (r + threadIdx.x)] = __dummysdata_w();
    if (threadIdx.x < r) {
        sdata[((r + threadIdx.y) * tilew) + threadIdx.x] = __dummysdata_w();
        sdata[((r + threadIdx.y) * tilew) + ((32 + r) + threadIdx.x)] = __dummysdata_w();
    }
    if (threadIdx.y < r) {
        sdata[(threadIdx.y * tilew) + (r + threadIdx.x)] = __dummysdata_w();
        sdata[(((32 + r) + threadIdx.y) * tilew) + (r + threadIdx.x)] = __dummysdata_w();
    }
    if (threadIdx.x < r && threadIdx.y < r) {
        sdata[(threadIdx.y * tilew) + threadIdx.x] = __dummysdata_w();
        sdata[(((32 + r) + threadIdx.y) * tilew) + threadIdx.x] = __dummysdata_w();
        sdata[(threadIdx.y * tilew) + ((32 + r) + threadIdx.x)] = __dummysdata_w();
        sdata[(((32 + r) + threadIdx.y) * tilew) + ((32 + r) + threadIdx.x)] = __dummysdata_w();
    }
    __syncthreads();
    for (int dy = 0; dy < 1 + r; dy += 1) {
        for (int dx = 0; dx < 1 + r; dx += 1) {
            __dummysdata = sdata[(((r + threadIdx.y) + dy) * tilew) + ((r + threadIdx.x) + dx)];
        }
    }
    g_odata[(((32 * blockIdx.y) + threadIdx.y) * imgw) + ((32 * blockIdx.x) + threadIdx.x)] = __dummyg_odata_w();
}
