//pass
//--blockDim=[32,32,1] --gridDim=[192,48,1]

extern __attribute__((device)) int __dummyd_Dst_w();
extern __attribute__((device)) int __dummyd_Src_w();
extern __attribute__((device)) int __dummys_Data_w();
__global__ void kernel(int *__dummy, int *d_Dst, int *d_Src, int imageH, int imageW, int pitch)
{
    __shared__ int s_Data[1296];
    int __dummyd_Dst;
    int __dummyd_Src;
    int __dummys_Data;
    for (int i = 1; i < 9; i += 1) {
        __dummyd_Src = d_Src[((((8 * ((8 * blockIdx.y) - 1)) + threadIdx.y) * pitch) + ((16 * blockIdx.x) + threadIdx.x)) + ((8 * i) * pitch)];
        s_Data[(81 * threadIdx.x) + (threadIdx.y + (8 * i))] = __dummys_Data_w();
    }
    __dummyd_Src = d_Src[(((8 * ((8 * blockIdx.y) - 1)) + threadIdx.y) * pitch) + ((16 * blockIdx.x) + threadIdx.x)];
    s_Data[(81 * threadIdx.x) + threadIdx.y] = __dummys_Data_w();
    __dummyd_Src = d_Src[((((8 * ((8 * blockIdx.y) - 1)) + threadIdx.y) * pitch) + ((16 * blockIdx.x) + threadIdx.x)) + (72 * pitch)];
    s_Data[(81 * threadIdx.x) + (72 + threadIdx.y)] = __dummys_Data_w();
    __syncthreads();
    for (int i1 = 1; i1 < 9; i1 += 1) {
        for (int j = -8; j < 9; j += 1) {
            __dummys_Data = s_Data[(81 * threadIdx.x) + ((threadIdx.y + (8 * i1)) + j)];
        }
        d_Dst[((((8 * ((8 * blockIdx.y) - 1)) + threadIdx.y) * pitch) + ((16 * blockIdx.x) + threadIdx.x)) + ((8 * i1) * pitch)] = __dummyd_Dst_w();
    }
}
