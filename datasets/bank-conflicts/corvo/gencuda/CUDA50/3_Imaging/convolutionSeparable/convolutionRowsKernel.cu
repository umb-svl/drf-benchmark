//pass
//--blockDim=[32,32,1] --gridDim=[24,768,1]

extern __attribute__((device)) int __dummyd_Dst_w();
extern __attribute__((device)) int __dummyd_Src_w();
extern __attribute__((device)) int __dummys_Data_w();
__global__ void kernel(int *__dummy, int *d_Dst, int *d_Src, int imageH, int imageW, int pitch)
{
    __shared__ int s_Data[640];
    int __dummyd_Dst;
    int __dummyd_Src;
    int __dummys_Data;
    for (int i = 1; i < 9; i += 1) {
        __dummyd_Src = d_Src[((((4 * blockIdx.y) + threadIdx.y) * pitch) + ((16 * ((8 * blockIdx.x) - 1)) + threadIdx.x)) + (16 * i)];
        s_Data[(160 * threadIdx.y) + (threadIdx.x + (16 * i))] = __dummys_Data_w();
    }
    __dummyd_Src = d_Src[(((4 * blockIdx.y) + threadIdx.y) * pitch) + ((16 * ((8 * blockIdx.x) - 1)) + threadIdx.x)];
    s_Data[(160 * threadIdx.y) + threadIdx.x] = __dummys_Data_w();
    __dummyd_Src = d_Src[144 + ((((4 * blockIdx.y) + threadIdx.y) * pitch) + ((16 * ((8 * blockIdx.x) - 1)) + threadIdx.x))];
    s_Data[(160 * threadIdx.y) + (144 + threadIdx.x)] = __dummys_Data_w();
    __syncthreads();
    for (int i1 = 1; i1 < 9; i1 += 1) {
        for (int j = -8; j < 9; j += 1) {
            __dummys_Data = s_Data[(160 * threadIdx.y) + ((threadIdx.x + (16 * i1)) + j)];
        }
        d_Dst[((((4 * blockIdx.y) + threadIdx.y) * pitch) + ((16 * ((8 * blockIdx.x) - 1)) + threadIdx.x)) + (16 * i1)] = __dummyd_Dst_w();
    }
}
