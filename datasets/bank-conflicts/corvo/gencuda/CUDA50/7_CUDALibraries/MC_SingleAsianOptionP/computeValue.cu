//pass
//--blockDim=[128,1,1] --gridDim=[195,1,1]

extern __attribute__((device)) int __dummyoption_w();
extern __attribute__((device)) int __dummypaths_w();
extern __attribute__((device)) int __dummysdata_w();
extern __attribute__((device)) int __dummyvalues_w();
__global__ void kernel(int *__dummy, int *option, int *paths, int *values, int numSims, int numTimesteps)
{
    __shared__ int sdata[512];
    int __dummyoption;
    int __dummypaths;
    int __dummysdata;
    int __dummyvalues;
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    for (int s = 1; s < 65; s *= 2) {
        if (threadIdx.x < s) {
            __dummysdata = sdata[threadIdx.x];
            __dummysdata = sdata[threadIdx.x + s];
            sdata[threadIdx.x] = __dummysdata_w();
        }
        __syncthreads();
    }
    __dummysdata = sdata[0];
    if (threadIdx.x == 0) {
        values[blockIdx.x] = __dummyvalues_w();
    }
}
