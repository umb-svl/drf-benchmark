//pass
//--blockDim=[128,1,1] --gridDim=[195,1,1]

extern __attribute__((device)) int __dummyresults_w();
extern __attribute__((device)) int __dummyrngStates_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *results, int *rngStates, int numSims)
{
    __shared__ int sdata[512];
    int __dummyresults;
    int __dummyrngStates;
    int __dummysdata;
    __dummyrngStates = rngStates[(128 * blockIdx.x) + threadIdx.x];
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    for (int s = 1; s < 65; s *= 2) {
        if (threadIdx.x < s) {
            __dummysdata = sdata[threadIdx.x];
            __dummysdata = sdata[threadIdx.x + s];
            sdata[threadIdx.x] = __dummysdata_w();
        }
        __syncthreads();
    }
    __dummysdata = sdata[0];
    if (threadIdx.x == 0) {
        results[blockIdx.x] = __dummyresults_w();
    }
}
