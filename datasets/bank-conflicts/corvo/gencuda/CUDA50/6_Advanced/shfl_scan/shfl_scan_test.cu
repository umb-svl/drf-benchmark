//pass
//--blockDim=[256,1,1] --gridDim=[256,1,1]

extern __attribute__((device)) int __dummydata_w();
extern __attribute__((device)) int __dummypartial_sums_w();
extern __attribute__((device)) int __dummysums_w();
__global__ void kernel(int *__dummy, int *data, int *partial_sums, int width)
{
    extern __shared__ int sums[];
    int __dummydata;
    int __dummypartial_sums;
    int __dummysums;
    __dummydata = data[(256 * blockIdx.x) + threadIdx.x];
    if (threadIdx.x % 32 == 31) {
        sums[threadIdx.x / 32] = __dummysums_w();
    }
    __syncthreads();
    if (threadIdx.x / 32 == 0) {
        __dummysums = sums[((256 * blockIdx.x) + threadIdx.x) % 32];
        sums[((256 * blockIdx.x) + threadIdx.x) % 32] = __dummysums_w();
    }
    __syncthreads();
    if (threadIdx.x / 32 > 0) {
        __dummysums = sums[(threadIdx.x / 32) - 1];
    }
    data[(256 * blockIdx.x) + threadIdx.x] = __dummydata_w();
    if (partial_sums != 0 && threadIdx.x == 255) {
        partial_sums[blockIdx.x] = __dummypartial_sums_w();
    }
}
