//pass
//--blockDim=[256,1,1] --gridDim=[255,1,1]

extern __attribute__((device)) int __dummybuf_w();
extern __attribute__((device)) int __dummydata_w();
extern __attribute__((device)) int __dummypartial_sums_w();
__global__ void kernel(int *__dummy, int *data, int *partial_sums, int len)
{
    extern __shared__ int buf[];
    int __dummybuf;
    int __dummydata;
    int __dummypartial_sums;
    if (threadIdx.x == 0) {
        __dummypartial_sums = partial_sums[blockIdx.x];
        buf[0] = __dummybuf_w();
    }
    __syncthreads();
    __dummydata = data[(256 * blockIdx.x) + threadIdx.x];
    __dummybuf = buf[0];
    data[(256 * blockIdx.x) + threadIdx.x] = __dummydata_w();
}
