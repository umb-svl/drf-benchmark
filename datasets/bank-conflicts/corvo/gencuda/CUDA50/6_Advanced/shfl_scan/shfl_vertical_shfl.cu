//pass
//--blockDim=[32,32,1] --gridDim=[60,1,1]

extern __attribute__((device)) int __dummyimg_w();
extern __attribute__((device)) int __dummysums_w();
__global__ void kernel(int *__dummy, int *img, int height, int width)
{
    __shared__ int sums[288];
    int __dummyimg;
    int __dummysums;
    sums[(9 * threadIdx.x) + threadIdx.y] = __dummysums_w();
    __syncthreads();
    for (int step = 0; step < 135; step += 1) {
        __dummyimg = img[((threadIdx.y + (8 * step)) * width) + ((32 * blockIdx.x) + threadIdx.x)];
        sums[(9 * threadIdx.x) + threadIdx.y] = __dummysums_w();
        __syncthreads();
        __dummysums = sums[(9 * ((threadIdx.x / 8) + (4 * threadIdx.y))) + (threadIdx.x % 8)];
        sums[(9 * ((threadIdx.x / 8) + (4 * threadIdx.y))) + (threadIdx.x % 8)] = __dummysums_w();
        __syncthreads();
        if (threadIdx.y > 0) {
            __dummysums = sums[(9 * threadIdx.x) + (threadIdx.y - 1)];
        }
        __dummysums = sums[31 + (9 * threadIdx.x)];
        __syncthreads();
        img[((threadIdx.y + (8 * step)) * width) + ((32 * blockIdx.x) + threadIdx.x)] = __dummyimg_w();
    }
}
