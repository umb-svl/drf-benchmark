//pass
//--blockDim=[128,1,1] --gridDim=[8,1,1]

extern __attribute__((device)) int __dummyd_DstKey_w();
extern __attribute__((device)) int __dummyd_DstVal_w();
extern __attribute__((device)) int __dummyd_LimitsA_w();
extern __attribute__((device)) int __dummyd_LimitsB_w();
extern __attribute__((device)) int __dummyd_SrcKey_w();
extern __attribute__((device)) int __dummyd_SrcVal_w();
extern __attribute__((device)) int __dummylenSrcA_w();
extern __attribute__((device)) int __dummylenSrcB_w();
extern __attribute__((device)) int __dummys_inf_w();
extern __attribute__((device)) int __dummys_key_w();
extern __attribute__((device)) int __dummys_val_w();
extern __attribute__((device)) int __dummystartDst_w();
extern __attribute__((device)) int __dummystartSrcA_w();
extern __attribute__((device)) int __dummystartSrcB_w();
__global__ void kernel(int *__dummy, int *d_DstKey, int *d_DstVal, int *d_LimitsA, int *d_LimitsB, int *d_SrcKey, int *d_SrcVal, int N, int stride)
{
    extern __shared__ int lenSrcA[];
    extern __shared__ int lenSrcB[];
    __shared__ int s_inf[256];
    __shared__ int s_key[256];
    __shared__ int s_val[256];
    extern __shared__ int startDst[];
    extern __shared__ int startSrcA[];
    extern __shared__ int startSrcB[];
    int _unknown_14 = __dummy[threadIdx.x];
    int _unknown_15 = __dummy[threadIdx.x];
    int _unknown_17 = __dummy[threadIdx.x];
    int _unknown_20 = __dummy[threadIdx.x];
    int _unknown_21 = __dummy[threadIdx.x];
    int _unknown_23 = __dummy[threadIdx.x];
    int _unknown_34 = __dummy[threadIdx.x];
    int _unknown_37 = __dummy[threadIdx.x];
    int _unknown_38 = __dummy[threadIdx.x];
    int _unknown_40 = __dummy[threadIdx.x];
    int _unknown_41 = __dummy[threadIdx.x];
    int _unknown_43 = __dummy[threadIdx.x];
    int __dummyd_DstKey;
    int __dummyd_DstVal;
    int __dummyd_LimitsA;
    int __dummyd_LimitsB;
    int __dummyd_SrcKey;
    int __dummyd_SrcVal;
    int __dummylenSrcA;
    int __dummylenSrcB;
    int __dummys_inf;
    int __dummys_key;
    int __dummys_val;
    int __dummystartDst;
    int __dummystartSrcA;
    int __dummystartSrcB;
    if (threadIdx.x == 0) {
        __dummyd_LimitsA = d_LimitsA[blockIdx.x];
        startSrcA[0] = __dummystartSrcA_w();
        __dummyd_LimitsB = d_LimitsB[blockIdx.x];
        startSrcB[0] = __dummystartSrcB_w();
        __dummystartSrcA = startSrcA[0];
        __dummystartSrcB = startSrcB[0];
        startDst[0] = __dummystartDst_w();
        __dummyd_LimitsA = d_LimitsA[1 + blockIdx.x];
        __dummyd_LimitsB = d_LimitsB[1 + blockIdx.x];
        __dummystartSrcA = startSrcA[0];
        lenSrcA[0] = __dummylenSrcA_w();
        __dummystartSrcB = startSrcB[0];
        lenSrcB[0] = __dummylenSrcB_w();
    }
    s_inf[threadIdx.x] = __dummys_inf_w();
    s_inf[128 + threadIdx.x] = __dummys_inf_w();
    __syncthreads();
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_14) {
        __dummystartSrcA = startSrcA[0];
        __dummyd_SrcKey = d_SrcKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_15 + threadIdx.x)];
        s_key[threadIdx.x] = __dummys_key_w();
        __dummystartSrcA = startSrcA[0];
        __dummyd_SrcVal = d_SrcVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_17 + threadIdx.x)];
        s_val[threadIdx.x] = __dummys_val_w();
        s_inf[threadIdx.x] = __dummys_inf_w();
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_20) {
        __dummystartSrcB = startSrcB[0];
        __dummyd_SrcKey = d_SrcKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + ((stride + _unknown_21) + threadIdx.x)];
        s_key[255 - threadIdx.x] = __dummys_key_w();
        __dummystartSrcB = startSrcB[0];
        __dummyd_SrcVal = d_SrcVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + ((stride + _unknown_23) + threadIdx.x)];
        s_val[255 - threadIdx.x] = __dummys_val_w();
        s_inf[255 - threadIdx.x] = __dummys_inf_w();
    }
    for (int stride1 = 1; stride1 < 129; stride1 *= 2) {
        __syncthreads();
        __dummys_key = s_key[(2 * threadIdx.x) - (threadIdx.x % stride1)];
        __dummys_val = s_val[(2 * threadIdx.x) - (threadIdx.x % stride1)];
        __dummys_inf = s_inf[(2 * threadIdx.x) - (threadIdx.x % stride1)];
        __dummys_key = s_key[((2 * threadIdx.x) - (threadIdx.x % stride1)) + stride1];
        __dummys_val = s_val[((2 * threadIdx.x) - (threadIdx.x % stride1)) + stride1];
        __dummys_inf = s_inf[((2 * threadIdx.x) - (threadIdx.x % stride1)) + stride1];
    }
    __syncthreads();
    __dummystartDst = startDst[0];
    __dummystartDst = startDst[0];
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_34) {
        __dummys_key = s_key[threadIdx.x];
        d_DstKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + threadIdx.x] = __dummyd_DstKey_w();
        __dummys_val = s_val[threadIdx.x];
        d_DstVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + threadIdx.x] = __dummyd_DstVal_w();
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_37) {
        __dummylenSrcA = lenSrcA[0];
        __dummys_key = s_key[_unknown_38 + threadIdx.x];
        __dummylenSrcA = lenSrcA[0];
        d_DstKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_40 + threadIdx.x)] = __dummyd_DstKey_w();
        __dummylenSrcA = lenSrcA[0];
        __dummys_val = s_val[_unknown_41 + threadIdx.x];
        __dummylenSrcA = lenSrcA[0];
        d_DstVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_43 + threadIdx.x)] = __dummyd_DstVal_w();
    }
}
