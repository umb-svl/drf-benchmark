//pass
//--blockDim=[128,1,1] --gridDim=[32768,1,1]

extern __attribute__((device)) int __dummyd_DstKey_w();
extern __attribute__((device)) int __dummyd_DstVal_w();
extern __attribute__((device)) int __dummyd_LimitsA_w();
extern __attribute__((device)) int __dummyd_LimitsB_w();
extern __attribute__((device)) int __dummyd_SrcKey_w();
extern __attribute__((device)) int __dummyd_SrcVal_w();
extern __attribute__((device)) int __dummylenSrcA_w();
extern __attribute__((device)) int __dummylenSrcB_w();
extern __attribute__((device)) int __dummys_key_w();
extern __attribute__((device)) int __dummys_val_w();
extern __attribute__((device)) int __dummystartDstA_w();
extern __attribute__((device)) int __dummystartDstB_w();
extern __attribute__((device)) int __dummystartSrcA_w();
extern __attribute__((device)) int __dummystartSrcB_w();
__global__ void kernel(int *__dummy, int *d_DstKey, int *d_DstVal, int *d_LimitsA, int *d_LimitsB, int *d_SrcKey, int *d_SrcVal, int N, int stride)
{
    extern __shared__ int lenSrcA[];
    extern __shared__ int lenSrcB[];
    __shared__ int s_key[256];
    __shared__ int s_val[256];
    extern __shared__ int startDstA[];
    extern __shared__ int startDstB[];
    extern __shared__ int startSrcA[];
    extern __shared__ int startSrcB[];
    int _unknown_15 = __dummy[threadIdx.x];
    int _unknown_16 = __dummy[threadIdx.x];
    int _unknown_18 = __dummy[threadIdx.x];
    int _unknown_20 = __dummy[threadIdx.x];
    int _unknown_21 = __dummy[threadIdx.x];
    int _unknown_23 = __dummy[threadIdx.x];
    int _unknown_25 = __dummy[threadIdx.x];
    int _unknown_29 = __dummy[threadIdx.x];
    int _unknown_33 = __dummy[threadIdx.x];
    int _unknown_34 = __dummy[threadIdx.x];
    int _unknown_35 = __dummy[threadIdx.x];
    int _unknown_37 = __dummy[threadIdx.x];
    int _unknown_39 = __dummy[threadIdx.x];
    int _unknown_40 = __dummy[threadIdx.x];
    int _unknown_41 = __dummy[threadIdx.x];
    int _unknown_43 = __dummy[threadIdx.x];
    int _unknown_44 = __dummy[threadIdx.x];
    int _unknown_46 = __dummy[threadIdx.x];
    int dstPosA = __dummy[threadIdx.x];
    int dstPosB = __dummy[threadIdx.x];
    int __dummyd_DstKey;
    int __dummyd_DstVal;
    int __dummyd_LimitsA;
    int __dummyd_LimitsB;
    int __dummyd_SrcKey;
    int __dummyd_SrcVal;
    int __dummylenSrcA;
    int __dummylenSrcB;
    int __dummys_key;
    int __dummys_val;
    int __dummystartDstA;
    int __dummystartDstB;
    int __dummystartSrcA;
    int __dummystartSrcB;
    if (threadIdx.x == 0) {
        __dummyd_LimitsA = d_LimitsA[blockIdx.x];
        startSrcA[0] = __dummystartSrcA_w();
        __dummyd_LimitsB = d_LimitsB[blockIdx.x];
        startSrcB[0] = __dummystartSrcB_w();
        __dummyd_LimitsA = d_LimitsA[1 + blockIdx.x];
        __dummyd_LimitsB = d_LimitsB[1 + blockIdx.x];
        __dummystartSrcA = startSrcA[0];
        lenSrcA[0] = __dummylenSrcA_w();
        __dummystartSrcB = startSrcB[0];
        lenSrcB[0] = __dummylenSrcB_w();
        __dummystartSrcA = startSrcA[0];
        __dummystartSrcB = startSrcB[0];
        startDstA[0] = __dummystartDstA_w();
        __dummystartDstA = startDstA[0];
        __dummylenSrcA = lenSrcA[0];
        startDstB[0] = __dummystartDstB_w();
    }
    __syncthreads();
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_15) {
        __dummystartSrcA = startSrcA[0];
        __dummyd_SrcKey = d_SrcKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_16 + threadIdx.x)];
        s_key[threadIdx.x] = __dummys_key_w();
        __dummystartSrcA = startSrcA[0];
        __dummyd_SrcVal = d_SrcVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_18 + threadIdx.x)];
        s_val[threadIdx.x] = __dummys_val_w();
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_20) {
        __dummystartSrcB = startSrcB[0];
        __dummyd_SrcKey = d_SrcKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + ((stride + _unknown_21) + threadIdx.x)];
        s_key[128 + threadIdx.x] = __dummys_key_w();
        __dummystartSrcB = startSrcB[0];
        __dummyd_SrcVal = d_SrcVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + ((stride + _unknown_23) + threadIdx.x)];
        s_val[128 + threadIdx.x] = __dummys_val_w();
    }
    __syncthreads();
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_25) {
        __dummys_key = s_key[threadIdx.x];
        __dummys_val = s_val[threadIdx.x];
        __dummylenSrcB = lenSrcB[0];
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_29) {
        __dummys_key = s_key[128 + threadIdx.x];
        __dummys_val = s_val[128 + threadIdx.x];
        __dummylenSrcA = lenSrcA[0];
    }
    __syncthreads();
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_33) {
        s_key[dstPosA] = __dummys_key_w();
        s_val[dstPosA] = __dummys_val_w();
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_34) {
        s_key[dstPosB] = __dummys_key_w();
        s_val[dstPosB] = __dummys_val_w();
    }
    __syncthreads();
    __dummylenSrcA = lenSrcA[0];
    if (threadIdx.x < _unknown_35) {
        __dummys_key = s_key[threadIdx.x];
        __dummystartDstA = startDstA[0];
        d_DstKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_37 + threadIdx.x)] = __dummyd_DstKey_w();
        __dummys_val = s_val[threadIdx.x];
        __dummystartDstA = startDstA[0];
        d_DstVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_39 + threadIdx.x)] = __dummyd_DstVal_w();
    }
    __dummylenSrcB = lenSrcB[0];
    if (threadIdx.x < _unknown_40) {
        __dummylenSrcA = lenSrcA[0];
        __dummys_key = s_key[_unknown_41 + threadIdx.x];
        __dummystartDstB = startDstB[0];
        d_DstKey[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_43 + threadIdx.x)] = __dummyd_DstKey_w();
        __dummylenSrcA = lenSrcA[0];
        __dummys_val = s_val[_unknown_44 + threadIdx.x];
        __dummystartDstB = startDstB[0];
        d_DstVal[(128 * (blockIdx.x - (blockIdx.x % ((2 * stride) / 128)))) + (_unknown_46 + threadIdx.x)] = __dummyd_DstVal_w();
    }
}
