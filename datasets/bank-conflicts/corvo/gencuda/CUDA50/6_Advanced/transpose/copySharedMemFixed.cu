//pass
//--blockDim=[32,32,1] --gridDim=[64,64,1]

extern __attribute__((device)) int __dummyidata_w();
extern __attribute__((device)) int __dummyodata_w();
extern __attribute__((device)) int __dummytile_w();
__global__ void kernel(int *__dummy, int *idata, int *odata)
{
    __shared__ int tile[256];
    int __dummyidata;
    int __dummyodata;
    int __dummytile;
    for (int j = 0; j < 16; j += 16) {
        __dummyidata = idata[(1024 * (((16 * blockIdx.y) + threadIdx.y) + j)) + ((16 * blockIdx.x) + threadIdx.x)];
        tile[(16 * (threadIdx.y + j)) + threadIdx.x] = __dummytile_w();
    }
    __syncthreads();
    for (int j1 = 0; j1 < 16; j1 += 16) {
        __dummytile = tile[(16 * (threadIdx.y + j1)) + threadIdx.x];
        odata[(1024 * (((16 * blockIdx.y) + threadIdx.y) + j1)) + ((16 * blockIdx.x) + threadIdx.x)] = __dummyodata_w();
    }
}
