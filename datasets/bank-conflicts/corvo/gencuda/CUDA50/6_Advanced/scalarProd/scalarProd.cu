//pass
//--blockDim=[256,1,1] --gridDim=[128,1,1]

extern __attribute__((device)) int __dummyaccumResult_w();
extern __attribute__((device)) int __dummyd_A_w();
extern __attribute__((device)) int __dummyd_B_w();
extern __attribute__((device)) int __dummyd_C_w();
__global__ void kernel(int *__dummy, int *d_A, int *d_B, int *d_C, int elementN, int vectorN)
{
    __shared__ int accumResult[1024];
    int __dummyaccumResult;
    int __dummyd_A;
    int __dummyd_B;
    int __dummyd_C;
    for (int vec = blockIdx.x; vec < vectorN; vec += 128) {
        for (int iAccum = 0; iAccum < 1024; iAccum += 256) {
            if (iAccum >= threadIdx.x && iAccum < 1024) {
                for (int pos = (elementN * vec) + iAccum; pos < (elementN * vec) + elementN; pos += 1024) {
                    __dummyd_A = d_A[pos];
                    __dummyd_B = d_B[pos];
                }
                accumResult[iAccum] = __dummyaccumResult_w();
            }
        }
        for (int stride = 1; stride < 513; stride *= 2) {
            __syncthreads();
            for (int iAccum1 = 0; iAccum1 < stride; iAccum1 += 256) {
                if (iAccum1 >= threadIdx.x && iAccum1 < stride) {
                    __dummyaccumResult = accumResult[iAccum1];
                    __dummyaccumResult = accumResult[stride + iAccum1];
                    accumResult[iAccum1] = __dummyaccumResult_w();
                }
            }
        }
        if (threadIdx.x == 0) {
            __dummyaccumResult = accumResult[0];
            d_C[vec] = __dummyd_C_w();
        }
    }
}
