//pass
//--blockDim=[256,1,1] --gridDim=[6624,1,1]

extern __attribute__((device)) int __dummybuf_w();
extern __attribute__((device)) int __dummyd_Buffer_w();
extern __attribute__((device)) int __dummyd_Data_w();
__global__ void kernel(int *__dummy, int *d_Buffer, int *d_Data)
{
    extern __shared__ int buf[];
    int __dummybuf;
    int __dummyd_Buffer;
    int __dummyd_Data;
    if (threadIdx.x == 0) {
        __dummyd_Buffer = d_Buffer[blockIdx.x];
        buf[0] = __dummybuf_w();
    }
    __syncthreads();
    __dummyd_Data = d_Data[(256 * blockIdx.x) + threadIdx.x];
    __dummybuf = buf[0];
    __dummybuf = buf[0];
    __dummybuf = buf[0];
    __dummybuf = buf[0];
    d_Data[(256 * blockIdx.x) + threadIdx.x] = __dummyd_Data_w();
}
