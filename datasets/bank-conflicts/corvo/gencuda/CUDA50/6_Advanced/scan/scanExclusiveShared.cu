//pass
//--blockDim=[256,1,1] --gridDim=[6624,1,1]

extern __attribute__((device)) int __dummyd_Dst_w();
extern __attribute__((device)) int __dummyd_Src_w();
extern __attribute__((device)) int __dummys_Data_w();
__global__ void kernel(int *__dummy, int *d_Dst, int *d_Src, int size)
{
    __shared__ int s_Data[512];
    int __dummyd_Dst;
    int __dummyd_Src;
    int __dummys_Data;
    __dummyd_Src = d_Src[(256 * blockIdx.x) + threadIdx.x];
    d_Dst[(256 * blockIdx.x) + threadIdx.x] = __dummyd_Dst_w();
}
