//pass
//--blockDim=[256,1,1] --gridDim=[64,1,1]

extern __attribute__((device)) int __dummyg_idata_w();
extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *g_idata, int *g_odata, int n)
{
    __shared__ int sdata[16384];
    int __dummyg_idata;
    int __dummyg_odata;
    int __dummysdata;
    __dummyg_idata = g_idata[(256 * blockIdx.x) + threadIdx.x];
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    for (int s = 1; s < 256; s *= 2) {
        if (threadIdx.x % (2 * s) == 0) {
            __dummysdata = sdata[threadIdx.x];
            __dummysdata = sdata[threadIdx.x + s];
            sdata[threadIdx.x] = __dummysdata_w();
        }
        __syncthreads();
    }
    if (threadIdx.x == 0) {
        __dummysdata = sdata[0];
        g_odata[blockIdx.x] = __dummyg_odata_w();
    }
}
