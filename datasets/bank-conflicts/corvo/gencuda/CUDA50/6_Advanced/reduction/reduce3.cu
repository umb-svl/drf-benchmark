//pass
//--blockDim=[256,1,1] --gridDim=[64,1,1]

extern __attribute__((device)) int __dummyg_idata_w();
extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *g_idata, int *g_odata, int n)
{
    __shared__ int sdata[16384];
    int __dummyg_idata;
    int __dummyg_odata;
    int __dummysdata;
    __dummyg_idata = g_idata[(512 * blockIdx.x) + threadIdx.x];
    if (256 + ((512 * blockIdx.x) + threadIdx.x) < n) {
        __dummyg_idata = g_idata[256 + ((512 * blockIdx.x) + threadIdx.x)];
    }
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    for (int s = 1; s < 129; s *= 2) {
        if (threadIdx.x < s) {
            __dummysdata = sdata[threadIdx.x + s];
            sdata[threadIdx.x] = __dummysdata_w();
        }
        __syncthreads();
    }
    if (threadIdx.x == 0) {
        __dummysdata = sdata[0];
        g_odata[blockIdx.x] = __dummyg_odata_w();
    }
}
