//pass
//--blockDim=[128,1,1] --gridDim=[64,1,1]

extern __attribute__((device)) int __dummyg_idata_w();
extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *g_idata, int *g_odata, int n)
{
    __shared__ int sdata[1024];
    int __unk1 = __dummy[threadIdx.x];
    int __unk2 = __dummy[threadIdx.x];
    int __dummyg_idata;
    int __dummyg_odata;
    int __dummysdata;
    for (int __unk0 = __unk1; __unk0 < __unk2; __unk0 += 1) {
        __dummyg_idata = g_idata[(256 * blockIdx.x) + threadIdx.x];
        __dummyg_idata = g_idata[128 + ((256 * blockIdx.x) + threadIdx.x)];
    }
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    if (threadIdx.x < 64) {
        __dummysdata = sdata[64 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
    }
    __syncthreads();
    if (threadIdx.x < 32) {
        __dummysdata = sdata[32 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
        __dummysdata = sdata[16 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
        __dummysdata = sdata[8 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
        __dummysdata = sdata[4 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
        __dummysdata = sdata[2 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
        __dummysdata = sdata[1 + threadIdx.x];
        sdata[threadIdx.x] = __dummysdata_w();
    }
    if (threadIdx.x == 0) {
        __dummysdata = sdata[0];
        g_odata[blockIdx.x] = __dummyg_odata_w();
    }
}
