//pass
//--blockDim=[256,1,1] --gridDim=[256,1,1]

extern __attribute__((device)) int __dummyd_CallValue_w();
extern __attribute__((device)) int __dummyd_OptionData_w();
extern __attribute__((device)) int __dummyrngStates_w();
extern __attribute__((device)) int __dummys_Sum2Call_w();
extern __attribute__((device)) int __dummys_SumCall_w();
__global__ void kernel(int *__dummy, int *d_CallValue, int *d_OptionData, int *rngStates, int pathN)
{
    __shared__ int s_Sum2Call[256];
    __shared__ int s_SumCall[256];
    int __dummyd_CallValue;
    int __dummyd_OptionData;
    int __dummyrngStates;
    int __dummys_Sum2Call;
    int __dummys_SumCall;
    __dummyd_OptionData = d_OptionData[blockIdx.x];
    __dummyd_OptionData = d_OptionData[blockIdx.x];
    __dummyd_OptionData = d_OptionData[blockIdx.x];
    __dummyd_OptionData = d_OptionData[blockIdx.x];
    __dummyrngStates = rngStates[threadIdx.x + (256 * blockIdx.x)];
    for (int iSum = 0; iSum < 256; iSum += 256) {
        if (iSum >= threadIdx.x && iSum < 256) {
            s_SumCall[iSum] = __dummys_SumCall_w();
            s_Sum2Call[iSum] = __dummys_Sum2Call_w();
        }
    }
    rngStates[threadIdx.x + (256 * blockIdx.x)] = __dummyrngStates_w();
    if (threadIdx.x == 0) {
        __dummys_SumCall = s_SumCall[0];
        __dummys_Sum2Call = s_Sum2Call[0];
        d_CallValue[blockIdx.x] = __dummyd_CallValue_w();
    }
}
