//pass
//--blockDim=[32,1,1] --gridDim=[1,1,1]

extern __attribute__((device)) int __dummyg_idata_w();
extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummysdata_w();
__global__ void kernel(int *__dummy, int *g_idata, int *g_odata)
{
    extern __shared__ int sdata[];
    int __dummyg_idata;
    int __dummyg_odata;
    int __dummysdata;
    __dummyg_idata = g_idata[threadIdx.x];
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    __dummysdata = sdata[threadIdx.x];
    sdata[threadIdx.x] = __dummysdata_w();
    __syncthreads();
    __dummysdata = sdata[threadIdx.x];
    g_odata[threadIdx.x] = __dummyg_odata_w();
}
