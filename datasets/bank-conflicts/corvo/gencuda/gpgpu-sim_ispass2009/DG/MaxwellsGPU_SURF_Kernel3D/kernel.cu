//pass
//--blockDim=[32,1,1] --gridDim=[2,1,1]

extern __attribute__((device)) int __dummyg_Q_w();
extern __attribute__((device)) int __dummyg_rhsQ_w();
extern __attribute__((device)) int __dummys_fluxQ_w();
__global__ void kernel(int *__dummy, int *g_Q, int *g_rhsQ)
{
    __shared__ int s_fluxQ[63];
    int __unk0 = __dummy[threadIdx.x];
    int __unk1 = __dummy[threadIdx.x];
    int __unk2 = __dummy[threadIdx.x];
    int m = __dummy[threadIdx.x];
    int __dummyg_Q;
    int __dummyg_rhsQ;
    int __dummys_fluxQ;
    if (threadIdx.x < 21) {
        s_fluxQ[threadIdx.x] = __dummys_fluxQ_w();
        s_fluxQ[21 + threadIdx.x] = __dummys_fluxQ_w();
        s_fluxQ[42 + threadIdx.x] = __dummys_fluxQ_w();
        s_fluxQ[63 + threadIdx.x] = __dummys_fluxQ_w();
        s_fluxQ[84 + threadIdx.x] = __dummys_fluxQ_w();
        s_fluxQ[105 + threadIdx.x] = __dummys_fluxQ_w();
    }
    __syncthreads();
    if (threadIdx.x < 28) {
        for (int __unk01 = __unk1; __unk01 < __unk2; __unk01 += 1) {
            __dummys_fluxQ = s_fluxQ[m];
            __dummys_fluxQ = s_fluxQ[21 + m];
            __dummys_fluxQ = s_fluxQ[42 + m];
            __dummys_fluxQ = s_fluxQ[63 + m];
            __dummys_fluxQ = s_fluxQ[84 + m];
            __dummys_fluxQ = s_fluxQ[105 + m];
            __dummys_fluxQ = s_fluxQ[1 + m];
            __dummys_fluxQ = s_fluxQ[22 + m];
            __dummys_fluxQ = s_fluxQ[43 + m];
            __dummys_fluxQ = s_fluxQ[64 + m];
            __dummys_fluxQ = s_fluxQ[85 + m];
            __dummys_fluxQ = s_fluxQ[106 + m];
            __dummys_fluxQ = s_fluxQ[2 + m];
            __dummys_fluxQ = s_fluxQ[23 + m];
            __dummys_fluxQ = s_fluxQ[44 + m];
            __dummys_fluxQ = s_fluxQ[65 + m];
            __dummys_fluxQ = s_fluxQ[86 + m];
            __dummys_fluxQ = s_fluxQ[107 + m];
            __dummys_fluxQ = s_fluxQ[3 + m];
            __dummys_fluxQ = s_fluxQ[24 + m];
            __dummys_fluxQ = s_fluxQ[45 + m];
            __dummys_fluxQ = s_fluxQ[66 + m];
            __dummys_fluxQ = s_fluxQ[87 + m];
            __dummys_fluxQ = s_fluxQ[108 + m];
        }
        __dummyg_rhsQ = g_rhsQ[threadIdx.x + (96 * blockIdx.x)];
        g_rhsQ[threadIdx.x + (96 * blockIdx.x)] = __dummyg_rhsQ_w();
        __dummyg_rhsQ = g_rhsQ[32 + (threadIdx.x + (96 * blockIdx.x))];
        g_rhsQ[32 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
        __dummyg_rhsQ = g_rhsQ[64 + (threadIdx.x + (96 * blockIdx.x))];
        g_rhsQ[64 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
        __dummyg_rhsQ = g_rhsQ[96 + (threadIdx.x + (96 * blockIdx.x))];
        g_rhsQ[96 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
        __dummyg_rhsQ = g_rhsQ[128 + (threadIdx.x + (96 * blockIdx.x))];
        g_rhsQ[128 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
        __dummyg_rhsQ = g_rhsQ[160 + (threadIdx.x + (96 * blockIdx.x))];
        g_rhsQ[160 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
    }
}
