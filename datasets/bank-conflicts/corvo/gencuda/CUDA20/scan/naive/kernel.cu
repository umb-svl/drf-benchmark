//pass
//--blockDim=[32,1,1] --gridDim=[1,1,1]

extern __attribute__((device)) int __dummyg_idata_w();
extern __attribute__((device)) int __dummyg_odata_w();
extern __attribute__((device)) int __dummytemp_w();
__global__ void kernel(int *__dummy, int *g_idata, int *g_odata, int n)
{
    __shared__ int temp[64];
    int __dummyg_idata;
    int __dummyg_odata;
    int __dummytemp;
    __dummyg_idata = g_idata[threadIdx.x - 1];
    temp[threadIdx.x] = __dummytemp_w();
    for (int offset = 1; offset < n; offset *= 2) {
        __syncthreads();
        __dummytemp = temp[threadIdx.x];
        temp[n + threadIdx.x] = __dummytemp_w();
        if (threadIdx.x >= offset) {
            __dummytemp = temp[n + threadIdx.x];
            __dummytemp = temp[threadIdx.x - offset];
            temp[n + threadIdx.x] = __dummytemp_w();
        }
    }
    __syncthreads();
    __dummytemp = temp[threadIdx.x];
    g_odata[threadIdx.x] = __dummyg_odata_w();
}
