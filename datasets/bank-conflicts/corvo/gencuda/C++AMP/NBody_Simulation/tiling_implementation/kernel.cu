//pass
//--blockDim=[256,1,1] --gridDim=[1024,1,1]

extern __attribute__((device)) int __dummydata_in_pos_w();
extern __attribute__((device)) int __dummydata_in_vel_w();
extern __attribute__((device)) int __dummydata_out_pos_w();
extern __attribute__((device)) int __dummydata_out_vel_w();
extern __attribute__((device)) int __dummytile_mem_w();
__global__ void kernel(int *__dummy, int *data_in_pos, int *data_in_vel, int *data_out_pos, int *data_out_vel, int num_bodies, int offset, int size)
{
    __shared__ int tile_mem[256];
    int __dummydata_in_pos;
    int __dummydata_in_vel;
    int __dummydata_out_pos;
    int __dummydata_out_vel;
    int __dummytile_mem;
    __dummydata_in_pos = data_in_pos[((256 * blockIdx.x) + threadIdx.x) + offset];
    __dummydata_in_vel = data_in_vel[((256 * blockIdx.x) + threadIdx.x) + offset];
    for (int tile = 0; tile < num_bodies / 256; tile += 1) {
        __dummydata_in_pos = data_in_pos[threadIdx.x];
        tile_mem[threadIdx.x] = __dummytile_mem_w();
        __syncthreads();
        for (int j = 0; j < 256; j += 4) {
            __dummytile_mem = tile_mem[j];
            __dummytile_mem = tile_mem[1 + j];
            __dummytile_mem = tile_mem[2 + j];
            __dummytile_mem = tile_mem[3 + j];
        }
        __syncthreads();
    }
    data_out_pos[((256 * blockIdx.x) + threadIdx.x) + offset] = __dummydata_out_pos_w();
    data_out_vel[((256 * blockIdx.x) + threadIdx.x) + offset] = __dummydata_out_vel_w();
}
