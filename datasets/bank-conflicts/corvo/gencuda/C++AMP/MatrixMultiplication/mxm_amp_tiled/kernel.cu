//pass
//--blockDim=[32,32,1] --gridDim=[16,1,1]

extern __attribute__((device)) int __dummylocalA_w();
extern __attribute__((device)) int __dummylocalB_w();
extern __attribute__((device)) int __dummyva_w();
extern __attribute__((device)) int __dummyvb_w();
extern __attribute__((device)) int __dummyvresult_w();
__global__ void kernel(int *__dummy, int *va, int *vb, int *vresult)
{
    __shared__ int localA[256];
    __shared__ int localB[256];
    int __dummylocalA;
    int __dummylocalB;
    int __dummyva;
    int __dummyvb;
    int __dummyvresult;
    for (int i = 0; i < 256; i += 16) {
        __dummyva = va[((256 * ((32 * blockIdx.y) + threadIdx.y)) + i) + threadIdx.x];
        localA[(16 * threadIdx.y) + threadIdx.x] = __dummylocalA_w();
        __dummyvb = vb[(256 * (i + threadIdx.y)) + ((32 * blockIdx.x) + threadIdx.x)];
        localB[(16 * threadIdx.y) + threadIdx.x] = __dummylocalB_w();
        __syncthreads();
        for (int k = 0; k < 16; k += 1) {
            __dummylocalA = localA[(16 * threadIdx.y) + k];
            __dummylocalB = localB[(16 * k) + threadIdx.x];
        }
        __syncthreads();
    }
    vresult[(256 * ((32 * blockIdx.y) + threadIdx.y)) + ((32 * blockIdx.x) + threadIdx.x)] = __dummyvresult_w();
}
