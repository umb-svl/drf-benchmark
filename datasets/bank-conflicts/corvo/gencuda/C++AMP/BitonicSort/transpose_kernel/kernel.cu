//pass
//--blockDim=[32,32,1] --gridDim=[32,1,1]

extern __attribute__((device)) int __dummydata_in_w();
extern __attribute__((device)) int __dummydata_out_w();
extern __attribute__((device)) int __dummytranspose_shared_data_w();
__global__ void kernel(int *__dummy, int *data_in, int *data_out, int height)
{
    __shared__ int transpose_shared_data[256];
    int __dummydata_in;
    int __dummydata_out;
    int __dummytranspose_shared_data;
    __dummydata_in = data_in[(512 * ((32 * blockIdx.y) + threadIdx.y)) + ((32 * blockIdx.x) + threadIdx.x)];
    transpose_shared_data[(16 * threadIdx.y) + threadIdx.x] = __dummytranspose_shared_data_w();
    __syncthreads();
    __dummytranspose_shared_data = transpose_shared_data[(16 * threadIdx.y) + threadIdx.x];
    data_out[(512 * ((32 * blockIdx.x) + threadIdx.x)) + ((32 * blockIdx.y) + threadIdx.y)] = __dummydata_out_w();
}
