//pass
//--blockDim=[32,1,1] --gridDim=[256,1,1]

extern __attribute__((device)) int __dummyhistogram_amp_w();
extern __attribute__((device)) int __dummypartial_result_w();
extern __attribute__((device)) int __dummys_data_w();
__global__ void kernel(int *__dummy, int *histogram_amp, int *partial_result)
{
    __shared__ int s_data[256];
    int __dummyhistogram_amp;
    int __dummypartial_result;
    int __dummys_data;
    for (int i = 0; i < 65536; i += 256) {
        if (i >= threadIdx.x && i < 65536) {
            __dummypartial_result = partial_result[blockIdx.x + (256 * i)];
        }
    }
    s_data[threadIdx.x] = __dummys_data_w();
    for (int stride = 1; stride < 129; stride *= 2) {
        __syncthreads();
        if (threadIdx.x < stride) {
            __dummys_data = s_data[threadIdx.x];
            __dummys_data = s_data[threadIdx.x + stride];
            s_data[threadIdx.x] = __dummys_data_w();
        }
    }
    if (threadIdx.x == 0) {
        __dummys_data = s_data[0];
        histogram_amp[blockIdx.x] = __dummyhistogram_amp_w();
    }
}
