//pass
//--blockDim=[32,32,1] --gridDim=[1,1,1]

extern __attribute__((device)) int __dummygraph_w();
extern __attribute__((device)) int __dummyprimary_block_buffer_w();
__global__ void kernel(int *__dummy, int *graph, int passnum)
{
    __shared__ int primary_block_buffer[64];
    int _unknown_2 = __dummy[threadIdx.x];
    int _unknown_3 = __dummy[threadIdx.x];
    int _unknown_4 = __dummy[threadIdx.x];
    int __dummygraph;
    int __dummyprimary_block_buffer;
    __dummygraph = graph[(64 * ((8 * passnum) + threadIdx.y)) + ((8 * passnum) + threadIdx.x)];
    primary_block_buffer[(8 * threadIdx.y) + threadIdx.x] = __dummyprimary_block_buffer_w();
    __syncthreads();
    for (int k = 0; k < 8; k += 1) {
        __dummyprimary_block_buffer = primary_block_buffer[(8 * threadIdx.y) + threadIdx.x];
        if (_unknown_2 == 0) {
            __dummyprimary_block_buffer = primary_block_buffer[(8 * threadIdx.y) + k];
            if (_unknown_3 != 0) {
                __dummyprimary_block_buffer = primary_block_buffer[(8 * k) + threadIdx.x];
                if (_unknown_4 != 0) {
                    primary_block_buffer[(8 * threadIdx.y) + threadIdx.x] = __dummyprimary_block_buffer_w();
                }
            }
        }
        __syncthreads();
    }
    __dummyprimary_block_buffer = primary_block_buffer[(8 * threadIdx.y) + threadIdx.x];
    graph[(64 * ((8 * passnum) + threadIdx.y)) + ((8 * passnum) + threadIdx.x)] = __dummygraph_w();
}
