# /usr/bin/env python3
import os
import shutil
import subprocess

import pandas as pd


# Run faial-bc and racuda on all kernels with shared arrays.
subprocess.run(['python', 'generate-cuda.py', '-f',
               'all-shared.txt', '-g', 'gencuda_all', '-l', 'gencuda_all.log'])
subprocess.run(['python', 'run-bc.py', '-f', 'all-shared.txt', '-d',
               'gencuda_all', '-o' 'bc_all.csv', '-l', 'bc_all.log'])
subprocess.run(['python', 'run-racuda.py', '-f', 'all-shared.txt', '-d',
               'gencuda_all', '-o' 'racuda_all.csv', '-l', 'racuda_all.log'])

# Collect results.
faial = pd.read_csv('bc_all.csv')
racuda = pd.read_csv('racuda_all.csv')

# Get all kernels that can be analyzed.
analyzable = pd.concat([faial, racuda], axis=1).dropna()
analyzable = analyzable['filename'].aggregate('\n'.join)[0]

# Write the names of analyzable kernels to a file.
with open('kernels.txt', 'w') as kernels:
    kernels.write(f'{analyzable}\n')

# Clean up files.
os.remove('bc_all.csv')
os.remove('racuda_all.csv')
os.remove('bc_all.log')
os.remove('racuda_all.log')
shutil.rmtree('gencuda_all')
