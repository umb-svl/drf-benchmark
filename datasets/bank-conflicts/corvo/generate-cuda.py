# /usr/bin/env python3

import argparse
import os
import subprocess
from pathlib import Path


# Exits if the path is not a valid directory.
def validate_dir(path):
    if not os.path.isdir(path):
        print(f'{os.path.abspath(path)} is not a directory.')
        exit(1)


# Generate the new dataset of kernels.
def generate_files(orig_dataset, gen_dataset, files, log, params):
    # Load the names of kernels in the dataset.
    kernels = open(files, 'r').read().splitlines()

    # Open log file to capture error output.
    with open(log, 'w') as error_log:
        for kernel in kernels:
            orig_kernel = f'{orig_dataset}/{kernel}'

            # Check whether the kernel exists.
            if not os.path.isfile(orig_kernel):
                error_log.write(f'{orig_kernel} is not a valid kernel path.\n')
                continue

            gen_kernel = f'{gen_dataset}/{kernel}'

            # Create parent directories (if they do not exist already).
            Path(gen_kernel).parent.mkdir(parents=True, exist_ok=True)

            # Set up command.
            args = ['faial-gen', orig_kernel, gen_kernel]
            args += [f'--{arg}' for arg in params.split(' ')]

            # Generate new CUDA kernel.
            result = subprocess.run(
                args=args,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.PIPE,
                universal_newlines=True
            )

            # Write errors to the log file.
            error_log.write(f'{kernel}\n{result.stderr}')


def main():
    parser = argparse.ArgumentParser(description='Generates a new dataset of '
                                     'CUDA kernels from an existing dataset.')
    parser.add_argument('--orig-dataset', '-o', default='faial',
                        help='Directory of the original dataset. '
                        'Default: %(default)s')
    parser.add_argument('--gen-dataset', '-g', default='gencuda',
                        help='Directory of the generated dataset. '
                        'Default: %(default)s')
    parser.add_argument('--files', '-f', default='kernels.txt',
                        help='Text file containing the list of kernel names. '
                        'Default: %(default)s')
    parser.add_argument('--log', '-l', default='gencuda.log',
                        help='Log file of the errors. Default: %(default)s')
    parser.add_argument('--params', '-p', default='racuda',
                        help='The parameters passed to faial-gen.')
    args = parser.parse_args()
    validate_dir(args.orig_dataset)
    generate_files(**vars(args))


if __name__ == '__main__':
    main()
