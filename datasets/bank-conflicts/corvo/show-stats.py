import pandas as pd


# Read in CSV data.
faial = pd.read_csv('bc_results.csv')
racuda = pd.read_csv('racuda_results.csv')

# Combine data from each tool into one dataframe.
faial.rename(columns={'exit_code': 'faial_exit_code',
             'cost': 'faial_cost'}, inplace=True)
faial.drop(columns=['filename'], inplace=True, errors='ignore')
racuda.rename(columns={'exit_code': 'racuda_exit_code',
              'cost': 'racuda_cost'}, inplace=True)
df = pd.concat([faial, racuda], axis=1)

# Categorize kernels based on the respective tools' costs.
both_zero = df.query("faial_cost == '0' & racuda_cost == '0'")
timed_out = df.query("racuda_cost == 'timed out'")
not_found = df.query("racuda_cost == 'not found'")
racuda_zero = df.query("faial_cost != '0' & racuda_cost == '0'")
faial_zero = df.query(
    "faial_cost == '0' & racuda_cost != '0' & racuda_exit_code == 0")

# Report on the number of kernels in each category.
print(f'{len(both_zero)} kernels are reported by both tools as 0.')
print(f'{len(timed_out)} kernels are timed out by RaCUDA.')
print(f'{len(not_found)} kernels are unable to find a cost by RaCUDA.')
print(f'{len(racuda_zero)} kernels are reported by RaCUDA as 0 and Faial as nonzero.')
print(f'{len(faial_zero)} kernels are reported by Faial as 0 and RaCUDA as nonzero.')
