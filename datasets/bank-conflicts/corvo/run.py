# /usr/bin/env python3

import subprocess

import numpy as np
import pandas as pd


# Run the experiment.
subprocess.run(['python', 'run-bc.py'])
subprocess.run(['python', 'run-racuda.py', '-t', '5m'])
subprocess.run(['python', 'run-bc.py', '-c', 'asympt', '-o', 'bc_results_a.csv'])
subprocess.run(['python', 'run-racuda.py', '-a', '-t', '5m', '-o', 'racuda_results_a.csv'])

# Read in the results.
faial = pd.read_csv('bc_results.csv')
racuda = pd.read_csv('racuda_results.csv')
faial_asympt = pd.read_csv('bc_results_a.csv')
racuda_asympt = pd.read_csv('racuda_results_a.csv')

# Combine the results into a single dataframe.
results = [faial, racuda, faial_asympt, racuda_asympt]
df = pd.concat(map(lambda x: x['cost'], results), axis=1)
df.columns = ['faial_cost', 'racuda_cost', 'faial_cost_asympt', 'racuda_cost_asympt']
df['filename'] = faial['filename']

# Document the reason for each cost discrepancy/similarity.
df.insert(0, 'reason', value=pd.Series(dtype=str))
df['reason'] = np.where(df['faial_cost_asympt'] == df['racuda_cost_asympt'], 'same asymptotic costs', df['reason'])
df['reason'] = np.where(df['faial_cost_asympt'] != df['racuda_cost_asympt'], 'different costs', df['reason'])
df['reason'] = np.where((df['faial_cost'] == '0') & (df['racuda_cost'] != '0'), 'zero/nonzero', df['reason'])
df['reason'] = np.where((df['faial_cost'] != '0') & (df['racuda_cost'] == '0'), 'nonzero/zero', df['reason'])
df['reason'] = np.where(df['faial_cost'] == df['racuda_cost'], 'same cost', df['reason'])
df['reason'] = np.where(df['racuda_cost'].isin({'timed out', 'not found'}), 'unsupported', df['reason'])

# Write the annotated results to a CSV file.
df.to_csv('results.csv', index=False)
