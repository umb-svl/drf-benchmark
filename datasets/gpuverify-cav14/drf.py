#!/usr/bin/env python3
import csv
from pathlib import Path
from tabulate import tabulate

def main():
    drf = get_drf()
    drf_all = list(drf['faial'].intersection(drf['gpuverify']).intersection(drf['pug']))

    # List kernels and note presence of possible barriers and loops
    kernels = [*map(lambda k: dict(toml=k), drf_all)]
    for kernel in kernels:
        with open(kernel['toml']) as fp:
            data = fp.read()
        kernel['has_loops'] = 'maybe' if data.find("for") > 0 else 'no'
        kernel['has_syncs'] = 'maybe' if data.find("__sync") > 0 else 'no'
    print(tabulate(kernels, headers="keys"))
    print(f"\n{len(drf_all)} kernels are DRF for faial, gpuverify, and pug.")

def get_drf():
    tools = ['faial', 'gpuverify', 'pug']
    drf = dict(map(lambda tool: (tool, set()), tools))
    for tool in tools:
        start = len(tool)+1
        filename = f"timings-{tool}.csv"
        with open(filename) as fp:
            for row in csv.DictReader(fp):
                if row['data_races'] == 'drf':
                    kernel = row['filename'][start:]
                    drf[tool].add(Path(kernel).with_suffix('.toml'))
    return drf

if __name__ == '__main__':
    main()
