import tomlkit
import yaml
import statistics
import numpy as np
import pandas as pd

column_names = ['max_sync_nesting','sync_loop_count','unsync_loop_count',
    'loop_count','write_count','read_count','if_count','sync_count',
    'line_count']
data = pd.DataFrame(columns=column_names)
stats = {}

def handle(filename):
    with open(filename) as fp:
        parsed_data = tomlkit.parse(fp.read())
    return parsed_data

def extract_info(entry):
    global data
    data = data.append({
        'max_sync_nesting': entry.get("max_sync_nesting"),
        'sync_loop_count': entry.get("sync_loop_count"),
        'unsync_loop_count': entry.get("unsync_loop_count"),
        'loop_count': entry.get("loop_count"),
        'write_count': entry.get("write_count"),
        'read_count': entry.get("read_count"),
        'if_count': entry.get("if_count"),
        'sync_count': entry.get("sync_count"),
        'line_count': entry.get("line_count"),
        }, ignore_index=True)

def myprint():
    global data,stats
    interesting_data = [
        # Overview
        ['Total kernels',
        'Total kernels with synced loops',
        'Total kernels with unsynced loops',
        'Total kernels with writes',
        'Total kernels with reads',
        'Total kernels with writes & reads',
        'Total kernels with conditionals',
        'Total kernels with syncs',
        ],
        # Synced loops
        ['Max nesting stats','# of Syncs stats','# of Conditionals stats'],
        # Line counts
        ['Line count stats'],
        # Accesses
        ['Write access stats','Read access stats'],
        # Conditionals
        ['Conditionals stats'],
        # Syncs
        ['Sync stats'],
    ]

    print(f"\n----- overview -----")
    for d in interesting_data[0]:
        print(f"  {d:<36}:{stats[d]:>10}")
    for i in range(1,len(interesting_data[1:])+1):
        if i==1:
            print(f"\n----- Kernels with synced loops stats -----")
            print(f"  {interesting_data[0][1]:<36}:{stats[interesting_data[0][1]]:>10}")
        elif i==2:
            print(f"\n----- Kernel line count stats -----")
        elif i==3:
            print(f"\n----- Kernel accesses stats -----")
            print(f"  {interesting_data[0][3]:<36}:{stats[interesting_data[0][3]]:>10}")
            print(f"  {interesting_data[0][4]:<36}:{stats[interesting_data[0][4]]:>10}")
            print(f"  {interesting_data[0][5]:<36}:{stats[interesting_data[0][5]]:>10}")
        elif i==4:
            print(f"\n----- Kernel conditionals stats -----")
        elif i==5:
            print(f"\n----- Kernel syncs stats -----")
            print(f"  {interesting_data[0][7]:<36}:{stats[interesting_data[0][5]]:>10}")
        for j in interesting_data[i]:
            if type(stats[j]) is tuple:
                v1,v2,v3,v4,v5 = stats[j]
                print(f"  {j:<30}\n\
                {'Mean':<22}:{v1:>10}\n\
                {'StdDev':<22}:{v2:>10}\n\
                {'Var':<22}:{v3:>10}\n\
                {'Min':<22}:{v4:>10}\n\
                {'Max':<22}:{v5:>10}")
            else:
                print(f"  {j:<30}{'':<22}:{v1:>10}")

def analyze_data(overview_data):
    global data,stats
    stats['Total kernels'] = overview_data['line_count']
    stats['Total kernels with synced loops'] = overview_data['sync_loop_count']
    stats['Total kernels with unsynced loops'] = overview_data['unsync_loop_count']
    stats['Total kernels with writes'] = overview_data['write_count']
    stats['Total kernels with reads'] = overview_data['read_count']
    stats['Total kernels with conditionals'] = overview_data['if_count']
    stats['Total kernels with syncs'] = overview_data['sync_count']
    stats['Total kernels with writes & reads'] = len(data["read_count"][(data["read_count"] > 0 ) & (data["write_count"] > 0 )])
    # for kernels with sync-loops, max level nesting stats
    sl_mln = data["max_sync_nesting"][data["sync_loop_count"] > 0 ].values
    stats['Max nesting stats'] = (format(np.mean(sl_mln), '.2f'),\
        format(np.std(sl_mln), '.2f'),format(np.var(sl_mln), '.2f'),\
        format(np.min(sl_mln), 'd'),format(np.max(sl_mln), 'd'))
    sl_s = data["sync_count"][data["sync_loop_count"] > 0 ].values
    stats['# of Syncs stats'] = (format(np.mean(sl_s), '.2f'),\
        format(np.std(sl_s), '.2f'),format(np.var(sl_s), '.2f'),\
        format(np.min(sl_s), 'd'),format(np.max(sl_s), 'd'))
    sl_c = data["if_count"][data["sync_loop_count"] > 0 ].values
    stats['# of Conditionals stats'] = (format(np.mean(sl_c), '.2f'),\
        format(np.std(sl_c), '.2f'),format(np.var(sl_c), '.2f'),\
        format(np.min(sl_c), 'd'),format(np.max(sl_c), 'd'))
    # line count stats
    lc_info = data["line_count"].values
    stats['Line count stats'] = (format(np.mean(lc_info), '.2f'),\
        format(np.std(lc_info), '.2f'),format(np.var(lc_info), '.2f'),\
        format(np.min(lc_info), 'd'),format(np.max(lc_info), 'd'))
    # accesses stats
    acc_wr_info = data["write_count"][(data["write_count"] > 0 )]
    stats['Write access stats'] = (format(np.mean(acc_wr_info), '.2f'),\
        format(np.std(acc_wr_info), '.2f'),format(np.var(acc_wr_info), '.2f'),\
        format(np.min(acc_wr_info), 'd'),format(np.max(acc_wr_info), 'd'))
    acc_rd_info = data["read_count"][(data["read_count"] > 0 )]
    stats['Read access stats'] = (format(np.mean(acc_rd_info), '.2f'),\
        format(np.std(acc_rd_info), '.2f'),format(np.var(acc_rd_info), '.2f'),\
        format(np.min(acc_rd_info), 'd'),format(np.max(acc_rd_info), 'd'))
    # ifs
    ifs_info = data["if_count"][(data["if_count"] > 0 )]
    stats['Conditionals stats'] = (format(np.mean(ifs_info), '.2f'),\
        format(np.std(ifs_info), '.2f'),format(np.var(ifs_info), '.2f'),\
        format(np.min(ifs_info), 'd'),format(np.max(ifs_info), 'd'))
    # syncs
    syncs_info = data["sync_count"][(data["sync_count"] > 0 )]
    stats['Sync stats'] = (format(np.mean(syncs_info), '.2f'),\
        format(np.std(syncs_info), '.2f'),format(np.var(syncs_info), '.2f'),\
        format(np.min(syncs_info), 'd'),format(np.max(syncs_info), 'd'))

def main():
    global data,stats
    with open("config.yaml") as fp:
        conf = yaml.safe_load(fp)
    for filename in conf["kernels"]:
        entry = handle(filename)
        extract_info(entry)

    overview_data = data.astype(bool).sum(axis=0)
    analyze_data(overview_data)
    myprint()


main()
