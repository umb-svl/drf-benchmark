#!/usr/bin/env python3
import csv
import tomlkit

def get_kernels():
    filename = f"timings-gpuverify.csv"
    with open(filename) as fp:
        for row in csv.DictReader(fp):
           if row['data_races'] != 'drf':
                filename = row['filename'][len("gpuverify/"):-2] + "toml"
                yield filename

def get_toml(filename):
    return filename[len("gpuverify/"):-len("cu")] + "toml"

def get_stats_of(filenames):
    total = 0
    sync_loop_count = 0
    loop_count = 0
    for filename in filenames:
        with open(filename) as fp:
            data = tomlkit.parse(fp.read())
        data = dict(data)
        for name in ["scalars", "arrays", "header", "pre", "body", "pass", "block_dim", "grid_dim", "includes"]:
            if name in data:
                del data[name]
        total += 1
        print(filename)
        print(data)
        print()
        try:
            if data["sync_loop_count"] > 0:
                sync_loop_count += 1
            if data["loop_count"] > 0:
                loop_count += 1
        except KeyError:
            pass
    print("total:", total, "sync_loops:", sync_loop_count, "all_loops:", loop_count)

kernels = list(get_kernels())
get_stats_of(kernels)
