#include <call_kernel.h>
#include <stdio.h>
#include <vector_types.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#define N 8

__device__  double C[2][2][2];

__device__ int index (int a, int b, int c){
	return 4*a + 2*b + c;
}

__global__ void foo(double *H) {

	int idx = index (threadIdx.x,threadIdx.y,threadIdx.z);

	H[idx] = C[threadIdx.x][threadIdx.y][threadIdx.z];
}

int main(){
	double *a;
	double *dev_a;
	int size = N*sizeof(double);

	cudaMalloc((void**)&dev_a, size);

	a = (double*)malloc(N*size);

	for (int i = 0; i < N; i++)
		a[i] = i;

	cudaMemcpy(dev_a,a,size, cudaMemcpyHostToDevice);

	printf("a:  ");
	for (int i = 0; i < N; i++)
		printf("%f	", a[i]);

	dim3 blockDim(2,2,2);
	//foo<<<1,blockDim>>>(dev_a);
	ESBMC_verify_kernel_d(foo, 1, blockDim, dev_a);

	cudaMemcpy(a,dev_a,size,cudaMemcpyDeviceToHost);

	printf("\nFunction Results:\n   ");

	for (int i = 0; i < N; i++){
		printf("%f	", a[i]);
		assert(a[i]==0);
	}
	free(a);

	cudaFree(&dev_a);

	return 0;
}

