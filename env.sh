TOOLS="$PWD/tools"
BENCH="$PWD/benchmark"

if ! which esbmc-gpu > /dev/null; then
    export PATH="$PATH:$TOOLS/esbmc-gpu-2.0"
fi

if ! which gklee-exec > /dev/null; then
    export PATH="$PATH:$TOOLS/gklee-f77577"
fi

if ! which gpuverify > /dev/null; then
    export PATH="$PATH:$TOOLS/gpuverify-2018-03-22"
fi

if ! which pug > /dev/null; then
    export PATH="$PATH:$TOOLS/pug-v0.2_x64"
fi

if ! which run-benchmark.py > /dev/null; then
    export PATH="$PATH:$BENCH"
fi

if ! which microbench-gen.py > /dev/null; then
    export PATH="$PATH:$BENCH/contrib"
fi

if ! which simulee.py > /dev/null; then
    export PATH="$PATH:$TOOLS/Simulee"
fi

if ! which racuda > /dev/null; then
    export PATH="$PATH:$TOOLS/racuda"
fi
