#!/usr/bin/env python3
import sys
import argparse
import subprocess
import time
import csv
import yaml
import string
import shlex
import os
from pathlib import Path
import json
import tempfile

def resolve_path(filename, base_dir=None):
    if not filename.is_absolute():
        filename = filename.expanduser()
    if base_dir is not None and not filename.is_absolute():
        filename = base_dir / filename
    return filename

def parse_sh(expr, data):
    return list(string.Template(arg).substitute(data) for arg in expr)

def parse_cmd(cmd, filename):
    return parse_sh(cmd, {"filename": filename})


def timed_call(cmd, cwd):
    with tempfile.NamedTemporaryFile() as f:
        tmr = ["/usr/bin/env", "time", "--quiet", "-f", '{"time":%e, "mem":%M}', "-o", f.name]
        start = time.time()
        result = subprocess.run(tmr + cmd,
            cwd=cwd,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
        )
        end = time.time()
        elapsed = end - start
        memory = None
        with open(f.name) as fp:
            try:
                parsed = json.load(fp)
                elapsed = parsed["time"]
                memory = parsed["mem"]
            except json.decoder.JSONDecodeError as err:
                pass
        return {
            "status": result.returncode,
            "time": elapsed,
            "output": result.stdout,
            "memory": None if memory is None else memory / 1024,
        }

def run_files(args, filenames):
    if args.command is None or len(args.command) == 0:
        raise ValueError(args.command)
    cmd_expr = args.command
    base_dir = resolve_path(Path(args.base_dir))
    cwd = None
    idx = 0
    for filename in filenames:
        filename = filename.strip()
        filename = Path(filename)
        orig_filename = filename
        filename = resolve_path(filename, base_dir=base_dir)

        if not filename.exists():
            print("ERR", "File not found:", filename, file=sys.stderr)
            if args.abort_on_missing:
                sys.exit(1)
            continue
        if args.change_dir:
            cwd = filename.parent
            filename = filename.name
        cmd = parse_cmd(cmd_expr, filename)
        for _ in range(args.repeat):
            idx += 1
            print("RUN", " ".join(cmd), file=sys.stderr)
            result = timed_call(cmd=cmd, cwd=cwd)
            result["filename"] = orig_filename
            if result["status"] != 0:
               sys.stderr.write(result["output"].decode('utf-8'))
            if args.log is not None:
                result["log"] = string.Template(args.log).substitute({'count': idx})
                with open(result["log"], "wb") as fpout:
                    fpout.write(result["output"])
            del result["output"]

            yield result

def main():
    parser = argparse.ArgumentParser(description="Runs a command a repeated number of times.")
    parser.add_argument('--input', '-i', required=True, help="Input filename")
    parser.add_argument('--log', '-l', default="output-${count}.log", help="Write the output of commands on each file.")
    parser.add_argument('--output', '-o', help="Output CSV file. Default: STDOUT")
    parser.add_argument('--base-dir', '-b', default='.', help="Base directory from which the filenames will be resolved from")
    parser.add_argument('--change-dir', '-C', action='store_true', help="Before executing, change parent directory of input.")
    parser.add_argument('--repeat', type=int, default=1, help="Repeat command the given number of times. Default: %(default)s")
    parser.add_argument('--abort-on-missing', action='store_true')
    parser.add_argument('--command', '-c', dest="command", nargs=argparse.REMAINDER)
    parser.add_argument('--config', '-f', default=None, help="Default: %(default)s")
    args = parser.parse_args()
    new_args = argparse.Namespace()
    config = resolve_path(Path(args.config)) if args.config is not None else None
    if config is not None and config.exists() and config.is_file():
        parser.set_defaults(**yaml.safe_load(config.open()))
    args = parser.parse_args()

    if args.output is None:
        output_file = sys.stdout
    else:
        output_file = open(args.output, "w")

    if isinstance(args.command, str):
        args.command = shlex.split(args.command)
    fields = ["status", "time", "memory", "filename"]
    if args.log is not None:
        fields.append("log")

    with resolve_path(Path(args.input)).open("r") as text_file:
        writer = csv.DictWriter(output_file, delimiter=',', fieldnames=fields)
        writer.writeheader()
        for row in run_files(args, text_file):
            writer.writerow(row)
            output_file.flush()


if __name__ == '__main__':
  main()
