mb_dir="../../../datasets/micro-benchmarks/"
rw_dir="../../../datasets/gpuverify-cav14/"

mb_leg="microbenchmark-legend.pdf"
mb_f1="Micro-benchmark-memory-1-50.pdf"
mb_f2="Micro-benchmark-time-1-50.pdf"
rw_leg1="time-relation-legend.pdf"
rw_leg2="stats-legend.pdf"
rw_f1="faial-stats.pdf"
rw_f2="gpuverify-stats.pdf"
rw_f3="pug-stats.pdf"
rw_f4="time-relation-faial-scatter.pdf"

dir=$(pwd)"/"
dest_dir="../../../../faial-research/eval-data/"

cp ${dir}${mb_dir}{${mb_leg},${mb_f1},${mb_f2}}  ${dir}${dest_dir}
cp ${dir}${rw_dir}{${rw_leg1},${rw_leg2},${rw_f1},${rw_f2},${rw_f3},${rw_f4}} ${dir}${dest_dir}
