#!/usr/bin/env python3
import argparse
import re
import csv
import subprocess
import sys
from pathlib import Path

def main():
    tools = ['faial', 'gpuverify', 'pug', 'esbmc', 'gklee', 'sesa', 'simulee',
             'racuda', 'pico']
    parser = argparse.ArgumentParser(
            description="Adds data_races field to tool timings CSVs.")
    parser.add_argument('--tool', choices=tools,
            help="Spesify tool of CSVs. Default: try to infer from filename")
    parser.add_argument('filename', nargs='+', help="CSVs to process.")
    args = parser.parse_args()

    for filename in args.filename:
        if not args.tool:
            match = re.search(r'timings-(\w+).*\.csv', filename)
            if match and match[1] in tools:
                tool = match[1]
            else:
                print(f"Warning: Unable to infer tool for {filename}. " + \
                        "Skipping. Please specify tool with --tool.")
                continue
        else:
            tool = args.tool
        if tool == 'sesa': # use parse-gklee for sesa logs
            tool = 'gklee'

        with open(filename, mode='r+') as fp:
            # Read and process CSV data.
            data = list(csv.DictReader(fp))
            process(data, tool)

            # Write data back to CSV, now with data race status.
            fp.seek(0)
            fp.truncate(0)
            fields = data[0].keys()
            writer = csv.DictWriter(fp, fieldnames=fields)
            writer.writeheader()
            writer.writerows(data)

def process(csv_data, tool):
    for row in csv_data:
        status = int(row['status'].strip())
        row['data_races'] = check_races(tool, status, row['log'])

# Check pug log for data-races.
def check_races(tool, status, log):
    base_dir = Path(sys.path[0])
    output = subprocess.check_output([str(base_dir / f"./parse-{tool}"), \
            log, "--status", str(status)])
    return output.decode('utf-8').strip()

if __name__ == '__main__':
    main()
