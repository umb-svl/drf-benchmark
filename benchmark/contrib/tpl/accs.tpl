{% extends "main.base" %}
{% set arrays = [
    {"name": "out", "type": "float", "size": block_dim[0] ** 2},
    ]
%}

{% block body %}
    {%- for idx in range(size) %}
    out[threadIdx.x + {{ idx }} * blockDim.x ] = out[threadIdx.x + {{ range(1, block_dim[0]) | random }} * blockDim.x ];
    {%- endfor %}
{% endblock %}
