{% extends "main.base" %}
{% set arrays = [
    {"name": "out", "type": "float", "size": array_size},
    {"name": "a", "type": "float", "size": array_size},
    {"name": "b", "type": "float", "size": array_size},
    ]
%}
{% set scalars = [ {"name": "ub", "type": "int"} ] %}

{% block body %}
    {%- for idx in range(size) %}
    {%- set loop_var %}i{{idx}}{% endset %}
    for (int {{loop_var}} = 0; {{loop_var}} < ub; {{loop_var}}++) {
        out[threadIdx.x] = a[{{loop_var}}] + b[{{loop_var}}];
    {%- endfor %}
    {%- for idx in range(size) %}
    {%- set loop_var %}i{{idx}}{% endset %}
    }
    {%- endfor %}
{% endblock %}
