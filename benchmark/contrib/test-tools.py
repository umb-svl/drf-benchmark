#!/usr/bin/env python3
import os
from shutil import copyfile
import sys
import yaml
import argparse
import subprocess
from string import Template
import shlex
import csv
from tabulate import tabulate
from pathlib import Path

def main():
    # Process configuration file and command line arguments:
    base_dir = Path(sys.path[0])
    config = yaml.safe_load(open(base_dir / 'test-tools.yaml'))
    args, kernel_yaml = get_args(config)
    args.filename = os.path.abspath(args.filename)
    os.chdir(args.prefix) # otherwise, gklee leaves junk files behind
    to_rm = [] # list of files to cleanup later

    # Get nice kernal name. filename: foo/bar.yaml -> name: bar
    name = os.path.splitext(os.path.basename(args.filename))[0]

    # Generate kernels for all tools:
    kernels = dict()
    for tool in args.tool:
        ext = '.c' if tool == 'pug' else '.cu'
        kernels[tool] = kernel = f"{name}-{tool}{ext}"

        # Generate kernel:
        if tool in ['sesa', 'simulee']: # sesa is a gklee preprocessor, uses gklee format
            tool = 'gklee'
        gen_tool = ['-t', tool] if tool not in ['gpuverify', 'faial'] else []
        gen_cmd = ['kernel-gen.py', args.filename, '-o', kernel] + gen_tool + \
                ['--array-size', str(args.array_size)] 
        gen_process = subprocess.run(gen_cmd)
        if gen_process.returncode:
            sys.exit(f"ERROR {gen_process.returncode} in kernel-gen.py")

        # Display kernel:
        if args.show_kernels:
            with open(kernel, 'r') as kfile:
                print('CAT', kernel)
                print(kfile.read(), end='')

    # Copy includes listed in kernel YAML to prefix directory:
    kernel_dir = os.path.dirname(args.filename)
    for include in kernel_yaml.get('includes', []):
        try:
            copyfile(os.path.join(kernel_dir, include),  include)
        except Exception as e:
            sys.exit(f"ERROR copying include {include}:\n{e}")
        if not args.save_kernels:
            to_rm.append(include)

    # Run benchmarks for each tool:
    timeout_cmd = ['timeout', str(args.timeout)]
    tool_args = get_tool_args(args.grid_dim, args.block_dim)
    results = []
    for (tool, kernel) in kernels.items():
        # files used by run-benchmark.py:
        run_bm_csv = f"{name}-{tool}.csv"
        run_bm_log = f"{name}-{tool}.log"
        run_bm_txt = f"{name}-{tool}.txt"
        with open(run_bm_txt, 'w') as text_file:
            text_file.write(kernel)

        tool_cmd = shlex.split(Template(config['tools'][tool]).safe_substitute(tool_args))
        cmd = ['run-benchmark.py',
               '-i', run_bm_txt,
               '-l', run_bm_log,
               '-o', run_bm_csv,
               '--change-dir',
               '-c'] + timeout_cmd + tool_cmd
        benchmark_process = subprocess.run(cmd)

        # Gather benchmark results:
        subprocess.run(['check-races', '--tool', tool, run_bm_csv])
        with open(run_bm_csv, 'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',')
            result = next(reader)
            result['tool'] = tool; result.pop('filename'); result.pop('log')
            results.append(result)

        # Add files to cleanup list:
        if not args.save_artifacts:
            to_rm += [run_bm_txt, run_bm_log, run_bm_csv]
        if not args.save_kernels:
            to_rm.append(kernel)

    # Cleanup:
    if to_rm:
        for file in to_rm:
            os.remove(file)

    # Display run-benchmark.py results:
    print(tabulate(results, headers="keys"))

# get_tool_args: The only tool that uses {grid, block}_dim is gpuverify, and
# gpuverify requires a single comma-separated string as format.
def get_tool_args(grid_dim, block_dim):
    def format_dim(elem):
        if elem is None:
            return None
        if isinstance(elem, str):
            return elem
        if isinstance(elem, int):
            return str(elem)
        if len(elem) == 0:
            return None
        return ",".join(map(str, elem))

    return dict(
        grid_dim = format_dim(grid_dim),
        block_dim = format_dim(block_dim)
    )

def load_file(filename):
    if filename.suffix == ".json":
        import json
        loader = json.load
    elif filename.suffix == ".toml":
        import toml
        loader = toml.load
    else:
        import yaml
        loader = yaml.safe_load
    return loader(filename.open())

# get_args: Parse and validate arguments.
def get_args(config):
    tools = list(config["tools"].keys())
    tools.sort()
    parser = argparse.ArgumentParser(description="Runs multiple verification tools on a given kernel. See config.yaml for default settings.")
    parser.add_argument('--array-size', type=int, default=config['array_size'], help="Symbolic-exec tools require a known array size. Default: %(default)s")
    parser.add_argument('--show-kernels', '-k', default=False, action='store_true', help="Display the kernels generated for each tool.")
    parser.add_argument('--save-kernels', '-s', default=False, action='store_true', help="Save generated kernels. By default they are removed.")
    parser.add_argument('--save-artifacts', '-a', default=False, action='store_true', help="Save run-benchmark.py artifacts. By default they are removed.")
    parser.add_argument('--prefix', default=config['prefix'], help="Directory to store generated kernels and run-benchmark.py artifacts. Default: %(default)s")
    parser.add_argument('--timeout', type=int, default=config['timeout'], help="Tool timeout in seconds. Default: %(default)s")
    parser.add_argument('--tool', '-t', choices=tools, default=[], action='append', help="Only run a specific tools. Argument can be repeated to run multiple tools. Default: faial, gpuverify, pug")
    parser.add_argument('--all', default=False, action='store_true', help="Run all tools.")
    parser.add_argument('filename', help="YAML file describing a kernel.")
    args = parser.parse_args()
    if not args.tool:
        args.tool = ['faial', 'gpuverify', 'pug']
    if args.all:
        args.tool = tools
    
    # Validation:
    if not os.path.isfile(args.filename):
        parser.error(f"{args.filename}: No such file")
    if args.timeout < 0:
        parser.error(f"--timeout expects non-negative value, given {args.timeout}")
    if not os.path.isdir(args.prefix):
        os.makedirs(args.prefix)

    # Check given YAML file for grid_dim and block_dim:
    try:
        kernel_yaml = load_file(Path(args.filename))
    except yaml.scanner.ScannerError as e:
        print(f"Error parsing '{args.filename}':", e, file=sys.stderr)
        sys.exit(1)
    # dims are loaded from the kernel
    args.grid_dim = kernel_yaml['grid_dim']
    args.block_dim = kernel_yaml['block_dim']
    if isinstance(args.grid_dim, int):
        args.grid_dim = [ args.grid_dim ]
    if isinstance(args.block_dim, int):
        args.block_dim = [ args.block_dim ]
    return (args, kernel_yaml)

if __name__ == '__main__':
    main()
