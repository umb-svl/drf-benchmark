KoAT v2.1
February 23, 2022

Source: https://github.com/aprove-developers/KoAT2-Releases/releases/tag/v2.1-cfr-mprf-np
Binary: https://github.com/aprove-developers/KoAT2-Releases/releases/download/v2.1-cfr-mprf-np/koat2_static.xz
Git Hash: https://github.com/aprove-developers/KoAT2-Releases/commit/c22c249d4cb85ef18d5596afc9e69c96a34a84a9
