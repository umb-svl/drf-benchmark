# GPUVerify
* Source: https://github.com/mc-imperial/gpuverify/releases/download/2018-03-22/GPUVerifyLinux64.zip
* Version: 2018-03-22

# GKLEE + SESA
* Source: in ./gklee-geof23, from https://github.com/Geof23/Gklee
* Binaries: in ./gklee-f77577
* Hash of git commit used: f77577343c67a3f1815ce5e802e2c933f8df876a


# ESBMC-GPU
* Source: https://github.com/ssvlab/esbmc-gpu
* Hash: 408a1f83585c38437084a6bed2a721789651cabc
* Depends on libz3-4

# PUG
* Source (binary): http://formalverification.cs.utah.edu/PUG/distributions/pug-v0.2_x64.tar.gz
* Version: 0.2
