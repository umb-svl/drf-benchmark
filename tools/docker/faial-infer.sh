set -x

source /common.sh

export VERSION=67f7004a563d9d36300848ef3ef1d6a67f45f119

PKGS="git cargo pkg-config librust-openssl-dev"
pkg_install $PKGS &&
    cargo install pyoxidizer --version 0.8.0 && \
    git clone https://gitlab.com/umb-svl/faial-infer/ &&
    cd faial-infer &&
    git checkout -b dev $VERSION &&
    export USER=faial-infer &&
    /root/.cargo/bin/pyoxidizer build &&
    mv /faial-infer/./build/x86_64-unknown-linux-gnu/debug/install/faial-infer /usr/local/bin &&
    # Undo dev data
    rm -rf /faial-infer &&
pkg_remove $PKGS
