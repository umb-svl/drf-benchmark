pkg_install() {
    apt-get update &&
    apt-get install -y "$@" &&
    apt-get clean &&
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
}
pkg_remove() {
    apt-get autoremove -y "$@"
}