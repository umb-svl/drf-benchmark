#include <call_kernel.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>



__global__ void kernel (int* p) {
p[threadIdx.x] = p[threadIdx.x + 1];

}
int main () {
    int *p;
    cudaMalloc((void**)&p, 1024 * sizeof(int));
    ESBMC_verify_kernel(kernel, 1, 64, p);
    return 0;
}
