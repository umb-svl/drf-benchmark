# How to update the tools container

Go to each repository `faial`, `faial-infer`, `c-to-json` and run:

```
$ git rev-parse HEAD
```

Copy/paste the hash-code and update the VERSION variable of `faial.sh`,
`faial-infer.sh`, `c-to-json.sh` if needed.

Go to the tools directory:

```bash
drf-benchmark/tools $ make benchmark     # Builds the tools and benchmark containers
drf-benchmark/tools $ make push-tools    # Updates the stored container of tools in the repository
```
