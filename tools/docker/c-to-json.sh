set -x
export DEBIAN_FRONTEND="noninteractive"
export TZ="America/New_York"
export VERSION=59454939c98cb197bdf85c4ccc2005629356e95f
source common.sh
PKGS="llvm-dev libclang-dev build-essential m4 git lld"
PKGS="$PKGS ninja-build cmake upx-ucl python"

pkg_install $PKGS &&

    git clone https://gitlab.com/umb-svl/c-to-json/ &&
    cd c-to-json &&
    git checkout -b dev $VERSION &&
    make &&
    make install &&
    rm -rf /c-to-json &&

pkg_remove $PKGS
