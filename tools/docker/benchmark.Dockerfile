FROM registry.gitlab.com/umb-svl/drf-benchmark/tools:1
USER root
RUN apt-get update && \
    apt-get install -y \
        time \
        python3 \
        python3-pip \
        vim \
        && \
    rm -rf /var/lib/apt/lists/* && \
    python3 -m pip install \
        toml==0.10.2 \
        Jinja2==2.11.2 \
        tabulate==0.8.7 \
        PyYAML==5.3.1 \
        numpy==1.18.2 \
        matplotlib==3.3.3 \
        pandas==1.1.4 \
        psutil==5.7.3 \
        scipy==1.5.4 \
	tomlkit==0.7.0
# Add run-benchmark.py and kernel-gen.py to the path for gpuverify-cav14/run.py:
ENV PATH="$PATH:/home/verify/benchmark"
ENV PATH="$PATH:/home/verify/benchmark/contrib"
USER verify
