#include <stdio.h>




__global__ void kernel (int* p) {
p[threadIdx.x] = 0;
}
int main () {
    
    /* Declare array 'p' */
    int *p;
    cudaMalloc((void**)&p, 1024 * sizeof(int));
    kernel<<<1, 64>>>(
        p
    );
    return 0;
}
