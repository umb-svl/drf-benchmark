FROM ubuntu:20.04

ADD common.sh /common.sh

ADD c-to-json.sh /
RUN bash /c-to-json.sh

ADD faial-infer.sh /
RUN bash /faial-infer.sh

ADD faial.sh /
RUN bash /faial.sh