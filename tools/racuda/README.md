# RaCUDA

Binary `absynth-cuda` taken from path `/home/artifact/popl20/absynth-cuda` from
inside VirtualBox archive `popl21-paper25.ova` fetched from paper artifact at
<https://doi.org/10.5281/zenodo.4323505>.

## Usage

To analyze for shared-memory bank conflicts, run: `racuda -metric shared kernel.cu`
