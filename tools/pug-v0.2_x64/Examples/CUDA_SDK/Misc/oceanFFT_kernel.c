
///////////////////////////////////////////////////////////////////////////////
// #include <cufft.h>
// #include <math_constants.h>

#include "my_cutil.h"
#define CUDART_PI_F 3.14


//Round a / b to nearest higher integer value
int cuda_iDivUp(int a, int b)
{
    return (a + (b - 1)) / b;
}


__device__
float2 make_float2(float x, float y)
{
    float2 z;
    z.x = x;
    // z.y = y;
    return z;
}

/* __device__ */
/* float2 make_float2(float2 arg) */
/* { */
/*     return make_float2(arg.x, arg.y); */
/* } */

// complex math functions
__device__
float2 conjugate(float2 arg)
{
    return make_float2(arg.x, -arg.y);
}

__device__
float2 complex_exp(float arg)
{
  // return make_float2(cosf(arg), sinf(arg));
}

__device__
float2 complex_add(float2 a, float2 b)
{
    return make_float2(a.x + b.x, a.y + b.y);
}

__device__
float2 complex_mult(float2 ab, float2 cd)
{
    return make_float2(ab.x * cd.x - ab.y * cd.y, ab.x * cd.y + ab.y * cd.x);
}

// generate wave heightfield at time t based on initial heightfield and dispersion relationship
__global__ void generateSpectrumKernel(float2* h0, float2 *ht, unsigned int width, unsigned int height, float t, float patchSize)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
    unsigned int i = y*width+x;
    
    // calculate coordinates
    float2 k;
    k.x = CUDART_PI_F * x / (float) patchSize;
    k.y = 2.0f * CUDART_PI_F * y / (float) patchSize;

    // calculate dispersion w(k)
    float k_len; // = sqrtf(k.x*k.x + k.y*k.y);
    float w; // = sqrtf(9.81f * k_len);

    float2 h0_k = h0[i];
    float2 h0_mk = h0[(((height-1)-y)*width)+x];

    float2 h_tilda; // = complex_add( complex_mult(h0_k, complex_exp(w * t)),
                    //              complex_mult(conjugate(h0_mk), complex_exp(-w * t)) );

    // output frequency-space complex values
    if ((x < width) && (y < height)) {
        ht[i] = h_tilda;
    }
}


// generate slope by partial differences in spatial domain
__global__ void calculateSlopeKernel(float* h, float2 *slopeOut, unsigned int width, unsigned int height)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
    unsigned int i = y*width+x;

    float2 slope;
    if ((x > 0) && (y > 0) && (x < width-1) && (y < height-1)) {
      slope.x = h[i+1] - h[i-1];
      slope.y = h[i+width] - h[i-width];
    } else {
      slope = make_float2(0.0f, 0.0f);
    }
    if (y < 1) 
      slopeOut[i] = slope;
}
