
#define N 4

#include "my_cutil.h"

__global__ void kernel (int *a, int b) {

int idx = blockIdx.x * blockDim.x + threadIdx.x;

if (idx < N) a[idx] = a[idx] + b;}
