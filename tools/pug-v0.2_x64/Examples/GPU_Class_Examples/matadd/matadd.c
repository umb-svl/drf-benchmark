
// #include <stdio.h>
// #include <stdlib.h>
// #include <cuda_runtime.h>
#include "my_cutil.h"

/*  #define K1 // +P +R */
/*  #define K2 //    +R */
/*  #define K3 // +P  */
/*  #define K4 // +P +R */
/*  #define K5     // uncompilable */
/*  #define K6 */
/*  #define K7 // +C +P +R */
/*  #define K8 */
/*  #define K9     // unsupported syntax */
/*  #define K10 // +P */
/*  #define K11 */
/*  #define K12 */
/*  #define K13 // +P */
/*  #define K14 // +P +R */
/*  #define K15 // +P +R */
/*  #define K16 // +R */
/*  #define K17 */
/*  #define K18 */
/*  #define K19 */
/*  #define K20  */
/*  #define K21 */
/*  #define K22  // uncompilable */
/*  #define K23  // +P +R */

/*  #define K24 */
/*  #define K25 // +P +R */
 #define K26 // +P
/*  #define K27 //  test on 256 bitvectors and non-overflowing protection */
/*  #define K28 // +P */



/*****************************************************************/
/*          Kernel 1                                             */
/*****************************************************************/

#ifdef K1 

#define N (32 * 4)
#define M (64 * 4)
#define GRIDSIZE 4
#define BLOCKSIZE 64
#define ELEM_TO_OPERATE 32

 __global__ void matrixAddKernel(int *A, int *B, int *C)
{
  int block = blockIdx.x + (blockIdx.y * GRIDSIZE);
  int index = (block * BLOCKSIZE) + threadIdx.x;
  index += (blockIdx.x * 32);
  int index0 = index;
  
  for(int i = 0; i < ELEM_TO_OPERATE; i++)
    {		
      index = index0 + i * M;
      C[index] = A[index] + B[index];
      
      // index += M;		
    }
}

/*
 __global__ void matrixAddKernel1(int *A, int *B, int *C)
{
	int block = blockIdx.x + (blockIdx.y * GRIDSIZE);
 	int index = (block * BLOCKSIZE) + threadIdx.x;
 	index += (blockIdx.x * 32); 
	
	for(int i = 0; i < ELEM_TO_OPERATE; i++)
	{		
		C[block] = 0;
 		index += M;		
	}
}
*/

#endif


/*****************************************************************/
/*          Kernel 2                                             */
/*****************************************************************/


#ifdef K2

#define M 128 //number of rows
#define N 256 //number of cols
#define MATRIX_SIZE 32768
#define NUMBER_OF_BLOCKS 4
#define NUMBER_OF_THREADS_X 64
#define ELEMENTS_PER_THREAD 32

//Device function implementation
__global__ void add_matrix_gpuKernel(float *a, float *b, float *c) {
  
  int i,j, index;
    
  i = blockIdx.x*blockDim.x + threadIdx.x;
  j = blockIdx.y*blockDim.y + threadIdx.y;
  
  index = i+j*(NUMBER_OF_THREADS_X * ELEMENTS_PER_THREAD * NUMBER_OF_BLOCKS);
  int index0 = index;

  if(i<N && j<NUMBER_OF_BLOCKS)
  {
    //Each thread handles the addition of 32 elements in the Matrix
	for(int k=1; k<=ELEMENTS_PER_THREAD; k++)
	{
		if(index < MATRIX_SIZE)
		{
		  index = index0 + k * N;
		  c[index] = a[index] + b[index];
		  // index = index + N;
		}
	}
  }
}

#endif


/*****************************************************************/
/*          Kernel 3                                             */
/*****************************************************************/

#ifdef K3 

#define SIZE_ROW 128
#define SIZE_COL 256

#define BLOCKSIZE 64
#define GRIDSIZE_X 4
#define GRIDSIZE_Y 4
#define GRIDSIZE 16
#define ELE_PER_THREAD 32

__global__ void matrixAddKernel(int *inMatrix1, int *inMatrix2, int *outMatrix) {
        assume(blockDim.x <= BLOCKSIZE);
	int col = blockIdx.x * BLOCKSIZE + threadIdx.x;
	for(int i = 0; i < ELE_PER_THREAD; i++) {
		int row = blockIdx.y * ELE_PER_THREAD + i;
		int k = row*SIZE_COL+col;
/* 		*(outMatrix+k) = (*(inMatrix1+k)) + (*(inMatrix2+k)); */
		outMatrix[k] = inMatrix1[k] + inMatrix2[k];
	}
}

#endif


/*****************************************************************/
/*          Kernel 4                                             */
/*****************************************************************/

#ifdef K4 

#define THREADS				64
#define ELEMENTS_PER_THREAD		32
#define BLOCK_WIDTH			4
#define BLOCK_HEIGHT			4
#define BLOCK_SIZE			BLOCK_WIDTH * BLOCK_HEIGHT
#define MATRIX_WIDTH			THREADS * BLOCK_WIDTH
#define MATRIX_HEIGHT			ELEMENTS_PER_THREAD * BLOCK_HEIGHT
#define MATRIX_SIZE			MATRIX_WIDTH * MATRIX_HEIGHT

__global__ void add_matrix_gpuKernel(int *matrixA, int *matrixB, int *matrixC)
{
  assume(blockDim.x <= BLOCK_WIDTH);
	int i = blockIdx.x * THREADS + threadIdx.x;
	int j = blockIdx.y * ELEMENTS_PER_THREAD;
	int index = i + j * MATRIX_WIDTH;
        int index0 = index;
	for( int k = 0; k < ELEMENTS_PER_THREAD ; k++ )
	{
	  index = index0 + k * MATRIX_WIDTH;
	  matrixC[index] = matrixA[index] + matrixB[index];
	  //	index+=MATRIX_WIDTH;
	}
}

#endif


/*****************************************************************/
/*          Kernel 5                                             */
/*****************************************************************/

#ifdef K5 

#define SDATA( index)      CUT_BANK_CHECKER(sdata, index)

__global__ void
testKernel( DTYPE* A, DTYPE* B, DTYPE* result) 
{
  // shared memory
  // the size is determined by the host application
  extern  __shared__  DTYPE sdata[];

  // access thread id
  const unsigned int tid = threadIdx.x;
  // access number of threads in this block
  const unsigned int num_threads = blockDim.x;
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  
  i *= 32;

  for(unsigned int index = i; index < i + 32; index++) {
    result[index] = A[index]+B[index];
  }
//  g_odata[tid] = SDATA(tid);
}

#endif


/*****************************************************************/
/*          Kernel 6                                             */
/*****************************************************************/

#ifdef K6 

#define row 256
#define col 128
#define GRID 4
#define BLOCK 16
#define COLTHREAD 32
#define ROWS 128
#define COLUMNS 256
#define THREADS 64

__global__ void add_matrix_gpuKernel(int *a,int *b, int *c)
{
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x; 

	for(int i=0; i<COLTHREAD; i++){
		int index = COLUMNS*by*COLTHREAD+bx*THREADS+tx+i*COLUMNS;
		c[index] = a[index] + b[index];
	__syncthreads();
	}
}

#endif


/*****************************************************************/
/*          Kernel 7                                             */
/*****************************************************************/

#ifdef K7 

#define MATRIX_X 32
#define MATRIX_Y 16
#define GRID_X 4
#define GRID_Y 4
#define BLOCK_X 1
#define BLOCK_Y 8

__global__ void AddMatrixKernel (int *d_array_a, int *d_array_b, int *d_array_out, int amount_one_work) {
  assume(blockDim.x <= BLOCK_X && blockDim.y <= BLOCK_Y);
  assume(amount_one_work == 32);

	int my_tid_in_block = threadIdx.x + threadIdx.y * blockDim.x;
	int my_start_x = my_tid_in_block + blockIdx.x * blockDim.x * blockDim.y;
	int my_start_y = blockIdx.y * amount_one_work;
	int step = gridDim.x * blockDim.x * blockDim.y;
	int now_point = my_start_x + my_start_y * step;			
	// this is the start point in the "array like matrix"
	int now_point0 = now_point;


	for (int i = 0 ; i < amount_one_work ; i++) {
	  now_point = now_point0 + i * step;
	  d_array_out[now_point] = d_array_a[now_point] + d_array_b[now_point];
		// now_point += step;
	}
}

#endif


/*****************************************************************/
/*          Kernel 8                                             */
/*****************************************************************/

#ifdef K8 

#define GRIDSIZE_X 4
#define GRIDSIZE_Y 4
#define BLOCKSIZE 64
#define BLOCKCOLUMNSIZE 32

#define SIZE_X (GRIDSIZE_X * BLOCKSIZE)
#define SIZE_Y (GRIDSIZE_Y * BLOCKCOLUMNSIZE)
#define SIZE (SIZE_X * SIZE_Y)
#define GRIDCOLUMNSIZE (SIZE_Y * BLOCKSIZE)

#define DEBUG_MODE true

__global__ void add_matrix_gpuKernel(int *d_in_matrix_a, int *d_in_matrix_b, int *d_out_matrix_c) {
  
  /* Compute the starting and stopping indices for this thread */
  int startIndex = blockIdx.x  * GRIDCOLUMNSIZE
                 + threadIdx.x * SIZE_Y
                 + blockIdx.y  * BLOCKCOLUMNSIZE;
  
  int stopIndex = startIndex + BLOCKCOLUMNSIZE;
  
  /* Each thread runs through BLOCKCOLUMNSIZE elements in order (block data distribution) */
  for (int i=startIndex; i<stopIndex; i++) {
    d_out_matrix_c[i] = d_in_matrix_a[i] + d_in_matrix_b[i];
  } 
}

#endif


/*****************************************************************/
/*          Kernel 9                                             */
/*****************************************************************/

#ifdef K9 

#define GRID_SIZE           4   /* Blocks per row and column of grid. */
#define ELEMENTS_PER_THREAD 32  /* Number of matrix elements each thread adds. */
#define THREADS_PER_COLUMN  1   /* Number of threads operating on column in block (this * ELEMENTS_PER_THREAD is block row count). */
#define THREADS_PER_ROW     64  /* Number of threads operating on row in block (block column count). */

/* Helper definitions based on the above data - don't change. */
#define THREADS_PER_BLOCK   (THREADS_PER_COLUMN*THREADS_PER_ROW)
#define MATRIX_WIDTH        (GRID_SIZE*THREADS_PER_ROW)
#define MATRIX_HEIGHT       (GRID_SIZE*THREADS_PER_COLUMN*ELEMENTS_PER_THREAD)
#define MATRIX_ELEMENTS     (MATRIX_WIDTH*MATRIX_HEIGHT)

/*
 * add_matrix_gpu
 * This function is executed on the device. The input matrices are split in to
 * sections, so each thread adds ELEMENTS_PER_THREAD elements.
 */
__global__ void add_matrix_gpuKernel(int *A, int *B, int *C)
{
    int x, y;
    int index, end;

    /* Calculate the starting position. */
    x = blockIdx.x * blockDim.x + threadIdx.x;
    y = blockIdx.y * ELEMENTS_PER_THREAD * THREADS_PER_COLUMN + ELEMENTS_PER_THREAD * threadIdx.y;
    index = x + y * MATRIX_WIDTH;
    
    /* Compute the sums. */
    end = index + ELEMENTS_PER_THREAD * MATRIX_WIDTH;
    for (; index < end; index += MATRIX_WIDTH)
    {
        C[index] = A[index] + B[index];
    }
}

#endif


/*****************************************************************/
/*          Kernel 10                                            */
/*****************************************************************/

#ifdef K10 

#define BLOCK_WIDTH 64
#define BLOCK_HEIGHT 32
#define GRID_DIMENSION 4
#define BLOCK_SIZE BLOCK_WIDTH*BLOCK_HEIGHT
#define MATRIX_SIZE BLOCK_SIZE*GRID_DIMENSION*GRID_DIMENSION

__global__ void myaddMatricesKernel(int *matrix_a, int *matrix_b, int *output_matrix) {

  assume(blockDim.x <= BLOCK_WIDTH);
  int increment = BLOCK_WIDTH*GRID_DIMENSION;
  int startAddress = (gridDim.x*blockDim.x*blockIdx.y*BLOCK_HEIGHT) + (blockDim.x*blockIdx.x) + threadIdx.x;
  for(int i=0; i<BLOCK_HEIGHT; i++)
    {	
      output_matrix[startAddress + i*increment] = 
	matrix_a[startAddress + i*increment] + matrix_b[startAddress + i*increment];
    }

}

#endif


/*****************************************************************/
/*          Kernel 11                                            */
/*****************************************************************/

#ifdef K11 

typedef int myint;
#define NUM_THREADS 64
#define NUM_ELEMENTS_PER_THREAD 32
#define NUM_BLOCKS 16

__global__ void add_matrix_gpuKernel(myint *a, myint *b, myint *c, int N)
{
	int i = blockIdx.x ; 
	int j = blockIdx.y * gridDim.y;
	int index = (i + j) * (NUM_THREADS * NUM_ELEMENTS_PER_THREAD);
	index += threadIdx.x * NUM_ELEMENTS_PER_THREAD;
	for(int k=0; k < NUM_ELEMENTS_PER_THREAD; k++)
	{
		c[index + k] = a[index + k] + b[index + k];
	}
}

#endif

/*****************************************************************/
/*          Kernel 12                                            */
/*****************************************************************/

#ifdef K12 

#define THREADS_PER_BLOCK 64
#define BLOCKS_PER_GRID 16
#define BLOCKS_IN_ROW 4 //Sqrt(BLOCKS_PER_GRID)
#define ELEMENTS_PER_THREAD 32
#define N THREADS_PER_BLOCK*BLOCKS_PER_GRID*ELEMENTS_PER_THREAD   //total number of elements
#define MATRIX_WIDTH THREADS_PER_BLOCK*BLOCKS_IN_ROW
#define MATRIX_HEIGHT ELEMENTS_PER_THREAD*BLOCKS_IN_ROW

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c){   
	int i = blockIdx.x*THREADS_PER_BLOCK + threadIdx.x;
	int j = blockIdx.y*ELEMENTS_PER_THREAD;
	int index = i+j*MATRIX_WIDTH;
	for(int k=0; k < ELEMENTS_PER_THREAD; k++){
		c[index] = a[index] + b[index];  
		index += MATRIX_WIDTH;
    }
}

#endif

/*****************************************************************/
/*          Kernel 13                                            */
/*****************************************************************/

#ifdef K13 

#define GRIDX             4
#define GRIDY             4
#define BLOCKX            8
#define BLOCKY            8
#define BLOCKC            16
#define THREADC           64
#define NUMS_PER_THREAD   32
//#define THREADS_PER_BLOCK BLOCKX * BLOCKY
#define THREADS_PER_BLOCK THREADC
//#define SRC_ARRAY_SIZE (GRIDX * GRIDY * BLOCKX * BLOCKY * NUMS_PER_THREAD)
#define SRC_ARRAY_SIZE (BLOCKC * THREADC * NUMS_PER_THREAD)

#define BLOCK_SIZE (THREADS_PER_BLOCK * NUMS_PER_THREAD)

__global__ void array_addition_kernel_both_flatKernel(int *A, int *B, int *C) {
  assume(blockDim.x <= BLOCKX);
  int idx = (blockIdx.x * BLOCK_SIZE);
  //sums a column
  for (int k=0; k<NUMS_PER_THREAD; k++) {
    int lidx = idx + k * THREADS_PER_BLOCK + threadIdx.x;
    C[lidx] = A[lidx] + B[lidx];
  }
}
/*
__global__ void array_addition_kernel_flat_threadsKernel(int *A, int *B, int *C) {
  int idx = ((blockIdx.x * gridDim.x + blockIdx.y)* BLOCK_SIZE);
  //sums a column
  for (int k=0; k<NUMS_PER_THREAD; k++) {
	  int lidx = idx + k * THREADS_PER_BLOCK + threadIdx.x;
      C[lidx] = A[lidx] + B[lidx];
  }
}
__global__ void array_addition_kernel_2d_threadsKernel(int *A, int *B, int *C) {
  int idx =  ((blockIdx.x * gridDim.x + blockIdx.y)* BLOCK_SIZE);
      idx += (threadIdx.x * blockDim.x * NUMS_PER_THREAD);
  for (int k=0; k<NUMS_PER_THREAD; k++) {
	  int lidx = idx + (k * blockDim.y) + threadIdx.y;
      C[lidx] = A[lidx] + B[lidx];
  }
}
*/

#endif


/*****************************************************************/
/*          Kernel 14                                            */
/*****************************************************************/

#ifdef K14 

#define BLOCKS_PER_GRID 16
#define THREADS 64
#define ELEMENTS_PER_THREAD 32

// #define MATRIX_WIDTH THREADS*sqrt(BLOCKS_PER_GRID*1.0)
// #define MATRIX_HEIGHT ELEMENTS_PER_THREAD*sqrt(BLOCKS_PER_GRID*1.0)

#define MATRIX_WIDTH THREADS*BLOCKS_PER_GRID
#define MATRIX_HEIGHT ELEMENTS_PER_THREAD*BLOCKS_PER_GRID

__global__ void add_matrix_gpuKernel(int *a_matrix, int *b_matrix, int *c_matrix)
{
  assume(blockDim.x <= BLOCKS_PER_GRID);
  int i=blockIdx.x*THREADS+threadIdx.x;
  int j=blockIdx.y*ELEMENTS_PER_THREAD;
  int index = i+j*MATRIX_WIDTH;
  int index0 = index;
  for(int k=0; k < ELEMENTS_PER_THREAD; k++)
  {
    index = index0 + k * MATRIX_WIDTH;
    c_matrix[index]=a_matrix[index]+b_matrix[index];
	// index+=MATRIX_WIDTH;
  }
}

#endif


/*****************************************************************/
/*          Kernel 15                                            */
/*****************************************************************/

#ifdef K15 

#define	BLOCKS_IN_X			4
#define	BLOCKS_IN_Y			4	
#define THREADS				64
#define THREADS_IN_BLOCK	64
#define THREAD_ACCESSES		32

#define	COLS				(BLOCKS_IN_X * THREADS_IN_BLOCK)
#define	ROWS				(BLOCKS_IN_Y * THREAD_ACCESSES)
#define SIZE				(COLS * ROWS)

#define DEBUG

__global__ void add_matrix_gpuKernel(int* a, int* b, int* c)
{
  assume(blockDim.x <= 	BLOCKS_IN_X);
	int	index;

	index = blockIdx.y * 32 * COLS +
			blockIdx.x * 64 + threadIdx.x;
	int index0 = index;

	for (int i = 0; i < THREAD_ACCESSES; i++)
	{
	  index = index0 + i * COLS;
	  c[index] = a[index] + b[index];

		//index += COLS;
	}
}

#endif


/*****************************************************************/
/*          Kernel 16                                            */
/*****************************************************************/

#ifdef K16 

#define NUM_OF_ROWS 128
#define NUM_OF_ROWS_PER_BLOCK 32
#define NUM_OF_COLS 256
#define NUM_OF_COLS_PER_BLOCK 64

#define BLOCKSIZE 4

typedef int TYPE;

__global__ void add_matrix_gpuKernel(TYPE *a,TYPE *b, TYPE *c)
	{
	int x = blockIdx.x* blockDim.x * NUM_OF_ROWS_PER_BLOCK + threadIdx.x * NUM_OF_ROWS_PER_BLOCK;
	int y = blockIdx.y * NUM_OF_ROWS_PER_BLOCK * NUM_OF_COLS;
	int index = 0;
	for (int i=0; i<NUM_OF_ROWS_PER_BLOCK; i++){
		index = x + y + i;
		c[index] = a[index] + b[index];
	}
}

#endif


/*****************************************************************/
/*          Kernel 17                                            */
/*****************************************************************/

#ifdef K17 

#define SIZE 16
#define ELEMENTS 16*64*32
#define BLOCKSIZE 64
#define COLUMNS 128
#define ROWS 256               
#define ELEMENTS_PER_THREAD 32
#define MAT_COL_PER_BLOCK 8

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c, int N)
{ 
   int j;
   int index;
   int k = threadIdx.x * ELEMENTS_PER_THREAD;


   // There are 4x4 blocks. 8 columns of 32 elemens in each
   // block form one column (256 element) of a matrix. Therefore,
   // each block contains 8 columns of a matrix.

   j=(blockIdx.y*4 + blockIdx.x) * MAT_COL_PER_BLOCK;

   for(int i=0; i<ELEMENTS_PER_THREAD; i++ ) {
     index =i+k+j*N;
     c[index]=a[index]+b[index];
   }
}

#endif


/*****************************************************************/
/*          Kernel 18                                            */
/*****************************************************************/

#ifdef K18 

#define THREADS 64 //# of threads executing in a block
#define ElEMENTS 32 //# of elements accessed by a thread
#define COLUMN (64*4) // # of elements in a row
#define ROW (32*4) // # of elements in a column

/* Compute the kernel */
__global__ void add_matrix_gpuKernel(int *a, int *b, int *c)
{ 
   int i = blockIdx.x*blockDim.x+threadIdx.x;

   for(int k = 0; k < ElEMENTS; k++) {  
     int j = blockIdx.y*ElEMENTS + k;
     int index =i+j*COLUMN; 
     
     if( i<COLUMN && j<ROW) 
       c[index]=a[index]+b[index];
   } 
} 

#endif


/*****************************************************************/
/*          Kernel 19                                            */
/*****************************************************************/

#ifdef K19 

#define BLOCKS_PER_GRID_ROW 4
#define BLOCKS_PER_GRID_COLUMN 4
#define ELEMENTS_PER_THREAD 32
#define THREADS_PER_BLOCK 64

#define MATRIX_WIDTH (BLOCKS_PER_GRID_ROW * THREADS_PER_BLOCK)
#define MATRIX_HEIGHT (BLOCKS_PER_GRID_COLUMN * ELEMENTS_PER_THREAD)

#define SIZE (BLOCKS_PER_GRID_ROW * BLOCKS_PER_GRID_COLUMN * ELEMENTS_PER_THREAD * THREADS_PER_BLOCK)

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c)
{
  int i = blockIdx.x * THREADS_PER_BLOCK + threadIdx.x;
  int j = blockIdx.y * ELEMENTS_PER_THREAD;
  int index = i+j*MATRIX_WIDTH;
  for(int count = 0; count < ELEMENTS_PER_THREAD; count++) {
    c[index] = a[index] + b[index];
    index += MATRIX_WIDTH;
  }
}

#endif


/*****************************************************************/
/*          Kernel 20                                            */
/*****************************************************************/

#ifdef K20

#define BLOCK 4        //4*4 blocks per grid
#define THREADS 64     //64 threads per block
#define ELEMENTS 32    //32 elements per thread
#define N (4*64)       // x dim size of matrix
#define M (4*32)       // y dim size of matrix

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c)
{
	int i = blockIdx.x*blockDim.x+threadIdx.x;
	
	for (int j=0; j<ELEMENTS; j++){
	  int index = i+(j+blockIdx.y*ELEMENTS)*N;
	  if( i <N && j <M)
	    c[index]=a[index]+b[index];
	}
}

#endif


/*****************************************************************/
/*          Kernel 21                                            */
/*****************************************************************/

#ifdef K21

#define SIZE 32768
#define BLOCKSIZE 16

__global__ void computeKernel(int *d_in,int *d_m_arr,int *d_out) {
  int bid=blockIdx.x+blockIdx.y*4;

  for (int i=bid*(SIZE/BLOCKSIZE)+threadIdx.x*32;i<(bid*(SIZE/BLOCKSIZE)+((threadIdx.x+1)*32));i++)
    {
      if( i <SIZE )
	d_out[i]=d_in[i]+d_m_arr[i];
    }
}

#endif


/*****************************************************************/
/*          Kernel 22                                            */
/*****************************************************************/

#ifdef K22

#define SIZE 256
#define BLOCKSIZE 64
#define GRIDSIZE 16
#define NUMBER 6
#define M 128
#define N 256

__global__ void computeKernel(int *A,int *B, int *C) {

	dim3 dimBlock(BLOCKSIZE,BLOCKSIZE); 
	dim3 dimGrid(4,4); // N and M are NOT same so you need to change this

	int index;
	for (int temp=0; temp<32; temp++) {
		int j=64*blockIdx.y+threadIdx.x;
		int i=32*blockIdx.x+temp;
		if( i <M && j <N) {
			index = i*N + j;
			C[index]=A[index]+B[index];
		}
	}
}

#endif

/*****************************************************************/
/*          Kernel 23                                            */
/*****************************************************************/

#ifdef K23 

#define SIZEX 256
#define SIZEY 128
#define BLOCKSIZE 64
#define GRIDSIZE 4
//#define SIZE 16
//#define BLOCKSIZE 4

__global__ void computeKernel(int *d_in_a, int *d_in_b, int *d_out) {
  int index;

  assume(blockDim.x <= 8 && blockDim.y <= 8);
  index = blockIdx.x*SIZEX/GRIDSIZE + blockIdx.y*SIZEX*SIZEY/GRIDSIZE+ threadIdx.x;
  int index0 = index;

  for (int i = 0; i < SIZEY/GRIDSIZE; i++) {
    index = index0 + i * SIZEX;
    d_out[index] = d_in_a[index] + d_in_b[index];
    // index += SIZEX;
  }
}

#endif


/*****************************************************************/
/*          Kernel 24                                            */
/*****************************************************************/

#ifdef K24

#define DIMX 4*64
#define DIMY 4*32
#define SIZE (DIMX*DIMY)

int h_mat_b[SIZE];
int h_mat_a[SIZE];
int h_mat_c[SIZE];

__global__ void computeKernel(int *d_mat_a, int *d_mat_b, int *d_mat_c) {
  int i = blockIdx.y * 32 + (blockIdx.x * blockDim.x + threadIdx.x) * DIMY;

  for(int j=0; j<32; j++,i++)
    d_mat_c[i] = d_mat_a[i] + d_mat_b[i];
}

#endif


/*****************************************************************/
/*          Kernel 25                                            */
/*****************************************************************/

#ifdef K25

#define BLOCKS_IN_GRID 16
#define THREADS_IN_BLOCK 64
#define WORK_PER_THREAD 32
#define SIZE (WORK_PER_THREAD*THREADS_IN_BLOCK*BLOCKS_IN_GRID)

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c, int N)
{
  assume (blockDim.x <= 8);
  // Find the index in the array of the first element for this thread
  int row = blockIdx.y * WORK_PER_THREAD;
  int column = blockIdx.x * THREADS_IN_BLOCK + threadIdx.x;
  int rowlen = blockDim.x * gridDim.x;
  int start = row*rowlen + column;

  // Do the addition for this portion of the matrix
  int index = start;
  int index0 = index;
  for(int i = 0; i < WORK_PER_THREAD; i ++)
  {
    index = index0 + i * rowlen;
    c[index] = a[index] + b[index];
    // index += rowlen;
  }
}

#endif


/*****************************************************************/
/*          Kernel 26                                            */
/*****************************************************************/

#ifdef K26

#define ROWS 128
#define COLUMNS 256
//#define ROWS 10
//#define COLUMNS 10

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c)
{
  assume (blockDim.x <= 8);

  int iRow = blockIdx.y*blockDim.y + threadIdx.y;
  int jColumn = blockIdx.x*blockDim.x + threadIdx.x;
  int iStartIndex = iRow * 32 * COLUMNS;
  //printf ("iRow, jColumn = (%d, %d)\n", iRow, jColumn);
  //if (iRow % 32 == 0) {
    for (int iRow1 = iRow; iRow1 < 32+iRow; iRow1++) {
      int index = iStartIndex + (iRow1 * COLUMNS) + jColumn;
      if (index < ROWS * COLUMNS) {
	    c[index] = a[index]+ b[index];
      }
    }
  //}
}

#endif


/*****************************************************************/
/*          Kernel 27                                            */
/*****************************************************************/

#ifdef K27

__device__ int gpuadd(int a , int b)
{
	return a+b;
}

__global__ void add_matrix_gpuKernel(int *a, int *b, int *c, int N, int M )
{
	int index;
	for(int k= 0; k < 32; k++)
	{
	  index = N*(blockIdx.y*32+k) + blockIdx.x * 64 + threadIdx.x;
	  c[index] = gpuadd(a[index],b[index]);
	}
}

#endif


/*****************************************************************/
/*          Kernel 28                                            */
/*****************************************************************/

#ifdef K28 

#define NX 256
#define NZ 128
#define iter 32

__global__ void add_matrix_gpuKernel(int* a,int* b,int* c) {
  assume (blockDim.x <= 16);

   int i=blockIdx.x*blockDim.x+threadIdx.x;
   int j=blockIdx.y*blockDim.y+threadIdx.y;

   for(int k=0;k<iter;k++) {
      int index=i+j*NX*iter+k*NX;
      c[index]=a[index]+b[index];
   }
} 

#endif

