%-----------------------------------------------------------------------------

\documentclass[10pt]{llncs}

% \documentclass{sigplanconf}
% \documentclass[10pt]{IEEEtran}

\usepackage{url}
\usepackage{xspace}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{alltt}
\usepackage{color}
\usepackage{pb-diagram}
\usepackage{amsfonts}
\usepackage{wrapfig}

% \definecolor{gray1}{gray}{0.1}
\newcommand{\comment}[1]{}
\newcommand{\konst}[1]{\mbox{{#1}}}

\begin{document}

\title{PUG (Prover of User GPU Programs) v0.2}
% \subtitle{A Progress Report}

\author{Guodong Li}
\institute{School of Computing, University of Utah}

\maketitle

\pagestyle{plain}

\section{Installation}

For details on PUG, see our forthcoming FSE 2010 paper.
This manual pertains to our tool for which multiple versions exist
(this one refers to v0.2).

By un-taring the tarball we obtain the following directory structure:

\begin{itemize}
\item \textbf{bin} : the executable files;
\item \textbf{lib} : the required libraries (e.g. BOOST, ROSE and YICES); 
\item \textbf{include} : header files (e.g. BOOST, ROSE and YICES);
\item \textbf{src} : PUG source and header files (for developers);
\item \textbf{Examples} : Contains subdirectories with a set of CUDA kernels taken from the CUDA SDK 2.0 and
from Dr. Mary Hall's Parallel Programming for GPUs class at the University of Utah (CS 6963);
\item \textbf{Documentation} : user manual and README file along with cshrc and bashrc examples; 
\end{itemize}

PUG uses the following tools. 
Since all the relevant header files and libraries have been put into the ``include'' and ``lib'' directories
of the distribution, a developer need not install these tools again.  

\begin{itemize}
  \item BOOST C++ Libraries (http://www.boost.org/),
  \item The ROSE compiler (http://rosecompiler.org/) ,
  \item Yices -- the SMT solver (http://yices.csl.sri.com/).
\end{itemize}

PUG's execution refers to some libraries. 
Thus we need to set the environment variable \texttt{LD\_LIBRARY\_PATH} to include the path to
these libraries, e.g. setenv \texttt{LD\_LIBRARY\_PATH} ``/home/ligd/PUG/lib''.
See the file ``cshrc'' or ``bashrc'' for examples on how to do this in two common shell environments.


\section{Usage}

Firstly, the input kernel should be modified:

\begin{itemize}
  \item Replace the  {\sf \#include ``util.h''} with  {\sf \#include ``my\_cutil.h''}. 
        ``my\_cutil.h'' contains PUG's own definitions for CUDA extensions and other facilities. 
  \item Make sure the name of the kernel function to be checked contains ``ernel''. 
        For example, ``bitonic(...)'' should becomes ``bitonicKernel(...)'' or ``bitonickernel''.   
  \item Change the file extension to {\sf .c} or
     {\sf .C} (for more advanced programs).
  \item Cater to ROSE's need, e.g. make explicit type casting on the
     array arguments of a function call (otherwise ROSE will fail). 
     See the FAQ for more details.  
\end{itemize}

\noindent Then, to run PUG on an input kernel ``a.c'', use

\begin{verbatim}
   pug a.c
\end{verbatim}

\noindent where ``pug'' is the executable.
Note that the executable \texttt{pug}
resides in the \textbf{bin} directory.
You may add the bin directory to the environment variable \texttt{PATH},
e.g. setenv \texttt{PATH} ``/home/ligd/PUG/lib:/\$PATH''.

When a bug is found, PUG will report a counter example obtained from the SMT solver; 
otherwise PUG will report only statistics information, e.g. SMT solving times.  

Options for controlling PUG's execution can be set in [flags], [options] and
[values] sections of the ``config.txt''. The configuration file ``config.txt''
is assumed to be in the current directory .
%
\textbf{Flags} and \textbf{Options} values are either {\tt on} or {\tt off}.
\textbf{Values} take a numeric value: VERBOSE takes values from [0, 3], and
NBITS takes a value $>$ 1


\begin{itemize}

\item \konst{\textbf{ASSUME\_NO\_OVFLO}}  \textbf{(on / off)} : 
  When set to \textbf{on} in the {\tt config.txt} file, 
  PUG performs verification under the assumption that
  bit-vector operations do not overflow.
  Many kernels are written under the assumption that overflows are absent.
  PUG can generate false alarms if this flag is set to \textbf{off}.

\item \konst{\textbf{USE\_INCRSMT}}  \textbf{(on / off)} :
   Indicates whether to use incremental SMT solving 
       (this version seems to have bugs as the checker runs slower in this mode). 

\item \konst{\textbf{USE\_LOOP\_REFINEMENT}}  \textbf{(on / off)} :
       Indicates whether to turn on automatic loop refinement.
       This helps reduce false alarms caused by loop abstraction (default behavior
       when {\tt off} is chosen).

\item \konst{\textbf{SLICE\_ARRAYS}}  \textbf{(on / off)} :
      Indicates whether to slice away updates on shared arrays 
      (i.e. not adding them into the transition environment). 
      This option must be turned on for conflict (races and bank conflict) checking.  

\item \konst{\textbf{USE\_ASSUMES}}  \textbf{(on / off)} : 
      Indicates whether to consider the extra constraints introduced by 
      the ``assume'' derivative. If this option is off, 
      then these constraints will be ignored.  
      %
      Users can place these {\tt assume} directives in their source code
      in two senses:

      \begin{itemize}
      \item To constrain input arguments
      \item To state known loop invariants
      \item To constrain the values of shared variables
      \end{itemize}
      See our papers or the examples that come with the PUG distribution.
      
\item \konst{\textbf{CHK\_BANK\_CONFLICTS}}  \textbf{(on / off)} :
  Indicates whether to for check bank conflicts and grade the conflict rates
  into low or high rather than races. Race checking will terminate at the first race, 
  while bank conflict checking will continue till the end of the kernel. 

\item \konst{\textbf{VERBOSE}}  \textbf{(0, 1, 2, 3)} :
     Controls how many details are printed out 
     (the higher the value, the more verbose).

\item \konst{\textbf{NBITS}}  \textbf{12, 16, 32, etc.} : Specifies the size of the bitvectors
      to use during checking. This is a very important option since SMT solvers perform badly on large bitvectors.  
      The recommended size is 16 bits. 

\end{itemize}

Again, the configuration file ``config.txt'' is assumed to be at the current directory.


\section{Syntax of the input kernel}

PUG expects the kernels to be in specific format:

\begin{itemize}
  \item The kernel must be a C program containing no C++ features such as
     templates and classes.
  \item All the loops must be ``for'' loops.
     Thus \emph{you need to convert the ``while'' loops into ``for'' loops}. 
     In addition, the loop index must be declared in the loop header --
     thus {\sf for (i = lb; \dots; \dots)} should be changed to {\sf for (int i = lb; \dots; \dots)}.
     Infinite loops can be modeled by {\sf for (; ;)}.
  \item Only a portion of CUDA data structures (e.g. {\sf uint2}) and APIs (e.g. {\sf atomicAdd}) are supported. 
       They are defined in {\sf my\_cutil.h}. 
  \item Support for pointer arithmetic is presently disabled.
  \item Support for C structures/records is disabled in this version. 
\end{itemize}

\section{FAQ}

{\bf Q}: Why are the errors and warnings like the following reported?
  
``...kernel.c'', line 271: error: identifier ``bool'' is undefined.

``...kernel.c'', line 34: warning: function ... was declared but never referenced. 

\noindent {\bf A}: The ROSE compiler is unable to parse the input kernel. 
   This error may be fixed by changing ``kernel.c'' to ``kernel.C'' 
   or modifying the kernel to standard C format.  
   The warnings can be ignored. 
\\

\noindent {\bf Q}: Why does PUG complain about a type compatibility error at line 3? 

\begin{verbatim}
  1: __shared__ int s[10];
  2: int f(int* k);
  3: int i = f (s);
\end{verbatim}

\noindent {\bf A}: PUG defines {\sf\_\_shared\_\_} as {\sf volatile} so that ROSE can accept this syntax. 
You need to perform explicit type casting (from {\sf volatile int*} to {\sf int*}) when calling procedure f.
This limitation will be gone after the ROSE extension for CUDA is out.  
\begin{verbatim}
  3: int i = f((int*) s);  
\end{verbatim}

\noindent {\bf Q}: Why is the following access on $s[0]$ considered a race? 

\begin{verbatim}
  __shared__ int s[];
  s[0] = 1;
\end{verbatim}

\noindent {\bf A}: It is a race since multiple threads write the same address $0$. 
However this race could be considered to be \emph{benign} because all threads write the same value 
to $s[0]$. This version of PUG will report such races and terminate the checking.   
You may comment it out to make PUG resume the checking. 
Moreover, PUG doesn't consider the following access to non-array variable $x$ 
a race since we found that it was intended to be harmless race. 

\begin{verbatim}
  kernel(..., int x);
  x = 1;
\end{verbatim}

\noindent {\bf Q}: Why is there a false race alarm? 

\noindent {\bf A}: You may need to manually add the constraints (by using the ``assume'' derivative) 
on some symbolic input variables. If the alarm is related to a loop or the values of shared variables, 
you may inform PUG the facts you know about the program by inserting ``assume'' statements. 
\\

\noindent {\bf Q}: The automatic loop refinement seems to be wrong in analyzing the following loop.
{\small

\begin{verbatim}
__shared__ int s[];
int j = 0;
for (...; ...; ...) {
  1:  s[j] = ...;
  2:  j += 5;
}
\end{verbatim} 
}
\noindent {\bf A}: Here j is a loop carrying variable, PUG will try to refine its value range.
However our current data flow analyzer is accurate only if statement 2 appears before statement 1.
{\small
\begin{verbatim}
int j = -5;
for (...; ...; ...) {
  1: j += 5;  
  2: s[j] = ...;
}
\end{verbatim}
}

\noindent {\bf Q}: Where is the property checker (assertion checker)? 

\noindent {\bf A}: This checker is broken in this version due to some recent updates. 
The related source files are ``model.*'', ``modelPF.*''.  
The property checker is actually in PUG v0.3, which supports parametric verification.
An equivalence checker is also included in v0.3.   
\\

\noindent {\bf Q}: I am particularly interested in whether all schedules are enumerated by 
   controlling the SIds (Schedule Ids). How do I do this? 

\noindent {\bf A}: This method is shown to be un-scalable although it is the theoretical base of our method, 
thus we do not incorporate it in the checker anymore.
The current version adopts POR automatically, thus it need not enumerate all schedules explicitly. 
However, you may download PUG version 0.1 to experience this technique. 
Version 0.1 corresponds to our PPoPP 10 Poster and its source files are available from the PUG website at:

\begin{verbatim}
    http://www.cs.utah.edu/formal_verification/PUG/
\end{verbatim}


\noindent {\bf Q}: PUG seems to be not very stable and have a lot of limitations; 
   can it be more user friendly? 

\noindent {\bf A}: Yes it is. Currently PUG is still a prototype tool for research purposes 
  Currently it is under heavy development. 
  Users are assumed to be familiar with SMT solvers and should be able to interpret output the
  solver generates. We are extending it and plan to build a graphical UI for it.  
  Meanwhile your feedback and participation are welcome.

\end{document}
